<!DOCTYPE html>
<!--[if lt IE 7]>      <html lang="en" class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html lang="en" class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html lang="en" class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Jobvine</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

    <link rel="profile" href="http://gmpg.org/xfn/11">

    <!-- //Bootstrap
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">>
    -->

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">


    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css"/>

    <link rel="stylesheet" href="style.css">

    <link rel="shortcut icon" href="jobvine_favicon.ico" type="image/x-icon" >

    <!--[if lt IE 9]>
    <script src="js/vendor/html5shiv.js"></script>
    <script src="js/vendor/respond.js"></script>
    <![endif]-->

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>


    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', '', 'auto');
        ga('send', 'pageview');
    </script>

</head>

<body>

<div id="root"></div>

<!--[if lt IE 9]>
<p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/" target="_blank">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<div id="page" class="loggedIn">

    <header class="fixed change in">

        <div class="top">

            <div class="container">

                <div class="left">

                    <div class="logo"><a href="">Jobvine</a></div>

                    <div class="pageName">Recruiter Control Panel</div>

                    <div class="clear"></div>

                </div><!-- left -->

                <div class="right">

                    <a href="#" class="respMenu"><div class="bars"></div></a>

                    <div class="userNav">

                        <div class="top">

                            <div class="sym">

                                <span>H</span>

                            </div><!-- sym -->

                            <span class="name">Hellosquare</span>

                            <span class="arrow"></span>

                        </div>

                        <div class="dropdown">

                            <ul>

                                <li><a href="">My Profile</a></li>

                                <li><a href="">Post Job</a></li>

                                <li><a href="">Manage Jobs</a></li>

                                <li><a href="">CV Search</a></li>

                                <li><a href="">CV's Downloaded</a></li>

                                <li><a href="">Skills Alerts</a></li>

                                <li><a href="">Manage Agents</a></li>

                                <li><a href="">Buy Credits</a></li>

                                <li class="logout"><a href="">Logout</a></li>

                            </ul>

                        </div>

                    </div><!-- user nav -->


                    <div class="clear"></div>

                </div><!-- right -->

                <div class="clear"></div>

            </div><!-- end container -->

        </div><!-- top -->

        <div class="bottom overview">

            <nav class="links">

                <a href="">My Profile</a>

                <a href="">Post Job</a>

                <a href="" class="current">Manage Jobs</a>

                <a href="">CV Search</a>

                <a href="">CV's Downloaded</a>

                <a href="">Skills Alerts</a>

                <a href="">Manage Agents</a>

            </nav><!-- links -->

            <div class="overviewPanel">

                <div class="container l1">

                    <div class="wrapper">

                        <div id="jobCredits" class="block">

                            <span class="header">Job Credits</span>

                            <div class="box">

                                <a href="">
                                    <span class="value">100</span>
                                </a>

                                <a href="" class="link">Buy</a>

                            </div><!-- box -->

                        </div><!-- block -->

                        <div id="cvSearch" class="block">

                            <span class="header">CV Search</span>

                            <div class="box">

                                <a href="">
                                    <span class="value">31.06.16</span>
                                </a>

                                <a href="" class="link">Buy</a>

                            </div><!-- box -->

                        </div><!-- block -->

                        <div id="skillAlerts" class="block">

                            <span class="header">Skill Alerts</span>

                            <div class="box">

                                <a href="">
                                    <span class="value">33</span>
                                </a>

                                <a href="" class="link">Activate</a>

                            </div><!-- box -->

                        </div><!-- block -->

                        <div id="dailyCV" class="block">

                            <span class="header">Daily CV Limit</span>

                            <div class="box">
                                <span class="value">0 of 500</span>
                            </div><!-- box -->

                        </div><!-- block -->

                    </div><!-- wrapper -->

                    <div class="clear"></div>

                </div><!-- container -->

            </div><!-- overview -->

        </div><!-- bottom -->

    </header><!-- end header -->

    <div id="navigation">

        <a href="" class="respMenu"><div class="bars"></div></a>

        <div class="container">

            <div class="inner">

                <ul>

                    <li><a href="">My Profile</a></li>

                    <li><a href="">Post Job</a></li>

                    <li><a href="">Manage Jobs</a></li>

                    <li><a href="">CV Search</a></li>

                    <li><a href="">CV's Downloaded</a></li>

                    <li><a href="">Skills Alerts</a></li>

                    <li><a href="">Manage Agents</a></li>

                    <li><a href="">Buy Credits</a></li>

                    <li class="logout"><a href="">Logout</a></li>

                </ul>


            </div><!-- inner -->

        </div><!-- container -->

    </div><!-- end navigation -->


    <!--// main content body -->
    <main class="page recruiters">

        <div id="profile" class="content edit">

            <div class="container l1">

                <div class="title">

                    <h1>My Profile</h1>

                </div><!-- title -->

                <div class="form">

                    <div class="notification error">

                        <div class="icon"></div>

                        <p>Please fix the following errors in order to complete the form:</p>

                        <ul>

                            <li>Enter your first name</li>

                            <li>Enter your first name</li>

                        </ul>

                    </div><!-- notification -->

                    <form method="post">

                        <div class="block">

                            <ul>

                                <li>

                                    <label class="bolded">Sign In Details</label>

                                    <div class="field">chantel@hellosquare.co.za</div>

                                </li>

                            </ul>

                        </div><!-- block -->

                        <div class="block">

                            <h2>Company Details</h2>

                            <ul>

                                <li class="error">

                                    <label>Company/Organisation Name*</label>

                                    <div class="field">

                                        <input type="text" value="Icefield Consulting"/>

                                    </div><!-- field -->

                                </li>

                                <li>

                                    <label>Company Address Line 1</label>

                                    <div class="field">

                                        <input type="text"/>

                                    </div><!-- field -->

                                </li>

                                <li>

                                    <label>Company Address Line 2</label>

                                    <div class="field">

                                        <input type="text"/>

                                    </div><!-- field -->

                                </li>

                                <li>

                                    <label>Province*</label>

                                    <div class="field">

                                        <select class="custom_select" name="Province">

                                            <option selected="selected">Select</option>

                                            <option value="5">Eastern Cape</option>
                                            <option value="2">Free State</option>
                                            <option value="7">Gauteng</option>
                                            <option value="4">Kwazulu-Natal</option>
                                            <option value="6">Limpopo</option>
                                            <option value="9">Mpumalanga</option>
                                            <option value="3">Northern Cape</option>
                                            <option value="8">North-West</option>
                                            <option value="1">Western Cape</option>

                                        </select>

                                    </div><!-- field -->

                                </li>

                                <li>

                                    <label>City/Town*</label>

                                    <div class="field">

                                        <select class="custom_select" name="City">

                                            <option selected="selected">Select</option>

                                            <option value="1">Option 1</option>


                                        </select>

                                    </div><!-- field -->

                                </li>

                                <li>

                                    <label>Postal Code</label>

                                    <div class="field">

                                        <input type="text" value=""/>

                                    </div><!-- field -->

                                </li>

                                <li>

                                    <label>Different Postal Address</label>

                                    <div class="field check">

                                        <input type="checkbox"/>

                                    </div><!-- field -->

                                </li>

                                <li>

                                    <label>Recruitment Agency</label>

                                    <div class="field check">

                                        <input type="checkbox"/>

                                    </div><!-- field -->

                                </li>

                                <li>

                                    <label>Recruitment Area</label>

                                    <div class="field">

                                        <select class="custom_select">

                                            <option selected="selected">Select</option>

                                            <option value="1">Option 1</option>

                                        </select>

                                    </div><!-- field -->

                                </li>

                                <li>

                                    <label>Total Employees</label>

                                    <div class="field auto">

                                        <select class="custom_select">

                                            <option value="">Select</option>

                                            <option value="1" selected="selected">1-5</option>
                                            <option value="6">6-10</option>
                                            <option value="4">11-25</option>
                                            <option value="9">26-50</option>
                                            <option value="11">51-100</option>
                                            <option value="12">101-200</option>
                                            <option value="5">201-500</option>
                                            <option value="7">501-1000</option>
                                            <option value="1">1001-5000</option>
                                            <option value="2">5001-10000</option>
                                            <option value="10">10001-100000</option>
                                            <option value="8">100000+</option>

                                        </select>

                                    </div><!-- field -->

                                </li>

                            </ul>

                        </div><!-- block -->

                        <div class="block">

                            <h2>Personal Details</h2>

                            <ul>

                                <li>

                                    <label>Title*</label>

                                    <div class="field small">

                                        <select class="custom_select">

                                            <option value="">Select</option>

                                            <option value="4">Dr</option>
                                            <option value="2">Miss</option>
                                            <option value="3">Mr</option>
                                            <option value="5" selected="selected">Mrs</option>
                                            <option value="1">Ms</option>
                                            <option value="6">Prof</option>


                                        </select>

                                    </div><!-- field -->

                                </li>

                                <li>

                                    <label>First Name*</label>

                                    <div class="field">

                                        <input type="text" value="Joe"/>

                                    </div><!-- field -->

                                </li>

                                <li>

                                    <label>Surname*</label>

                                    <div class="field">

                                        <input type="text" value="Botha"/>

                                    </div><!-- field -->

                                </li>

                                <li>

                                    <label>Job Title</label>

                                    <div class="field">

                                        <input type="text"/>

                                    </div><!-- field -->

                                </li>

                                <li>

                                    <label>Phone number</label>

                                    <div class="field">

                                        <input type="text"/>

                                    </div><!-- field -->

                                </li>

                            </ul>

                        </div><!-- block -->

                        <div class="block">

                            <h2>Contact Details</h2>

                            <ul>

                                <li>

                                    <label>Website</label>

                                    <div class="field">

                                        <input type="text"/>

                                    </div><!-- field -->

                                </li>

                                <li>

                                    <label>Telephone Number*</label>

                                    <div class="field">

                                        <input type="text"/>

                                    </div><!-- field -->

                                </li>

                                <li>

                                    <label>Fax Number</label>

                                    <div class="field">

                                        <input type="text"/>

                                    </div><!-- field -->

                                </li>

                                <li>

                                    <label>Notify me of Expiring jobs</label>

                                    <div class="field check">

                                        <input type="checkbox" checked/>

                                    </div><!-- field -->

                                </li>

                            </ul>

                        </div><!-- block -->

                        <div class="submit">

                            <input type="submit" class="btn btnBlue" value="Update"/>

                            <div class="clear"></div>

                        </div>

                    </form>

                </div><!-- form -->

            </div><!-- container -->

        </div><!-- profil -->

    </main><!-- page -->

    <!--//footer -->
    <footer>

        <div class="tagline">

            <p>Be First <span></span> Be Fast <span></span> Be Smart</p>

        </div><!-- tag line -->

        <div class="container l1">

            <div class="top">

                <div class="threeColumn">

                    <div class="col one">

                        <h3>JobVine Global</h3>

                        <p>At Jobvine our goal is to help you make the most of the 80 or 90 years you have on this planet by connecting you to the real world opportunities that can help you achieve your goals and realize your dreams. Visit <a href="">Jobvine.com</a></p>

                    </div><!-- col -->

                    <div class="col two">

                        <h3>JobVine Blog</h3>

                        <p>News, views, career advice and interview tips. And more</p>

                    </div><!-- col -->

                    <div class="col three">

                        <h3>For Employers</h3>

                        <ul>

                            <li><a href="">Post a Job</a></li>

                            <li><a href="">Products & Services</a></li>

                            <li><a href="">Contact Us</a></li>

                        </ul>

                    </div><!-- col -->

                    <div class="clear"></div>

                </div><!-- three column -->

            </div><!-- top -->

            <div class="bottom">

                <div class="left">

                    <ul class="nav">

                        <li><a href="#">About Us</a></li>

                        <li><a href="#">Contact Us</a></li>

                        <li><a href="#">Terms and Conditions</a></li>

                        <li><a href="#">Testimonials</a></li>


                    </ul>

                    <div class="clear"></div>

                    <p>&#169; <?php echo date("Y");?>. JobVine.co.za All Right Reserved.  C/O Mauritius International Trust Company Limited, <br/>4th Floor, Ebene Skies, Rue de I'institut, Ebene, Mauritius</p>

                </div><!-- left -->


                <ul class="social">

                    <li><a href="#" class="twitter" target="_blank"></a></li>

                    <li><a href="#" class="fb" target="_blank"></a></li>

                    <li><a href="#" class="linkedin" target="_blank"></a></li>

                    <li><a href="#" class="gplus" target="_blank"></a></li>

                </ul><!-- end social -->


                <div class="clear"></div>

            </div><!-- bottom -->

            <div class="clear"></div>

        </div><!-- container -->

    </footer><!-- end footer -->



</div><!-- end page -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>

<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-color/2.1.2/jquery.color.min.js"></script>


<script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"></script>



<script src="js/main.js"></script>
<script src="js/dropdown.js"></script>


</body>
</html>
