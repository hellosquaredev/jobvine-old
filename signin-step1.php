<!DOCTYPE html>
<!--[if lt IE 7]>      <html lang="en" class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html lang="en" class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html lang="en" class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Jobvine</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

    <link rel="profile" href="http://gmpg.org/xfn/11">

    <!-- //Bootstrap
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">>
    -->

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">


    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css"/>

    <link rel="stylesheet" href="style.css">

    <link rel="shortcut icon" href="jobvine_favicon.ico" type="image/x-icon" >

    <!--[if lt IE 9]>
    <script src="js/vendor/html5shiv.js"></script>
    <script src="js/vendor/respond.js"></script>
    <![endif]-->

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>


    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', '', 'auto');
        ga('send', 'pageview');
    </script>

</head>

<body>

<div id="root"></div>

<!--[if lt IE 9]>
<p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/" target="_blank">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<div id="page">

    <header class="fixed change in">

        <div class="top">

            <div class="container">

                <div class="left">

                    <div class="logo"><a href="">Jobvine</a></div>

                    <div class="clear"></div>

                </div><!-- left -->

                <div class="clear"></div>

            </div><!-- end container -->

        </div><!-- top -->


    </header><!-- end header -->

    <div id="navigation">

        <a href="" class="respMenu"><div class="bars"></div></a>

        <div class="container">

            <div class="inner">

                <span class="header login">Login or Sign Up</span>

                <ul>

                    <li><a href="">Jobseekers</a></li>

                    <li><a href="">Recruiters</a></li>

                </ul>

                <a href="" class="btn btnWhiteB">Upload Your CV</a>

                <div class="recruiters">

                    <span class="header">Are You Recruiting?</span>

                    <a href="" class="btn btnCyan">Post A Job</a>

                </div><!-- recruiters -->


            </div><!-- inner -->

        </div><!-- container -->

    </div><!-- end navigation -->


    <!--// main content body -->
    <main class="page">

        <div id="profile" class="content signin step1">

            <div class="form container l1">

                <div class="title">

                    <h1>Job Seeker Registration</h1>

                </div><!-- title -->

                <div class="steps">

                    <div class="link current">

                        <span class="number">1</span>

                        <span class="text">Login Details</span>

                    </div>

                    <div class="link">

                        <span class="number">2</span>

                        <span class="text">CV &amp; Personal Details</span>

                    </div>

                    <div class="link">

                        <span class="number">3</span>

                        <span class="text">Confirmation</span>

                    </div>

                </div><!-- steps -->

                <form method="post" enctype="multipart/form-data">


                    <div class="block">

                        <ul>

                            <li class="error">

                                <label>Email Address*</label>

                                <div class="field">

                                    <div class="notification small error">

                                        <div class="icon"></div>

                                        <span>Email already exists</span>

                                    </div><!-- notification -->

                                    <input type="email" placeholder="Email Address" value="wayne@hellosquare.co.za"/>

                                </div>

                            </li>

                            <li>

                                <label>Confirm Email*</label>

                                <div class="field">

                                    <input type="email" placeholder="Confirm Email"/>

                                </div>

                            </li>

                        </ul>

                    </div><!-- block -->

                    <div class="block">

                        <ul>

                            <li>

                                <label>Password*</label>

                                <div class="field">

                                    <input type="password" placeholder="Password"/>

                                </div>

                            </li>


                            <li>

                                <label>Confirm Password*</label>

                                <div class="field">

                                    <input type="password" placeholder="Confirm Password"/>

                                </div>

                            </li>

                        </ul>

                    </div><!-- block -->


                    <div class="submit">

                        <span class="terms">

                            <input type="checkbox"/>

                            I accept the <a href="">Terms & Conditions*</a>

                        </span>

                        <input type="submit" class="btn btnBlue" value="Next Step"/>

                    </div>

                </form>

            </div><!-- container -->


        </div><!-- profile -->

    </main><!-- main -->




</div><!-- end page -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>

<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-color/2.1.2/jquery.color.min.js"></script>


<script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"></script>

<script src="js/main.js"></script>


</body>
</html>
