$(document).ready(function(){


    //add additional email accounts on step 2 recruiter sign in
    $("body").on("click", "#profile.signin.recruiter #addtionalAccounts a.add", function() {
         var parent = $(this).parent().parent("li");
        var field = parent.clone();

        field = field.find("input").val("").parent().parent().clone();

        parent.after(field);

        return false;

    });

    //add more questions on recruiter post a job
    $("body").on("click", "#postJob .questions .field.type a.add", function() {
        var parent = $(this).parent().parent();
        var field = parent.clone(), fieldlabel = "";

        if(!parent.next().hasClass("question")){

            field = field.find("input").val("").parent().parent().clone();
            field = field.find("label").text("Question "+($("#postJob .questions .question").length + 1)).parent().clone();

            parent.after(field);

        }
        else{

            parent.remove();

            $("#postJob .questions .question").each(function(index){

                $(this).find("label").text("Question "+(index + 1));

            });



        }




        return false;

    });


});

