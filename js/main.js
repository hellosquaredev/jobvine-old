$(document).ready(function(e) {

	var URL = $("#wpurl").val();//site url
	var templateURL = $("#wptemplateurl").val();//template url
	var hash = window.location.hash.replace("#","");
	var slider;

	var hasSticky = false; //set to true if an element needs to stick to the top
	var sticky_nav_offset_top = ($("nav").length > 0) ? $("nav").offset().top : 0;//sticky element offset


	//run intial function
	init();



	//navigation icon click
	$("body").on("click", ".respMenu", function(){
		var page = $("#page");

		page.removeClass("showNotice");

		if(page.hasClass("menu")){

			page.removeClass("menu");

		}
		else{

			page.addClass("menu");

		}

		return false;

	});

	$("body").on("click", "#companies .wrapper a.control.prev", function(){

		slider.slick("slickPrev");


		return false;

	});

	$("body").on("click", "#companies .wrapper a.control.next", function(){

		slider.slick("slickNext");


		return false;

	});

	// Open the jobs by email subscribe form
	$("body").on("click", "#openSubscribe", function(){

		$(this).addClass("close");
		$("#subscribe").addClass("open");

		return false;

	});

	//close the jobs by email form
	$("body").on("click", "#subscribe a.close", function(){

		$("#subscribe").removeClass("open").removeClass("success");
		$("#openSubscribe").removeClass("close");

		return false;

	});

	$("body").on("click", "#subscribe input[type='submit']", function(){

		$("#subscribe").addClass("success");

		return false;

	});



	//Show Alert Form
	$("body").on("click", ".alert span.header", function(){

		$(this).parent().toggleClass("open");

		return false;

	});

	//Close Alert Box
	$("body").on("click", ".alert a.closeAlert", function(){

		$(this).parent().removeClass("open").removeClass("success");

		return false;

	});


	//Alert form submit
	$("body").on("click", ".alert .form .inner input[type='submit']", function(){
		var wrapper = $(this).parent().parent().parent().parent().parent();

		wrapper.addClass("success");

		return false;

	});

	$("body").on("click", "header .left li a", function(){

		return false;

	});

	// contact notification click
	$("body").on("click", ".notification.small a.close", function(){

		$(this).parent().hide();

		$("li.error").removeClass("error");

		return false;

	});

	/* mobile header search icon */
	$("body").on("click", "header a.search", function(){

		$("header .bottom").toggleClass("active");
		$(this).toggleClass("active");

		return false;

	});

	// mobile notification click dropdown
	$("body").on("click", "header .notifications", function(){

		//$("header .notifications").toggleClass("active");

		return false;

	});

	$("header .notifications").on({
		mouseenter: function () {
			//alert(1);
			//$("header .notifications").addClass("active");
		},
		mouseleave: function () {
			//alert(2);
			//$("header .notifications").removeClass("active");
		}
	});

	//home page job types tabs
	$("body").on("click", "#jobTypes .tabButtons a", function(){

		if($(this).hasClass("active")){

			$(this).parent().parent().parent().addClass("open");

		}
		else{

			var tab = $(this).data("tab");

			$("#jobTypes .tabButtons a").removeClass("active");
			$(this).addClass("active");

			$(this).parent().parent().parent().removeClass("open");

			$("#jobTypes .tabs .tab").removeClass("active");
			$("#jobTypes .tabs .tab."+tab).addClass("active");

		}

		if($(window).width() < 750){

			$("html, body").animate({
				scrollTop: ($("#jobTypes .tabs .tab.active").offset().top - $("header").height() + 5)
			}, 1000);

		}

		return false;

	});

	$("body").on("click", "#jobTypes .tabs .tab a.tabButton", function(){
		var $this = $(this);


		if(!$(this).parent().hasClass("active")) {

			$("#jobTypes .tabs .tab").removeClass("open");


			$("#jobTypes .tabs .tab").removeClass("active");

			$this.parent().addClass("active");


			setTimeout(function () {

				$("html, body").animate({
					scrollTop: ($this.offset().top - $("header").height() + 5)
				}, 300, "linear", function () {

					//

					$this.parent().addClass("open");


				});

			}, 500);


		}
		else{

			$("#jobTypes .tabs .tab").removeClass("open");


			$("#jobTypes .tabs .tab").removeClass("active");

		}

		return false;

	});


	//mobile sidebar listing tabbing
	$("body").on("click", "#list aside h3", function(){


		if($(window).width() < 750) {
			var $this = $(this);

			/*
			 var parent = $(this).parent();

			 if(!parent.hasClass("active")){

			 $("#list aside .block").removeClass("active");
			 parent.addClass("active");

			 }
			 else{
			 $("#list aside .block").removeClass("active");
			 }
			 */

			if(!$(this).parent().hasClass("active")) {

				$("#list aside .block").removeClass("open");


				$("#list aside .block").removeClass("active");

				$this.parent().addClass("active");


				setTimeout(function () {

					$("html, body").animate({
						scrollTop: ($this.offset().top - $("header").height() + 5)
					}, 200, "linear", function () {

						$this.parent().addClass("open");

					});

				}, 500);


			}
			else{

				$("#list aside .block").removeClass("open");


				$("#list aside .block").removeClass("active");

			}



			return false;

		}

	});

	//Show the upload dropdown profile edit
	$("body").on("click", "#profile .cvUpdate", function(){

		$(this).parent().addClass("update");

		return false;

	});

	//cv file selector click
	$("body").on("click", "#files table tr", function(){

		$("#files table tr").removeClass("selected");
		$(this).addClass("selected");

		return false;

	});

	$("body").on("click", "#alerts table td a.removeAlert", function(){

		var row = $(this).parent().parent();

		row.addClass("removed").empty().append("<td colspan='3'>Job Alert Removed</td>");

		return false;

	});

	$("body").on("click", "#browse .paging a.control.prev", function(){

		$("#browse .paging").animate( { scrollLeft: "-=108" }, 300);

		return false;

	});

	$("body").on("click", "#browse .paging a.control.next", function(){

		$("#browse .paging").animate( { scrollLeft: "+=108" }, 300);

		return false;

	});

	//remove agent
	$("body").on("click", "#manageAgents table td a.removeAgent", function(){

		var row = $(this).parent().parent();

		row.addClass("removed").empty().append("<td colspan='6'>Agent Removed</td>");

		return false;

	});

	//toggle create agent form
	$("body").on("click", "#manageAgents a.create", function(){

		$("#manageAgents").addClass("createAgent");

		return false;

	});

	//remove skill alerts
	$("body").on("click", "#skillAlerts table td a.removeAlert", function(){

		var row = $(this).parent().parent();

		row.addClass("removed").empty().append("<td colspan='7'>Skill Alert Removed</td>");

		return false;

	});

	//close notice
	$("body").on("click", "#notice a.close", function(){

		$("#page").removeClass("showNotice");

		return false;

	});

	//close popup
	$("body").on("click", "#popup a.close", function(){

		$("#popup").remove();

		return false;

	});

	//F.A.Q accordion
	$("body").on("click", "#faq .accordion li .heading", function(){
		var parent = $(this).parent();

		$("#faq .accordion li").removeClass("current");
		parent.toggleClass("current");

		return false;

	});

	// Open the request more info form
	$("body").on("click", "#openRequestMore", function(){

		$(this).addClass("close");
		$("#requestMore").addClass("open");

		return false;

	});

	//close the request more info form
	$("body").on("click", "#requestMore a.close", function(){

		$("#requestMore").removeClass("open").removeClass("success");
		$("#openRequestMore").removeClass("close");

		return false;

	});

	$("body").on("click", "#requestMore input[type='submit']", function(){

		$("#requestMore").addClass("success");

		return false;

	});

	//capture clicks on any elements except
	$(document).click(function(e) {

		if(!$(e.target).parents().andSelf().is("header .bottom")) {

			$("header .bottom").removeClass("active");
			$("header a.search").removeClass("active");


		}


		if(!$(e.target).parents().andSelf().is(".fakeSelectWrap")) {

			$(".fakeSelectWrap").removeClass("open");


		}


	});

	//contact form, remove default value on click
	$(".contactForm input[type=text], .contactForm input[type=email], .contactForm textarea").each(function(){

		$(this).data("value", $(this).val());

		$(this).focus(function(){

			if ($(this).val() == $(this).data("value")) {
				$(this).val("");
			}

		});

		$(this).blur(function(){

			if ($(this).val().length == 0){
				$(this).val($(this).data("value"));
			}

		});

	});

	/* window scroll */
	$(window).scroll(function(e){

		var s = $(window).scrollTop();
		var hero = $("#hero");

		/* hero image parallax */
		//hero.css({"background-position":"0 -"+(s/4)+"px"});
		//hero.find(".container").css({"top":"-"+(s/3)+"px"});


		//header
		if($("main").hasClass("home")){

			if(s > $("header").height()){

				$("header").addClass("start");

			}
			else{

				$("header").removeClass("start");

			}

			if(($("#hero .caption .search").offset().top) < s){

				$("header").addClass("change");

			}
			else {

				$("header").removeClass("change");

			}

			//if(($("#jobTypes").offset().top - $("header.change .top").height()) < s){
			if(($("#hero .caption h1").offset().top) < s){

				$("header").addClass("in");

			}
			else{

				$("header").removeClass("in");

			}

			//Jobs By mail Sticky
			stickyHeader(s);

		}

		if($("main").hasClass("advertise")){

			//Request More Info Sticky
			stickyHeader(s);

		}


	});

	/* window resize */
	$(window).resize(function(e) {

		//close the mobile menu if window is resized
		$("#page").removeClass("menu");

		main();

	});

	/*
	 * Main function. runs on init() and window.resize
	 *
	 */
	function main(){

		//listing sidebar fix
		if($("#list .results").length){

			$("#list .results").css("min-height", ($("#list aside").outerHeight(false) + 80));

		}

		//$("#hero.advertise").css("height", $(window).height());
		//$("main.advertise").css("margin-top", $(window).height());

	}

	/*
	 * Main initialize function. runs on document.ready
	 *
	 */
	function init(){

		//run the main function
		main();

		//setup custom select boxes
		selects();


		//company slider
		if($("#companies").length){


			slider = $("#companies .slider").slick({
				infinite: true,
				slidesToShow: 6,
				slidesToScroll: 6,
				responsive: [
					{
						breakpoint: 850,
						settings: {
							slidesToShow: 3,
							slidesToScroll: 3
						}
					},
					{
						breakpoint: 720,
						settings: {
							slidesToShow: 2,
							slidesToScroll: 2
						}
					}
				]
			});

		}


		if($("#about").length){

			$("#about .blocks").imagesLoaded( function() {
				$("#about .blocks").masonry({
					itemSelector: '.block',
					stamp: ".stamp",
					columnWidth: 1,
					transitionDuration: 0,
					gutter: 0
				});
			});

		}


		//Jobs By mail Sticky
		stickyHeader($(window).scrollTop());

		//reset home tabs
		if($(window).width() < 750){

			$("#jobTypes .tabs .tab").removeClass("active");


			if($("#browse .paging").length){

				$("#browse .paging").animate({
					scrollLeft: ($("#browse .paging a.current").offset().left - ($("#browse .paging").width() / 2))
				}, 100);

			}

		}


	}



	//function to check if a string contains numbers only
	function isNumeric(n) {
		return !isNaN(parseFloat(n)) && isFinite(n);
	}



	//set a wrapper around all select boxes with .custom_select class
	function selects(){

		if ($(".custom_select").length > 0){

			$(".custom_select").each(function (index, el) {
				var $select = (!$(this).hasClass("gfield")) ? $(el) : $(this).find("select"); //check if gravity forms
				var $wrapper = $("<div class='fakeSelectWrap' />");
				var $fake_select = $("<div class='fakeSelect'></div>");
				var $fake_dropdown = $("<div class='fakeDropdown'></div>");

				var selected_text = $select.find("option:selected").text();
				var max_characters = 25;//max character length on the dropdown
				var classes = (selected_text.length > max_characters) ? "long" : "";

				$select.wrap($wrapper);
				$select.after($fake_select);
				$select.after($fake_dropdown);

				$select.parent().addClass($select.attr("name"));

				// set selected value as default
				if($select.val() !== ""){
					$fake_select.addClass("selected").html(selected_text);
				}
				else{
					$fake_select.html(selected_text);
				}
				//.css({"width":$select.width()}); //Auto size

				/*
				 * Loop through select options.
				 * Limit the length
				 *
				 */
				$select.find("option").each(function(index){
					var text = $(this).text(), sup = "";
					var classes = (text.length > max_characters) ? "long" : "";

					if(index != 0){

						if($select.hasClass("cv")){

							classes = getUploadType(text);

							sup = (index > 1) ? "<sup>TM</sup>" : "";

						}

						//Duplicate each option to the fake dropdown menu
						$fake_dropdown.append("<span data-id='"+$(this).val()+"' class='"+classes+"'>"+text+sup+"</span>");

					}

				});



				// change handler
				$select.change(function (){
					var text = $(this).find("option:selected").text();
					var classes = (text.length > 12) ? "long" : "";

					//set the selected options
					$fake_select.html(text).addClass(classes);
					$fake_select.append("<span></span>");
				});

				//add a class on focus
				$select.focus(function (){
					$fake_select.addClass("focus");
				}).focusout(function (){
					$fake_select.removeClass("focus");
				});

				//dropdown icon
				$fake_select.append("<span></span>")


				//add the icons for each cv upload type


			});

		}

		//
		$("body").on("click", ".fakeSelect", function(){
			$(".fakeSelectWrap").removeClass("open");

			$(this).parent().toggleClass("open");

		});

		//set value when dropdown selected
		$("body").on("click", ".fakeDropdown span", function(){
			var value = $(this).text();
			var classes = (value.length > 12) ? "long" : "";
			var id = $(this).data("id");



			if($(this).attr("class")){

				value = value.replace("TM", "<sup>TM</sup>");

				$(this).parent().siblings(".fakeSelect").removeAttr("class").addClass("fakeSelect").addClass("selected");

				classes = $(this).attr("class");

			}

			//set the fake value
			$(this).parent().siblings(".fakeSelect").addClass(classes).empty().html(value);
			$(this).parent().siblings(".fakeSelect").append("<span></span>");

			//set the real value to select box
			$(this).parent().siblings("select").val(id);

			$(this).parent().parent().toggleClass("open");

			//remove error class
			$(this).parent().parent().parent().parent().removeClass("error");

			if(!$(this).parent().parent().hasClass("sub")){

				//display sub categories
				$(".fakeDropdown span").removeClass("display");

				if(isNumeric(id)){

					$(".fakeDropdown span[data-parent="+id+"]").addClass("display");

				}

			}

		});

		// Upload csv dropbox. Get the icon
		function getUploadType(text){
			var result;

			if(text.indexOf("device") !== -1){

				result = "device";

			}

			if(text.indexOf("Google") !== -1){

				result = "google";

			}

			if(text.indexOf("OneDrive") !== -1){

				result = "onedrive";

			}

			if(text.indexOf("Dropbox") !== -1){

				result = "drop";

			}

			if(text.indexOf("Box") !== -1){

				result = "box";

			}

			return result;

		}

	}



	/*
	 *  Jobs By Email Button Sticky
	 *
	 */
	function stickyHeader(s){

		//Home Page
		if($("main").hasClass("home")){

			if(($("#hero .caption .search .inner").offset().top) > s){
				$("#openSubscribe").removeClass("fixed");
				$("#subscribe").removeClass("fixed");
			}
			else{

				$("#openSubscribe").addClass("fixed");
				$("#subscribe").addClass("fixed");

			}

		}

		//Advertise Landing Page
		if($("main").hasClass("advertise")){

			if(($("#hero.advertise h2").offset().top) > s){
				$("#openRequestMore").removeClass("fixed");
				$("#requestMore").removeClass("fixed");
			}
			else{

				$("#openRequestMore").addClass("fixed");
				$("#requestMore").addClass("fixed");

			}

		}

	}





});

$(window).load(function(){

	//add the loader class
	$("#page").addClass("load");

});

