$(document).ready(function(){

    //Province dropdown change
    $("body").on("click", ".Province .fakeDropdown span", function(){
        var id = $(this).data("id");

        changeCityDropdown(id);

    });

    //Province mobile dropdown change
    $("body").on("change", ".Province select", function(){
        var id = $(this).val();

        changeCityDropdown(id);

    });

    //
    function changeCityDropdown(id){
        var cities = [{"id":"1","name":"Durban","parentID":"4"}, {"id":"1","name":"Johannesburg","parentID":"7"}]; //TEST.list of cities


        //reset dropdowns
        $(".City select").empty().val("");
        $(".City .fakeDropdown").empty();
        $(".City .fakeSelect").empty().append("Select<span></span>");

        //loop through city data
        $.each(cities, function (i, city) {

            if(id == city.parentID){

                $(".City select").append("<option value='"+city.id+"'>"+city.name+"</option>");
                $(".City .fakeDropdown").append("<span data-id='"+city.id+"'>"+city.name+"</span>");

            }

        });


    }

});