$(document).ready(function(){

    $("#slider").slider({
        value:10,
        min: 1,
        max: 100,
        step: 1,
        range: "min",
        slide: function( event, ui ) {
            var adcount = ui.value,
                rate = 50;

            $("#jobCount").text(adcount);
            $("#saving").text("$75");
            $("#totalCost").text("$"+(adcount*rate));

        }
    });

});