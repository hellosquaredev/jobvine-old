<!DOCTYPE html>
<!--[if lt IE 7]>      <html lang="en" class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html lang="en" class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html lang="en" class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Jobvine</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

    <link rel="profile" href="http://gmpg.org/xfn/11">

    <!-- //Bootstrap
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">>
    -->

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">


    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css"/>

    <link rel="stylesheet" href="style.css">

    <link rel="shortcut icon" href="jobvine_favicon.ico" type="image/x-icon" >

    <!--[if lt IE 9]>
    <script src="js/vendor/html5shiv.js"></script>
    <script src="js/vendor/respond.js"></script>
    <![endif]-->

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>


    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', '', 'auto');
        ga('send', 'pageview');
    </script>

</head>

<body>

<div id="root"></div>

<!--[if lt IE 9]>
<p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/" target="_blank">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<div id="page">

    <header class="fixed">

        <div class="top">

            <div class="container">

                <div class="left">

                    <div class="logo"><a href="">Jobvine</a></div>

                    <div class="clear"></div>

                </div><!-- left -->

                <div class="right">

                    <p class="contact"><span class="label">Contact us on</span> <a href="tel:0217138000">+254(0)21 713 8000</a></p>

                </div><!-- right -->

                <div class="clear"></div>

            </div><!-- end container -->

        </div><!-- top -->

        <div class="bottom">

            <div class="search">

                <div class="inner">

                    <form  method="post">

                        <fieldset>

                            <input type="text" placeholder="Keywords (skills, job title etc)"/>

                            <input type="text" placeholder="Location (town, city etc)"/>

                            <input type="submit" value="Find Yours" class="btn btnBlue"/>

                        </fieldset>

                    </form>

                </div><!-- inner -->

            </div><!-- search -->

        </div><!-- bottom -->

    </header><!-- end header -->


    <div id="navigation">

        <a href="" class="respMenu"><div class="bars"></div></a>

        <div class="container">

            <div class="inner">

                <span class="header login">Login or Sign Up</span>

                <ul>

                    <li><a href="">Jobseekers</a></li>

                    <li><a href="">Recruiters</a></li>

                </ul>

                <a href="" class="btn btnWhiteB">Upload Your CV</a>

                <div class="recruiters">

                    <span class="header">Are You Recruiting?</span>

                    <a href="" class="btn btnCyan">Post A Job</a>

                </div><!-- recruiters -->


            </div><!-- inner -->

        </div><!-- container -->

    </div><!-- end navigation -->

    <!--//large hero image -->
    <section id="hero" class="advertise">

        <div class="container">

            <div class="caption">

                <div class="inner">

                    <h1>Hiring Made Easy</h1>

                    <p>We have 74,447 candidates looking for a job in Kenya</p>

                    <a href="#" class="btn btnCyan">Advertise Now</a>

                    <div class="line"></div>

                    <h2>We have the solution that <br/>suits your needs</h2>

                    <div class="actions">

                        <div class="action">

                            <div class="inner">

                                <img src="img/landing/icon_advertise_w.png" alt="Advertise Jobs" class="icon"/>

                                <h3>Advertise Jobs</h3>

                                <p>Post a job from $30 per month for 28 days</p>

                                <a href="" class="btn btnWhiteB">Tell Me More</a>

                            </div><!-- inner -->

                        </div><!-- action -->

                        <div class="action">

                            <div class="inner">

                                <img src="img/landing/icon_file_w.png" alt="Search CV's" class="icon"/>

                                <h3>Search CV’s</h3>

                                <p>Access to full our full database form $30 pm</p>

                                <a href="" class="btn btnWhiteB">Tell Me More</a>

                            </div><!-- inner -->

                        </div><!-- action -->

                        <div class="action">

                            <div class="inner">

                                <img src="img/landing/icon_alerts_w.png" alt="Skill Alerts" class="icon"/>

                                <h3>Skill Alerts</h3>

                                <p>Relevant candidates to your inbox from only 30$ pm</p>

                                <a href="" class="btn btnWhiteB">Tell Me More</a>

                            </div><!-- inner -->

                        </div><!-- action -->

                        <div class="clear"></div>

                        <div class="pre"></div>

                    </div><!-- actions -->

                </div><!-- inner -->

            </div><!-- caption -->

        </div><!-- container -->

        <div id="openRequestMore"><a href="" class="btn btnBlue">Request More Info</a></div><!-- request more -->

        <div id="requestMore">

            <div class="inner">

                <h3>We are here to help</h3>

                <p>Request a calll back, one of our team will get in touch shortly.</p>

                <form  method="post">

                    <fieldset>

                        <input type="text" placeholder="Name"/>

                        <input type="email" placeholder="Email"/>

                        <input type="text" placeholder="Tel No"/>

                        <input type="submit" value="Send Request" class="btn btnBlue"/>

                    </fieldset>

                </form>

                <a href="" class="close"></a>

            </div><!-- inner -->

            <div class="notification">

                <div class="icon"></div>

                <p>Request Sent</p>

            </div><!-- notification -->

        </div><!-- subscrbie -->

    </section><!-- end hero -->


    <!--// main content body -->
    <main class="page advertise">

        <section id="companies" class="content advertise">

            <div class="container l1">

                <div class="title">

                    <h2>Some Of The Great Companies <br/>Using Jobvine</h2>

                </div><!-- title -->

                <div class="wrapper">

                    <div class="image"><img src="img/home/og_logo.jpg" alt="ogilvy"/></div>

                    <div class="image"><img src="img/home/google_logo.jpg" alt="Google Logo"/></div>

                    <div class="image"><img src="img/home/mrp_logo.png" alt="Mr Price Logo"/></div>

                    <div class="image"><img src="img/home/edcon_logo.png" alt="Edcon Logo"/></div>

                    <div class="image"><img src="img/home/hrt_logo.png" alt="HRT & Carter Logo"/></div>

                    <div class="image"><img src="img/home/tmg_logo.png" alt="Times Media Group"/></div>

                </div><!-- wrapper -->

            </div><!-- container -->

        </section><!-- companies -->

        <section id="links" class="content">

            <div class="container l1">

                <div class="item">

                    <div class="title icon">

                        <img src="img/landing/icon_people.png" alt="Advertise Jobs"/>

                        <div class="copy">

                            <h1>Advertise Jobs</h1>

                            <p>Advertise a vacancy to the Jobvine audience. </p>

                        </div><!-- copy -->

                    </div><!-- title -->

                    <div class="features">

                        <ul>

                            <li>Job ad posting with logo</li>

                            <li>Applications in your inbox & account</li>

                            <li>Filter-Questionnaire & auto-reject criteria</li>

                            <li>Bulk & individual ad management</li>

                            <li>Filter of applications with keyword search</li>

                            <li>Candidate search & CV downloads</li>

                        </ul>

                    </div><!-- features -->

                    <div class="priceBox">

                        <div class="priceWrapper">

                            <span class="label">From</span>
                            <span class="currency">$</span>
                            <span class="amount">30</span>

                        </div><!-- price -->

                        <a href="" class="btn btnCyan">Start Free Trial</a>

                        <span class="note">Free trial includes 1 job post for 28 days</span>

                    </div><!-- price box -->

                    <div class="clear"></div>

                </div><!-- item -->

                <div class="item">

                    <div class="title icon">

                        <img src="img/landing/icon_file.png" alt="Search CV's"/>

                        <div class="copy">

                            <h1>Search CV’s</h1>

                            <p>CV search on the go. Find highly qualified candidates in our database and invite them to apply.</p>

                        </div><!-- copy -->

                    </div><!-- title -->

                    <div class="features">

                        <ul>

                            <li>Job ad posting with logo</li>

                            <li>Applications in your inbox & account</li>

                            <li>Filter-Questionnaire & auto-reject criteria</li>

                            <li>Bulk & individual ad management</li>

                            <li>Filter of applications with keyword search</li>

                            <li>Candidate search & CV downloads</li>

                        </ul>

                    </div><!-- features -->


                    <div class="priceBox">

                        <div class="priceWrapper">

                            <span class="label">From</span>
                            <span class="currency">$</span>
                            <span class="amount">30</span>

                        </div><!-- price -->

                        <a href="" class="btn btnCyan">Start Free Trial</a>

                        <span class="note">Free trial includes 1 job post for 28 days</span>

                    </div><!-- price box -->

                    <div class="clear"></div>

                </div><!-- item -->

                <div class="item">

                    <div class="title icon">

                        <img src="img/landing/icon_alerts.png" alt="Skill Alerts"/>

                        <div class="copy">

                            <h1>Skill Alerts</h1>

                            <p>Let us alert you when the right candidate adds their CV.</p>

                        </div><!-- copy -->

                    </div><!-- title -->

                    <div class="features">

                        <ul>

                            <li>Job ad posting with logo</li>

                            <li>Applications in your inbox & account</li>

                            <li>Filter-Questionnaire & auto-reject criteria</li>

                            <li>Bulk & individual ad management</li>

                            <li>Filter of applications with keyword search</li>

                            <li>Candidate search & CV downloads</li>

                        </ul>

                    </div><!-- features -->

                    <div class="priceBox">

                        <div class="priceWrapper">

                            <span class="label">From</span>
                            <span class="currency">$</span>
                            <span class="amount">30</span>

                        </div><!-- price -->

                        <a href="" class="btn btnCyan">Start Free Trial</a>

                        <span class="note">Free trial includes 1 job post for 28 days</span>

                    </div><!-- price box -->

                    <div class="clear"></div>

                </div><!-- item -->

            </div><!-- container -->

        </section>


    </main><!-- end main -->


    <!--//footer -->
    <footer>

        <div class="tagline">

            <p>Be First <span></span> Be Fast <span></span> Be Smart</p>

        </div><!-- tag line -->

        <div class="container l1">

            <div class="top">

                <div class="threeColumn">

                    <div class="col one">

                        <h3>JobVine Global</h3>

                        <p>At Jobvine our goal is to help you make the most of the 80 or 90 years you have on this planet by connecting you to the real world opportunities that can help you achieve your goals and realize your dreams. Visit <a href="">Jobvine.com</a></p>

                    </div><!-- col -->

                    <div class="col two">

                        <h3>JobVine Blog</h3>

                        <p>News, views, career advice and interview tips. And more</p>

                    </div><!-- col -->

                    <div class="col three">

                        <h3>For Employers</h3>

                        <ul>

                            <li><a href="">Post a Job</a></li>

                            <li><a href="">Products & Services</a></li>

                            <li><a href="">Contact Us</a></li>

                        </ul>

                    </div><!-- col -->

                    <div class="clear"></div>

                </div><!-- three column -->

            </div><!-- top -->

            <div class="bottom">

                <div class="left">

                    <ul class="nav">

                        <li><a href="#">About Us</a></li>

                        <li><a href="#">Contact Us</a></li>

                        <li><a href="#">Terms and Conditions</a></li>

                        <li><a href="#">Testimonials</a></li>


                    </ul>

                    <div class="clear"></div>

                    <p>&#169; <?php echo date("Y");?>. JobVine.co.za All Right Reserved.  C/O Mauritius International Trust Company Limited, <br/>4th Floor, Ebene Skies, Rue de I'institut, Ebene, Mauritius</p>

                </div><!-- left -->


                <ul class="social">

                    <li><a href="#" class="twitter" target="_blank"></a></li>

                    <li><a href="#" class="fb" target="_blank"></a></li>

                    <li><a href="#" class="linkedin" target="_blank"></a></li>

                    <li><a href="#" class="gplus" target="_blank"></a></li>

                </ul><!-- end social -->


                <div class="clear"></div>

            </div><!-- bottom -->

            <div class="clear"></div>

        </div><!-- container -->

    </footer><!-- end footer -->



</div><!-- end page -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>

<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-color/2.1.2/jquery.color.min.js"></script>


<script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"></script>

<script src="js/main.js"></script>

</body>
</html>

