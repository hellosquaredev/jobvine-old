<!DOCTYPE html>
<!--[if lt IE 7]>      <html lang="en" class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html lang="en" class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html lang="en" class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Jobvine</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

    <link rel="profile" href="http://gmpg.org/xfn/11">

    <!-- //Bootstrap
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">>
    -->

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">


    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css"/>

    <link rel="stylesheet" href="style.css">

    <link rel="shortcut icon" href="jobvine_favicon.ico" type="image/x-icon" >

    <!--[if lt IE 9]>
    <script src="js/vendor/html5shiv.js"></script>
    <script src="js/vendor/respond.js"></script>
    <![endif]-->

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>


    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', '', 'auto');
        ga('send', 'pageview');
    </script>

</head>

<body>

<div id="root"></div>

<!--[if lt IE 9]>
<p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/" target="_blank">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<div id="page">

    <header class="fixed change in">

        <div class="top">

            <div class="container">

                <div class="left">

                    <div class="logo"><a href="">Jobvine</a></div>

                    <ul>

                        <li class="dropdown">

                            <a href="">Jobseekers</a>

                            <div class="wrapper">

                                <div class="loginForm inner">

                                    <span class="header">Jobseekers Login</span>

                                    <form  method="post">

                                        <fieldset>

                                            <input type="email" placeholder="Email Address"/>

                                            <input type="password" placeholder="Password"/>

                                            <input type="submit" value="Login" class="btn btnBlue"/>

                                        </fieldset>

                                    </form>

                                    <a href="#" class="forgot">Forgot Password?</a>

                                    <div class="clear"></div>

                                </div><!-- inner -->

                                <div class="registerAction inner">

                                    <span class="header">Not a Member?</span>

                                    <a href="" class="btn btnDBlue">Register Here</a>

                                </div><!-- inner -->

                            </div><!-- wrapper -->

                        </li>

                        <li class="dropdown">

                            <a href="">Recruiters</a>

                            <div class="wrapper">

                                <div class="loginForm inner">

                                    <span class="header">Recruiters Login</span>

                                    <form  method="post">

                                        <fieldset>

                                            <input type="email" placeholder="Email Address"/>

                                            <input type="password" placeholder="Password"/>

                                            <input type="submit" value="Login" class="btn btnBlue"/>

                                        </fieldset>

                                    </form>

                                    <a href="#" class="forgot">Forgot Password?</a>

                                    <div class="clear"></div>

                                </div><!-- inner -->

                                <div class="registerAction inner">

                                    <span class="header">Not a Member?</span>

                                    <a href="" class="btn btnDBlue">Register Here</a>

                                </div><!-- inner -->

                            </div><!-- wrapper -->

                        </li>

                    </ul>

                    <div class="clear"></div>

                </div><!-- left -->


                <div class="right">

                    <a href="#" class="respMenu"><div class="bars"></div></a>

                    <a href="#" class="search mobile"><i class="fa fa-search" aria-hidden="true"></i></a>

                    <div class="notifications">

                        <div class="icon"></div>

                        <div class="count">1</div>

                        <div class="dropdown">

                            <span class="header">YAY! you have 1 new notification</span>

                            <div class="content">

                                <ul>

                                    <li><a href=""><strong>Sign up</strong> in seconds and find a job you’ll love!</a></li>

                                </ul>

                            </div><!-- content -->

                        </div><!-- dropdown -->

                    </div><!-- notifications -->

                    <a href="" class="btn btnWhiteB uploadCV">Upload Your CV</a>

                    <div class="userNav">

                        <div class="top">

                            <div class="sym">

                                <span>C</span>

                            </div><!-- sym -->

                            <span class="name">Chantel</span>

                            <span class="arrow"></span>

                        </div>

                        <div class="dropdown">

                            <ul>

                                <li><a href="">Edit Profile</a></li>

                                <li><a href="">Job Alerts</a></li>

                                <li><a href="">Job Applications</a></li>

                                <li><a href="">Freelance Profile</a></li>

                                <li class="logout"><a href="">Logout</a></li>

                            </ul>

                        </div>

                    </div><!-- user nav -->


                    <div class="clear"></div>

                </div><!-- right -->

                <div class="clear"></div>

            </div><!-- end container -->

        </div><!-- top -->

        <div class="bottom">

            <div class="search">

                <div class="inner">

                    <form  method="post">

                        <fieldset>

                            <span class="header">Search Jobs</span>

                            <input type="text" placeholder="Keywords (skills, job title etc)"/>

                            <input type="text" placeholder="Location (town, city etc)"/>

                            <input type="submit" value="Find Yours" class="btn btnBlue"/>

                        </fieldset>

                    </form>

                </div><!-- inner -->

            </div><!-- search -->

        </div><!-- bottom -->

    </header><!-- end header -->

    <div id="navigation">

        <a href="" class="respMenu"><div class="bars"></div></a>

        <div class="container">

            <div class="inner">

                <span class="header login">Login or Sign Up</span>

                <ul>

                    <li><a href="">Jobseekers</a></li>

                    <li><a href="">Recruiters</a></li>

                </ul>

                <a href="" class="btn btnWhiteB">Upload Your CV</a>

                <div class="recruiters">

                    <span class="header">Are You Recruiting?</span>

                    <a href="" class="btn btnCyan">Post A Job</a>

                </div><!-- recruiters -->


            </div><!-- inner -->

        </div><!-- container -->

    </div><!-- end navigation -->


    <!--// main content body -->
    <main class="page">

        <div id="breadcrumbs" class="border">

            <div class="container l0">

                <ul>

                    <li><a href="">Jobs</a></li>

                    <li>&gt;</li>

                    <li><a href="">KwaZulu-Natal</a></li>

                    <li>&gt;</li>

                    <li>Engineering Jobs</li>

                </ul>

                <a href="" class="backToResults">Back to Search Results</a>

                <div class="clear"></div>

            </div><!-- container -->

        </div><!-- meta -->

        <section id="detail">

            <div class="container l0">

                <div class="main" itemscope="itemscope" itemtype="http://schema.org/JobPosting">

                    <div class="jobOffer">

                        <h1 itemprop="title">Structural Engineer</h1>

                        <span class="location" itemprop="jobLocation">Durban Western Suburbs</span>

                        <span class="salary" itemprop="baseSalary">R30 000 - R35 000 Per Month (Negotiable)</span>

                        <ul class="jobDetails">

                            <li><strong>Job Type:</strong> <span itemprop="employmentType">Permanent</span></li>

                            <li><strong>Sectors:</strong> Construction, Engineering, Mining</li>

                            <li><strong>Posted by:</strong> <a href="" itemprop="hiringOrganization" itemscope itemtype="http://schema.org/Organization">Cap Personnel</a> on Wednesday, July 27, 2016</li>

                            <li><strong>Reference:</strong> BMPinetown</li>

                        </ul>

                        <p class="closingDate">Apply before Wednesday, October 5, 2016, <strong class="daysLeft">61 Days left</strong></p>

                        <img src="img/listing/mass_logo.png" alt="" class="companyLogo"/>

                        <a href="" class="btn btnBlue applyNow">Apply For This Job</a>

                        <a href="#" class="reportListing">Report Job</a>

                        <div class="clear"></div>

                    </div><!-- job offer -->

                    <div class="jobInfo" itemprop="responsibilities">

                        <div class="alert">

                            <span class="header">Alert Me of Jobs Like These</span>

                            <a href="#" class="closeAlert"></a>

                            <div class="form">

                                <div class="inner">

                                    <form  method="post">

                                        <fieldset>

                                            <input type="text" placeholder="Your Email"/>

                                            <input type="text" placeholder="Location"/>

                                            <input type="submit" value="Create Alert" class="btn btnDBlue"/>

                                        </fieldset>

                                    </form>

                                </div><!-- inner -->

                            </div><!-- form -->

                            <div class="notification">

                                <div class="icon"></div>

                                <p>Job alert created</p>

                            </div><!-- notification -->

                        </div><!-- alert -->


                        <span class="header">Job Details</span>

                        <p>Employer: <a href="">Peoplefinder Career Placements</a><br/>Our client, a leading manufacturer requires the services of a Maintenance Technician with Tetra experience</p>


                        <span class="header">Key Performance Areas</span>

                        <ul>

                            <li>Increase machine availability</li>

                            <li>Ensure modifications comply with standards</li>

                            <li>Maintain and repair instrumentation, process control equipment and automation systems</li>

                            <li>Optimise control systems regarding processes and production lines</li>

                            <li>Provide technical and specialist support</li>

                            <li>Inspect and test equipment as per prescribed ELKE schedule</li>

                            <li>Determine causes of failures (fault finding)</li>

                            <li>Ensure that all process control software is backed up and kept up to date</li>

                            <li>Perform preventative maintenance</li>

                            <li>Compile standby lists</li>

                            <li>Complete job cards and update records on ELKE</li>

                        </ul>


                        <span class="header">Candidate Requirements</span>

                        <ul>

                            <li>Matric with at least N5 or other relevant qualifications</li>

                            <li>Must have Tetra experience</li>

                            <li>Up to 3 years’ relevant experience on machinery – Filler and downstream machines</li>

                            <li>Knowledge and experience on Siemens S5 and S7</li>

                            <li>Knowledge and experience on SCADA systems</li>

                            <li>Must be prepared to work shifts, overtime, public holidays and weekends when</li>

                            <li>Excellent computer skills (MS Office)</li>

                            <li>Must be innovative, analytical and pro-active</li>

                            <li>Must be able to work under pressure</li>

                            <li>Good at troubleshooting and fault-finding</li>

                            <li>Excellent written & verbal communication skills</li>

                        </ul>

                        <p class="closingDate">Apply before Wednesday, October 5, 2016, <strong class="daysLeft">61 Days left</strong></p>

                        <div class="clear"></div>

                        <p>Companies may expire jobs at their own discretion.</p>

                        <a href="" class="btn btnBlue applyNow">Apply For This Job</a>

                    </div><!-- job info -->

                    <div class="more">

                        <a href="" class="btn btnDBlueB">Accounting jobs in Cape Town</a>

                        <a href="" class="btn btnDBlueB">Accounting jobs</a>

                        <a href="" class="btn btnDBlueB">Jobs in Cape Town</a>

                    </div><!-- more -->

                </div><!-- main -->

                <aside>

                    <div class="header">Similar Jobs</div>

                    <div class="inner">

                        <div class="block">

                            <a href="">

                                <h3>Branch Manager</h3>

                                <span class="location">Pinetown</span>

                                <span class="salary">R35 000 - R47 000 Per Month</span>

                                <p>(Negotiable) Benefits: Medical Aid, Pension, Car Allowance</p>

                            </a>

                        </div><!-- block -->

                        <div class="block">

                            <a href="">

                                <h3>Branch Manager</h3>

                                <span class="location">Pinetown</span>

                                <span class="salary">R35 000 - R47 000 Per Month</span>

                                <p>(Negotiable) Benefits: Medical Aid, Pension, Car Allowance</p>

                            </a>

                        </div><!-- block -->


                        <div class="block">

                            <a href="">

                                <h3>Branch Manager</h3>

                                <span class="location">Pinetown</span>

                                <span class="salary">R35 000 - R47 000 Per Month</span>

                                <p>(Negotiable) Benefits: Medical Aid, Pension, Car Allowance</p>

                            </a>

                        </div><!-- block -->



                    </div><!-- inner -->

                    <div class="header">Jobs From Recruiter</div>

                    <div class="inner">

                        <div class="block">

                            <a href="">

                                <h3>Branch Manager</h3>

                                <span class="location">Pinetown</span>

                                <span class="salary">R35 000 - R47 000 Per Month</span>

                                <p>(Negotiable) Benefits: Medical Aid, Pension, Car Allowance</p>

                            </a>

                        </div><!-- block -->

                        <div class="block">

                            <a href="">

                                <h3>Branch Manager</h3>

                                <span class="location">Pinetown</span>

                                <span class="salary">R35 000 - R47 000 Per Month</span>

                                <p>(Negotiable) Benefits: Medical Aid, Pension, Car Allowance</p>

                            </a>

                        </div><!-- block -->


                        <div class="block">

                            <a href="">

                                <h3>Branch Manager</h3>

                                <span class="location">Pinetown</span>

                                <span class="salary">R35 000 - R47 000 Per Month</span>

                                <p>(Negotiable) Benefits: Medical Aid, Pension, Car Allowance</p>

                            </a>

                        </div><!-- block -->



                    </div><!-- inner -->

                </aside>



                <div class="clear"></div>

            </div><!-- container -->

        </section><!-- listing -->

    </main><!-- end main -->


    <!--//footer -->
    <footer>

        <div class="tagline">

            <p>Be First <span></span> Be Fast <span></span> Be Smart</p>

        </div><!-- tag line -->

        <div class="container l1">

            <div class="top">

                <div class="threeColumn">

                    <div class="col one">

                        <h3>JobVine Global</h3>

                        <p>At Jobvine our goal is to help you make the most of the 80 or 90 years you have on this planet by connecting you to the real world opportunities that can help you achieve your goals and realize your dreams. Visit <a href="">Jobvine.com</a></p>

                    </div><!-- col -->

                    <div class="col two">

                        <h3>JobVine Blog</h3>

                        <p>News, views, career advice and interview tips. And more</p>

                    </div><!-- col -->

                    <div class="col three">

                        <h3>For Employers</h3>

                        <ul>

                            <li><a href="">Post a Job</a></li>

                            <li><a href="">Products & Services</a></li>

                            <li><a href="">Contact Us</a></li>

                        </ul>

                    </div><!-- col -->

                    <div class="clear"></div>

                </div><!-- three column -->

            </div><!-- top -->

            <div class="bottom">

                <div class="left">

                    <ul class="nav">

                        <li><a href="#">About Us</a></li>

                        <li><a href="#">Contact Us</a></li>

                        <li><a href="#">Terms and Conditions</a></li>

                        <li><a href="#">Testimonials</a></li>


                    </ul>

                    <div class="clear"></div>

                    <p>&#169; <?php echo date("Y");?>. JobVine.co.za All Right Reserved.  79 Roeland street, Cape Town, South Africa</p>

                </div><!-- left -->


                <ul class="social">

                    <li><a href="#" class="twitter" target="_blank"></a></li>

                    <li><a href="#" class="fb" target="_blank"></a></li>

                    <li><a href="#" class="linkedin" target="_blank"></a></li>

                    <li><a href="#" class="gplus" target="_blank"></a></li>

                </ul><!-- end social -->


                <div class="clear"></div>

            </div><!-- bottom -->

            <div class="clear"></div>

        </div><!-- container -->

    </footer><!-- end footer -->



</div><!-- end page -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>

<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-color/2.1.2/jquery.color.min.js"></script>


<script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"></script>



<script src="js/main.js"></script>


</body>
</html>
