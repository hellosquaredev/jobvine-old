<!DOCTYPE html>
<!--[if lt IE 7]>      <html lang="en" class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html lang="en" class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html lang="en" class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Jobvine</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

    <link rel="profile" href="http://gmpg.org/xfn/11">

    <!-- //Bootstrap
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">>
    -->

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">


    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css"/>

    <link rel="stylesheet" href="style.css">

    <link rel="shortcut icon" href="jobvine_favicon.ico" type="image/x-icon" >

    <!--[if lt IE 9]>
    <script src="js/vendor/html5shiv.js"></script>
    <script src="js/vendor/respond.js"></script>
    <![endif]-->

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>


    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', '', 'auto');
        ga('send', 'pageview');
    </script>

</head>

<body>

<div id="root"></div>

<!--[if lt IE 9]>
<p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/" target="_blank">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<div id="page" class="loggedIn showNotice">

    <div id="notice">

        <p>Credit promo code: 1289675 - Redeem Now!</p>

        <a href="" class="close"></a>

    </div><!-- notice -->

    <header class="fixed change in">

        <div class="top">

            <div class="container">

                <div class="left">

                    <div class="logo"><a href="">Jobvine</a></div>

                    <div class="pageName">Recruiter Control Panel</div>

                    <div class="clear"></div>

                </div><!-- left -->

                <div class="right">

                    <a href="#" class="respMenu"><div class="bars"></div></a>

                    <div class="userNav">

                        <div class="top">

                            <div class="sym">

                                <span>H</span>

                            </div><!-- sym -->

                            <span class="name">Hellosquare</span>

                            <span class="arrow"></span>

                        </div>

                        <div class="dropdown">

                            <ul>

                                <li><a href="">My Profile</a></li>

                                <li><a href="">Post Job</a></li>

                                <li><a href="">Manage Jobs</a></li>

                                <li><a href="">CV Search</a></li>

                                <li><a href="">CV's Downloaded</a></li>

                                <li><a href="">Skills Alerts</a></li>

                                <li><a href="">Manage Agents</a></li>

                                <li><a href="">Buy Credits</a></li>

                                <li class="logout"><a href="">Logout</a></li>

                            </ul>

                        </div>

                    </div><!-- user nav -->


                    <div class="clear"></div>

                </div><!-- right -->

                <div class="clear"></div>

            </div><!-- end container -->

        </div><!-- top -->

        <div class="bottom overview">

            <nav class="links">

                <a href="">My Profile</a>

                <a href="">Post Job</a>

                <a href="">Manage Jobs</a>

                <a href="">CV Search</a>

                <a href="">CV's Downloaded</a>

                <a href="">Skills Alerts</a>

                <a href="" class="current">Manage Agents</a>

            </nav><!-- links -->

            <div class="overviewPanel">

                <div class="container l1">

                    <div class="wrapper">

                        <div id="jobCredits" class="block">

                            <span class="header">Job Credits</span>

                            <div class="box zero">

                                <a href="">
                                    <span class="value">0</span>
                                </a>

                                <a href="" class="link">Buy</a>

                            </div><!-- box -->

                        </div><!-- block -->

                        <div id="cvSearch" class="block">

                            <span class="header">CV Search</span>

                            <div class="box zero">

                                <a href="">
                                    <span class="value">31.06.16</span>
                                </a>

                                <a href="" class="link">Buy</a>

                            </div><!-- box -->

                        </div><!-- block -->

                        <div id="skillAlerts" class="block">

                            <span class="header">Skill Alerts</span>

                            <div class="box zero">

                                <a href="">
                                    <span class="value">33</span>
                                </a>

                                <a href="" class="link">Activate</a>

                            </div><!-- box -->

                        </div><!-- block -->

                        <div id="dailyCV" class="block">

                            <span class="header">Daily CV Limit</span>

                            <div class="box">
                                <span class="value">0 of 500</span>
                            </div><!-- box -->

                        </div><!-- block -->

                    </div><!-- wrapper -->

                    <div class="clear"></div>

                </div><!-- container -->

            </div><!-- overview -->

        </div><!-- bottom -->

    </header><!-- end header -->

    <div id="navigation">

        <a href="" class="respMenu"><div class="bars"></div></a>

        <div class="container">

            <div class="inner">

                <ul>

                    <li><a href="">My Profile</a></li>

                    <li><a href="">Post Job</a></li>

                    <li><a href="">Manage Jobs</a></li>

                    <li><a href="">CV Search</a></li>

                    <li><a href="">CV's Downloaded</a></li>

                    <li><a href="">Skills Alerts</a></li>

                    <li><a href="">Manage Agents</a></li>

                    <li><a href="">Buy Credits</a></li>

                    <li class="logout"><a href="">Logout</a></li>

                </ul>


            </div><!-- inner -->

        </div><!-- container -->

    </div><!-- end navigation -->


    <!--// main content body -->
    <main class="page recruiters">

        <div id="postJob" class="content">

            <div class="container l1">


                <div class="title">

                    <h1>Post a Job</h1>

                </div><!-- title -->

                <div class="form">

                    <form action="recruiter-post-job.php" method="post">


                        <div class="row">

                            <div class="block">

                                <h2>About your job</h2>

                                <ul>

                                    <li>

                                        <label>Job Title</label>

                                        <div class="field">

                                            <input type="text"/>

                                        </div><!-- field -->

                                    </li>

                                    <li>

                                        <label>Industry sector*</label>

                                        <div class="field">

                                            <select class="custom_select">

                                                <option selected="selected">Select</option>

                                                <option value="1">Option 1</option>


                                            </select>

                                        </div><!-- field -->

                                    </li>

                                    <li>

                                        <label>Type of job*</label>

                                        <div class="field">

                                            <select class="custom_select">

                                                <option selected="selected">Select</option>

                                                <option value="1">Option 1</option>


                                            </select>

                                        </div><!-- field -->

                                    </li>

                                    <li class="top">

                                        <label>Is this job</label>

                                        <div class="field">

                                            <label>

                                                <input type="checkbox"/>

                                                Suitable for a graduate trainee?

                                            </label>

                                            <label>

                                                <input type="checkbox"/>

                                                Within the Public Sector?

                                            </label>

                                        </div><!-- field -->

                                    </li>

                                </ul>

                            </div><!-- block -->

                            <div class="block">

                                <h2>Job location</h2>

                                <ul>

                                    <li>

                                        <label>Province*</label>

                                        <div class="field">

                                            <select class="custom_select" name="Province">

                                                <option selected="selected">Select</option>

                                                <option value="5">Eastern Cape</option>
                                                <option value="2">Free State</option>
                                                <option value="7">Gauteng</option>
                                                <option value="4">Kwazulu-Natal</option>
                                                <option value="6">Limpopo</option>
                                                <option value="9">Mpumalanga</option>
                                                <option value="3">Northern Cape</option>
                                                <option value="8">North-West</option>
                                                <option value="1">Western Cape</option>

                                            </select>

                                        </div><!-- field -->

                                    </li>

                                    <li>

                                        <label>City/Town*</label>

                                        <div class="field">

                                            <select class="custom_select" name="City">

                                                <option selected="selected">Select</option>

                                                <option value="1">Option 1</option>

                                            </select>

                                        </div><!-- field -->

                                    </li>

                                </ul>

                            </div><!-- block -->

                            <div class="clear"></div>

                        </div><!-- row -->

                        <div class="row">

                            <div class="block">

                                <h2>Salary details</h2>

                                <ul>

                                    <li class="top">

                                        <label>Salary Negotiable</label>

                                        <div class="field">

                                            <label>

                                                <input type="checkbox"/>

                                                Don't show

                                            </label>

                                        </div><!-- field -->

                                    </li>

                                    <li>

                                        <label>Salary*</label>

                                        <div class="field">

                                            <div class="col first">

                                                <input type="text"/>

                                            </div><!-- col -->

                                            <div class="col second">

                                                <select class="custom_select">

                                                    <option selected="selected">Per Month</option>

                                                    <option value="1">Option 1</option>


                                                </select>

                                            </div><!-- col -->

                                            <div class="clear"></div>

                                        </div><!-- field -->

                                    </li>

                                    <li class="top">

                                        <label>Salary Options*</label>

                                        <div class="field">

                                            <label>

                                                <input type="checkbox"/>

                                                OTE

                                            </label>

                                            <label>

                                                <input type="checkbox"/>

                                                Including Benefits

                                            </label>

                                            <label>

                                                <input type="checkbox"/>

                                                ProRata

                                            </label>

                                        </div><!-- field -->

                                    </li>

                                </ul>

                            </div><!-- block -->

                            <div class="block">

                                <h2>Organisation details</h2>

                                <ul>

                                    <li class="error">

                                        <label>Company Name*</label>

                                        <div class="field">

                                            <input type="text"/>

                                        </div><!-- field -->

                                    </li>

                                    <li>

                                        <label>First Name*</label>

                                        <div class="field">

                                            <input type="text"/>

                                        </div><!-- field -->

                                    </li>

                                    <li>

                                        <label>Surname*</label>

                                        <div class="field">

                                            <input type="text"/>

                                        </div><!-- field -->

                                    </li>

                                    <li>

                                        <label>Email*</label>

                                        <div class="field">

                                            <input type="email"/>

                                        </div><!-- field -->

                                    </li>

                                    <li>

                                        <label>Phone Number*</label>

                                        <div class="field">

                                            <input type="text"/>

                                        </div><!-- field -->

                                    </li>

                                </ul>

                            </div><!-- block -->

                            <div class="clear"></div>

                        </div><!-- row -->

                        <div class="row questions">

                            <div class="block full">

                                <h2>Required questions</h2>

                                <div class="wrapper">

                                    <div class="question">

                                        <label>Question 1</label>

                                        <div class="field">

                                            <input type="text"/>

                                        </div><!-- field -->

                                        <div class="field type">

                                            <select class="custom_select">

                                                <option selected="selected">Select</option>

                                                <option value="1">Any Answer</option>

                                                <option value="2">Only Yes</option>

                                                <option value="3">Only No</option>

                                            </select>

                                            <a href="#" class="add"></a>

                                        </div><!-- field -->

                                    </div><!-- question -->

                                </div><!-- questions -->

                            </div><!-- block -->

                        </div><!-- row -->

                        <div class="row description">

                            <div class="block full">

                                <h2>Job Description</h2>

                                <p class="note">Please note that no working from home jobs will get approved</p>

                                <textarea>



                                </textarea>

                            </div><!-- block -->

                        </div><!-- row -->

                        <div class="bottom">

                            <div class="discountWrapper">

                                <span class="text">Discount Code</span>

                                <input type="text"/>

                            </div><!-- discount wrapper -->

                            <div class="submit">

                                <span class="terms">

                                    <input type="checkbox"/>

                                    I accept the <a href="">Terms & Conditions.</a>

                                </span>

                                <input type="submit" class="btn btnBlue" value="Post Job">

                            </div><!-- submit -->

                            <div class="clear"></div>

                        </div><!-- bottom -->


                    </form>

                </div><!-- form -->

            </div><!-- container -->

        </div><!-- manage agents -->

    </main><!-- page -->

    <!--//footer -->
    <footer>

        <div class="tagline">

            <p>Be First <span></span> Be Fast <span></span> Be Smart</p>

        </div><!-- tag line -->

        <div class="container l1">

            <div class="top">

                <div class="threeColumn">

                    <div class="col one">

                        <h3>JobVine Global</h3>

                        <p>At Jobvine our goal is to help you make the most of the 80 or 90 years you have on this planet by connecting you to the real world opportunities that can help you achieve your goals and realize your dreams. Visit <a href="">Jobvine.com</a></p>

                    </div><!-- col -->

                    <div class="col two">

                        <h3>JobVine Blog</h3>

                        <p>News, views, career advice and interview tips. And more</p>

                    </div><!-- col -->

                    <div class="col three">

                        <h3>For Employers</h3>

                        <ul>

                            <li><a href="">Post a Job</a></li>

                            <li><a href="">Products & Services</a></li>

                            <li><a href="">Contact Us</a></li>

                        </ul>

                    </div><!-- col -->

                    <div class="clear"></div>

                </div><!-- three column -->

            </div><!-- top -->

            <div class="bottom">

                <div class="left">

                    <ul class="nav">

                        <li><a href="#">About Us</a></li>

                        <li><a href="#">Contact Us</a></li>

                        <li><a href="#">Terms and Conditions</a></li>

                        <li><a href="#">Testimonials</a></li>


                    </ul>

                    <div class="clear"></div>

                    <p>&#169; <?php echo date("Y");?>. JobVine.co.za All Right Reserved.  C/O Mauritius International Trust Company Limited, <br/>4th Floor, Ebene Skies, Rue de I'institut, Ebene, Mauritius</p>

                </div><!-- left -->


                <ul class="social">

                    <li><a href="#" class="twitter" target="_blank"></a></li>

                    <li><a href="#" class="fb" target="_blank"></a></li>

                    <li><a href="#" class="linkedin" target="_blank"></a></li>

                    <li><a href="#" class="gplus" target="_blank"></a></li>

                </ul><!-- end social -->


                <div class="clear"></div>

            </div><!-- bottom -->

            <div class="clear"></div>

        </div><!-- container -->

    </footer><!-- end footer -->



</div><!-- end page -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>

<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-color/2.1.2/jquery.color.min.js"></script>


<script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"></script>


<script src="js/main.js"></script>
<script src="js/repeater.js"></script>
<script src="js/dropdown.js"></script>


</body>
</html>
