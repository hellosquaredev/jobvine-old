<!DOCTYPE html>
<!--[if lt IE 7]>      <html lang="en" class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html lang="en" class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html lang="en" class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Jobvine</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

    <link rel="profile" href="http://gmpg.org/xfn/11">

    <!-- //Bootstrap
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">>
    -->

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">


    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css"/>

    <link rel="stylesheet" href="style.css">

    <link rel="shortcut icon" href="jobvine_favicon.ico" type="image/x-icon" >


    <!--[if lt IE 9]>
    <script src="js/vendor/html5shiv.js"></script>
    <script src="js/vendor/respond.js"></script>
    <![endif]-->

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>


    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', '', 'auto');
        ga('send', 'pageview');
    </script>

</head>

<body>

<div id="root"></div>

<!--[if lt IE 9]>
<p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/" target="_blank">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<div id="page">

    <header class="fixed change in">

        <div class="top">

            <div class="container">

                <div class="left">

                    <div class="logo"><a href="">Jobvine</a></div>

                    <ul>

                        <li class="dropdown">

                            <a href="">Jobseekers</a>

                            <div class="wrapper">

                                <div class="loginForm inner">

                                    <span class="header">Jobseekers Login</span>

                                    <form  method="post">

                                        <fieldset>

                                            <input type="email" placeholder="Email Address"/>

                                            <input type="password" placeholder="Password"/>

                                            <input type="submit" value="Login" class="btn btnBlue"/>

                                        </fieldset>

                                    </form>

                                    <a href="#" class="forgot">Forgot Password?</a>

                                    <div class="clear"></div>

                                </div><!-- inner -->

                                <div class="registerAction inner">

                                    <span class="header">Not a Member?</span>

                                    <a href="" class="btn btnDBlue">Register Here</a>

                                </div><!-- inner -->

                            </div><!-- wrapper -->

                        </li>

                        <li class="dropdown">

                            <a href="">Recruiters</a>

                            <div class="wrapper">

                                <div class="loginForm inner">

                                    <span class="header">Recruiters Login</span>

                                    <form  method="post">

                                        <fieldset>

                                            <input type="email" placeholder="Email Address"/>

                                            <input type="password" placeholder="Password"/>

                                            <input type="submit" value="Login" class="btn btnBlue"/>

                                        </fieldset>

                                    </form>

                                    <a href="#" class="forgot">Forgot Password?</a>

                                    <div class="clear"></div>

                                </div><!-- inner -->

                                <div class="registerAction inner">

                                    <span class="header">Not a Member?</span>

                                    <a href="" class="btn btnDBlue">Register Here</a>

                                </div><!-- inner -->

                            </div><!-- wrapper -->

                        </li>

                    </ul>

                    <div class="clear"></div>

                </div><!-- left -->


                <div class="right">

                    <a href="#" class="respMenu"><div class="bars"></div></a>

                    <a href="#" class="search mobile"><i class="fa fa-search" aria-hidden="true"></i></a>

                    <div class="notifications">

                        <div class="icon"></div>

                        <div class="count">1</div>

                        <div class="dropdown">

                            <span class="header">YAY! you have 1 new notification</span>

                            <div class="content">

                                <ul>

                                    <li><a href=""><strong>Sign up</strong> in seconds and find a job you’ll love!</a></li>

                                </ul>

                            </div><!-- content -->

                        </div><!-- dropdown -->

                    </div><!-- notifications -->

                    <a href="" class="btn btnWhiteB uploadCV">Upload Your CV</a>

                    <div class="userNav">

                        <div class="top">

                            <div class="sym">

                                <span>C</span>

                            </div><!-- sym -->

                            <span class="name">Chantel</span>

                            <span class="arrow"></span>

                        </div>

                        <div class="dropdown">

                            <ul>

                                <li><a href="">Edit Profile</a></li>

                                <li><a href="">Job Alerts</a></li>

                                <li><a href="">Job Applications</a></li>

                                <li><a href="">Freelance Profile</a></li>

                                <li class="logout"><a href="">Logout</a></li>

                            </ul>

                        </div>

                    </div><!-- user nav -->


                    <div class="clear"></div>

                </div><!-- right -->

                <div class="clear"></div>

            </div><!-- end container -->

        </div><!-- top -->

        <div class="bottom">

            <div class="search">

                <div class="inner">

                    <form  method="post">

                        <fieldset>

                            <span class="header">Search Jobs</span>

                            <input type="text" placeholder="Keywords (skills, job title etc)"/>

                            <input type="text" placeholder="Location (town, city etc)"/>

                            <input type="submit" value="Find Yours" class="btn btnBlue"/>

                        </fieldset>

                    </form>

                </div><!-- inner -->

            </div><!-- search -->

        </div><!-- bottom -->

    </header><!-- end header -->

    <div id="navigation">

        <a href="" class="respMenu"><div class="bars"></div></a>

        <div class="container">

            <div class="inner">

                <span class="header login">Login or Sign Up</span>

                <ul>

                    <li><a href="">Jobseekers</a></li>

                    <li><a href="">Recruiters</a></li>

                </ul>

                <a href="" class="btn btnWhiteB">Upload Your CV</a>

                <div class="recruiters">

                    <span class="header">Are You Recruiting?</span>

                    <a href="" class="btn btnCyan">Post A Job</a>

                </div><!-- recruiters -->


            </div><!-- inner -->

        </div><!-- container -->

    </div><!-- end navigation -->


    <!--// main content body -->
    <main class="page">

        <div id="breadcrumbs">

            <div class="container l0">

                <ul>

                    <li><a href="">Jobs</a></li>

                    <li>&gt;</li>

                    <li><a href="">KwaZulu-Natal</a></li>

                    <li>&gt;</li>

                    <li>Engineering Jobs</li>

                </ul>

                <div class="clear"></div>

            </div><!-- container -->

        </div><!-- breadcrumbs -->

        <section id="list">

            <div class="container l0">

                <div class="results">

                    <h1><strong>Engineering Jobs</strong> in KwaZulu Natal</h1>

                    <aside>

                        <div class="header">Explore More</div>

                        <div class="inner">

                            <div class="block">

                                <h3>Provinces</h3>

                                <ul>

                                    <li><a href="">The Eastern Cape <span class="value">(72)</span></a></li>

                                    <li><a href="">The Free State <span class="value">(72)</span></a></li>

                                    <li><a href="">Gauteng <span class="value">(72)</span></a></li>

                                    <li><a href="">KwaZulu-Natal <span class="value">(72)</span></a></li>

                                    <li><a href="">Limpopo <span class="value">(72)</span></a></li>

                                    <li><a href="">Mpumalanga <span class="value">(72)</span></a></li>

                                    <li><a href="">The Northern Cape <span class="value">(72)</span></a></li>

                                    <li><a href="">North West <span class="value">(72)</span></a></li>

                                    <li><a href="">The Western Cape <span class="value">(72)</span></a></li>

                                </ul>

                            </div><!-- block -->

                            <div class="block">

                                <h3>Top Cities</h3>

                                <ul>

                                    <li><a href="">Amanzimtoti <span class="value">(72)</span></a></li>

                                    <li><a href="">Cato Ridge <span class="value">(72)</span></a></li>

                                    <li><a href="">Doonside <span class="value">(72)</span></a></li>

                                    <li><a href="">Drummond <span class="value">(72)</span></a></li>

                                    <li><a href="">Durban <span class="value">(72)</span></a></li>

                                    <li><a href="">ekuPhakameni <span class="value">(72)</span></a></li>

                                    <li><a href="">Hillcrest <span class="value">(72)</span></a></li>

                                    <li><a href="">Illovo Beach <span class="value">(72)</span></a></li>

                                    <li><a href="">Inanda <span class="value">(72)</span></a></li>

                                    <li><a href="">Isipingo <span class="value">(72)</span></a></li>

                                </ul>

                            </div><!-- block -->

                            <div class="block">

                                <h3>Job Titles</h3>

                                <ul>

                                    <li><a href="">Structural Engineer <span class="value">(72)</span></a></li>

                                    <li><a href="">NVO Industrial Engineer <span class="value">(72)</span></a></li>

                                    <li><a href="">Design Mechanical Engineer <span class="value">(72)</span></a></li>

                                    <li><a href="">Engineer Engineer <span class="value">(72)</span></a></li>

                                    <li><a href="">Light Mechanical Engineer <span class="value">(72)</span></a></li>

                                    <li><a href="">SMG Mechanical Engineer <span class="value">(72)</span></a></li>

                                    <li><a href="">Structural Engineer <span class="value">(72)</span></a></li>

                                    <li><a href="">Industrial Engineer <span class="value">(72)</span></a></li>

                                    <li><a href="">inDiesign Mechanical <span class="value">(72)</span></a></li>

                                    <li><a href="">EngineerStructural Engineer <span class="value">(72)</span></a></li>

                                </ul>

                            </div>

                        </div><!-- inner -->

                    </aside><!-- sidebar -->

                    <div class="meta">

                         <p class="pageResultCount">Showing Results 1 - 15 of 9609</p>

                        <div id="filters">

                            <select class="custom_select">

                                <option value="">Date Filter</option>

                                <option value="1">Date</option>

                            </select>

                            <select class="custom_select">

                                <option value="">Job Type</option>

                                <option value="1">Type 1</option>

                                <option value="2">Type 2</option>

                                <option value="3">Type 3</option>

                            </select>

                        </div><!-- filter -->

                        <div class="clear"></div>

                    </div><!-- page meta -->


                    <div class="wrapper">


                        <div class="job listing" itemscope itemtype="http://schema.org/JobPosting">


                            <h2 itemprop="title"><a href="" class="detailLink">Structural Engineer</a></h2>

                            <p class="location"  itemprop="jobLocation">Ballito, Kwazulu-Natal</p>

                            <p class="salary" itemprop="baseSalary">R 600000 to 750000 per annum</p>

                            <img src="img/listing/mass_logo.png" class="companyLogo" alt="Mass Staffing Projects"/>

                            <p itemprop="description">Top Structural Engineering firm seeks Structural Engineer. Manage the Design Office and Perform Structural Design. Non-Negotiables (We will check): * Expert at Structural Design * Proficient in... <a href="" class="detailLink">More Info</a></p>


                            <dl>

                                <dt>Recruiter:</dt>

                                <dd><a href="#" itemprop="hiringOrganization">Dawning Truth</a></dd>

                            </dl>

                            <dl class="right">

                                <dt>Sector:</dt>

                                <dd><a href="#" itemprop="industrial">Engineering Jobs</a></dd>

                            </dl>

                            <div class="clear"></div>



                        </div><!-- job listing -->


                        <div class="alert">

                            <span class="header">Alert Me of Jobs Like These</span>

                            <a href="#" class="closeAlert"></a>

                            <div class="form">

                                <div class="inner">

                                    <form  method="post">

                                        <fieldset>

                                            <input type="text" placeholder="Your Email"/>

                                            <input type="text" placeholder="Location"/>

                                            <input type="submit" value="Create Alert" class="btn btnDBlue"/>

                                        </fieldset>

                                    </form>

                                </div><!-- inner -->

                            </div><!-- form -->

                            <div class="notification">

                                <div class="icon"></div>

                                <p>Job alert created</p>

                            </div><!-- notification -->

                        </div><!-- alert -->


                        <div class="job listing" itemscope itemtype="http://schema.org/JobPosting">

                            <h2 itemprop="title"><a href="" class="detailLink">Industrial Engineer</a></h2>

                            <p class="location" itemprop="jobLocation">Ballito, Kwazulu-Natal</p>

                            <p class="salary" itemprop="baseSalary">R 600000 to 750000 per annum</p>

                            <img src="img/listing/mass_logo.png" class="companyLogo" alt="Mass Staffing Projects"/>

                            <p itemprop="description">Top Structural Engineering firm seeks Structural Engineer. Manage the Design Office and Perform Structural Design. Non-Negotiables (We will check): * Expert at Structural Design * Proficient in... <a href="" class="detailLink">More Info</a></p>


                            <dl>

                                <dt>Recruiter:</dt>

                                <dd><a href="#" itemprop="hiringOrganization">Dawning Truth</a></dd>

                            </dl>

                            <dl class="right">

                                <dt>Sector:</dt>

                                <dd><a href="#" itemprop="industrial">Engineering Jobs</a></dd>

                            </dl>

                            <div class="clear"></div>

                        </div><!-- job listing -->

                        <div class="job listing" itemscope itemtype="http://schema.org/JobPosting">

                            <h2 itemprop="title"><a href="" class="detailLink">Industrial Engineer</a></h2>

                            <p class="location" itemprop="jobLocation">Ballito, Kwazulu-Natal</p>

                            <p class="salary" itemprop="baseSalary">R 600000 to 750000 per annum</p>

                            <img src="img/listing/mass_logo.png" class="companyLogo" alt="Mass Staffing Projects"/>

                            <p itemprop="description">Top Structural Engineering firm seeks Structural Engineer. Manage the Design Office and Perform Structural Design. Non-Negotiables (We will check): * Expert at Structural Design * Proficient in... <a href="" class="detailLink">More Info</a></p>


                            <dl>

                                <dt>Recruiter:</dt>

                                <dd><a href="#" itemprop="hiringOrganization">Dawning Truth</a></dd>

                            </dl>

                            <dl class="right">

                                <dt>Sector:</dt>

                                <dd><a href="#" itemprop="industrial">Engineering Jobs</a></dd>

                            </dl>

                            <div class="clear"></div>

                        </div><!-- job listing -->

                        <div class="job listing" itemscope itemtype="http://schema.org/JobPosting">

                            <h2 itemprop="title"><a href="" class="detailLink">Industrial Engineer</a></h2>

                            <p class="location" itemprop="jobLocation">Ballito, Kwazulu-Natal</p>

                            <p class="salary" itemprop="baseSalary">R 600000 to 750000 per annum</p>

                            <img src="img/listing/mass_logo.png" class="companyLogo" alt="Mass Staffing Projects"/>

                            <p itemprop="description">Top Structural Engineering firm seeks Structural Engineer. Manage the Design Office and Perform Structural Design. Non-Negotiables (We will check): * Expert at Structural Design * Proficient in... <a href="" class="detailLink">More Info</a></p>


                            <dl>

                                <dt>Recruiter:</dt>

                                <dd><a href="#" itemprop="hiringOrganization">Dawning Truth</a></dd>

                            </dl>

                            <dl class="right">

                                <dt>Sector:</dt>

                                <dd><a href="#" itemprop="industrial">Engineering Jobs</a></dd>

                            </dl>

                            <div class="clear"></div>

                        </div><!-- job listing -->

                        <div class="job listing" itemscope itemtype="http://schema.org/JobPosting">

                            <h2 itemprop="title"><a href="" class="detailLink">Industrial Engineer</a></h2>

                            <p class="location" itemprop="jobLocation">Ballito, Kwazulu-Natal</p>

                            <p class="salary" itemprop="baseSalary">R 600000 to 750000 per annum</p>

                            <img src="img/listing/mass_logo.png" class="companyLogo" alt="Mass Staffing Projects"/>

                            <p itemprop="description">Top Structural Engineering firm seeks Structural Engineer. Manage the Design Office and Perform Structural Design. Non-Negotiables (We will check): * Expert at Structural Design * Proficient in... <a href="" class="detailLink">More Info</a></p>


                            <dl>

                                <dt>Recruiter:</dt>

                                <dd><a href="#" itemprop="hiringOrganization">Dawning Truth</a></dd>

                            </dl>

                            <dl class="right">

                                <dt>Sector:</dt>

                                <dd><a href="#" itemprop="industrial">Engineering Jobs</a></dd>

                            </dl>

                            <div class="clear"></div>

                        </div><!-- job listing -->

                        <div class="job listing" itemscope itemtype="http://schema.org/JobPosting">

                            <h2 itemprop="title"><a href="" class="detailLink">Industrial Engineer</a></h2>

                            <p class="location" itemprop="jobLocation">Ballito, Kwazulu-Natal</p>

                            <p class="salary" itemprop="baseSalary">R 600000 to 750000 per annum</p>

                            <img src="img/listing/mass_logo.png" class="companyLogo" alt="Mass Staffing Projects"/>

                            <p itemprop="description">Top Structural Engineering firm seeks Structural Engineer. Manage the Design Office and Perform Structural Design. Non-Negotiables (We will check): * Expert at Structural Design * Proficient in... <a href="" class="detailLink">More Info</a></p>


                            <dl>

                                <dt>Recruiter:</dt>

                                <dd><a href="#" itemprop="hiringOrganization">Dawning Truth</a></dd>

                            </dl>

                            <dl class="right">

                                <dt>Sector:</dt>

                                <dd><a href="#" itemprop="industrial">Engineering Jobs</a></dd>

                            </dl>

                            <div class="clear"></div>

                        </div><!-- job listing -->


                        <div class="job listing" itemscope itemtype="http://schema.org/JobPosting">

                            <h2 itemprop="title"><a href="" class="detailLink">Industrial Engineer</a></h2>

                            <p class="location" itemprop="jobLocation">Ballito, Kwazulu-Natal</p>

                            <p class="salary" itemprop="baseSalary">R 600000 to 750000 per annum</p>

                            <img src="img/listing/mass_logo.png" class="companyLogo" alt="Mass Staffing Projects"/>

                            <p itemprop="description">Top Structural Engineering firm seeks Structural Engineer. Manage the Design Office and Perform Structural Design. Non-Negotiables (We will check): * Expert at Structural Design * Proficient in... <a href="" class="detailLink">More Info</a></p>


                            <dl>

                                <dt>Recruiter:</dt>

                                <dd><a href="#" itemprop="hiringOrganization">Dawning Truth</a></dd>

                            </dl>

                            <dl class="right">

                                <dt>Sector:</dt>

                                <dd><a href="#" itemprop="industrial">Engineering Jobs</a></dd>

                            </dl>

                            <div class="clear"></div>

                        </div><!-- job listing -->

                        <div class="alert">

                            <span class="header">Alert Me of Jobs Like These</span>

                            <a href="#" class="closeAlert"></a>

                            <div class="form">

                                <div class="inner">

                                    <form  method="post">

                                        <fieldset>

                                            <input type="text" placeholder="Your Email"/>

                                            <input type="text" placeholder="Location"/>

                                            <input type="submit" value="Create Alert" class="btn btnDBlue"/>

                                        </fieldset>

                                    </form>

                                </div><!-- inner -->

                            </div><!-- form -->

                            <div class="notification">

                                <div class="icon"></div>

                                <p>Job alert created</p>

                            </div><!-- notification -->

                        </div><!-- alert -->

                        <div id="pagination">

                            <p class="pageResultCount">Showing Results 1 - 15 of 9609</p>

                            <div class="paging">

                                <ul>

                                    <li class="prev"><a href="">Previous</a></li>

                                    <li><a href="">1</a></li>

                                    <li class="current"><a href="">2</a></li>

                                    <li><a href="">3</a></li>

                                    <li><a href="">4</a></li>

                                    <li><a href="">5</a></li>

                                    <li>...</li>

                                    <li><a href="">10</a></li>

                                    <li class="next"><a href="">Next</a></li>

                                </ul>

                                <div class="clear"></div>

                            </div>

                            <div class="clear"></div>

                        </div><!-- pagination -->

                        <div class="relatedSearches">

                            <span class="header">Searches related to: <strong>Engineering Jobs</strong></span>

                            <div class="threeColumn">

                                <div class="col">

                                    <ul>

                                        <li><a href="">Structural Engineer <span class="value">(72)</span></a></li>

                                        <li><a href="">NVO Industrial Engineer <span class="value">(72)</span></a></li>

                                        <li><a href="">Design Mechanical Engineer <span class="value">(72)</span></a></li>

                                        <li><a href="">Engineer Engineer <span class="value">(72)</span></a></li>

                                        <li><a href="">Light Industrial Engineer <span class="value">(72)</span></a></li>

                                    </ul>

                                </div><!-- col -->

                                <div class="col">

                                    <ul>

                                        <li><a href="">Structural Engineer <span class="value">(72)</span></a></li>

                                        <li><a href="">NVO Industrial Engineer <span class="value">(72)</span></a></li>

                                        <li><a href="">Design Mechanical Engineer <span class="value">(72)</span></a></li>

                                        <li><a href="">Engineer Engineer <span class="value">(72)</span></a></li>

                                        <li><a href="">Light Industrial Engineer <span class="value">(72)</span></a></li>

                                    </ul>

                                </div><!-- col -->

                                <div class="col">

                                    <ul>

                                        <li><a href="">Structural Engineer <span class="value">(72)</span></a></li>

                                        <li><a href="">NVO Industrial Engineer <span class="value">(72)</span></a></li>

                                        <li><a href="">Design Mechanical Engineer <span class="value">(72)</span></a></li>

                                        <li><a href="">Engineer Engineer <span class="value">(72)</span></a></li>

                                        <li><a href="">Light Industrial Engineer <span class="value">(72)</span></a></li>

                                    </ul>

                                </div><!-- col -->

                                <div class="clear"></div>

                            </div><!-- three coloumn -->

                        </div><!-- related searches -->

                    </div><!-- wrapper -->

                </div><!-- results -->



                <div class="clear"></div>

            </div><!-- container -->

        </section><!-- listing -->

    </main><!-- end main -->


    <!--//footer -->
    <footer>

        <div class="tagline">

            <p>Be First <span></span> Be Fast <span></span> Be Smart</p>

        </div><!-- tag line -->

        <div class="container l1">

            <div class="top">

                <div class="threeColumn">

                    <div class="col one">

                        <h3>JobVine Global</h3>

                        <p>At Jobvine our goal is to help you make the most of the 80 or 90 years you have on this planet by connecting you to the real world opportunities that can help you achieve your goals and realize your dreams. Visit <a href="">Jobvine.com</a></p>

                    </div><!-- col -->

                    <div class="col two">

                        <h3>JobVine Blog</h3>

                        <p>News, views, career advice and interview tips. And more</p>

                    </div><!-- col -->

                    <div class="col three">

                        <h3>For Employers</h3>

                        <ul>

                            <li><a href="">Post a Job</a></li>

                            <li><a href="">Products & Services</a></li>

                            <li><a href="">Contact Us</a></li>

                        </ul>

                    </div><!-- col -->

                    <div class="clear"></div>

                </div><!-- three column -->

            </div><!-- top -->

            <div class="bottom">

                <div class="left">

                    <ul class="nav">

                        <li><a href="#">About Us</a></li>

                        <li><a href="#">Contact Us</a></li>

                        <li><a href="#">Terms and Conditions</a></li>

                        <li><a href="#">Testimonials</a></li>


                    </ul>

                    <div class="clear"></div>

                    <p>&#169; <?php echo date("Y");?>. JobVine.co.za All Right Reserved.  79 Roeland street, Cape Town, South Africa</p>

                </div><!-- left -->


                <ul class="social">

                    <li><a href="#" class="twitter" target="_blank"></a></li>

                    <li><a href="#" class="fb" target="_blank"></a></li>

                    <li><a href="#" class="linkedin" target="_blank"></a></li>

                    <li><a href="#" class="gplus" target="_blank"></a></li>

                </ul><!-- end social -->


                <div class="clear"></div>

            </div><!-- bottom -->

            <div class="clear"></div>

        </div><!-- container -->

    </footer><!-- end footer -->



</div><!-- end page -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>

<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-color/2.1.2/jquery.color.min.js"></script>


<script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"></script>



<script src="js/main.js"></script>


</body>
</html>
