<!DOCTYPE html>
<!--[if lt IE 7]>      <html lang="en" class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html lang="en" class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html lang="en" class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Jobvine</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

    <link rel="profile" href="http://gmpg.org/xfn/11">

    <!-- //Bootstrap
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">>
    -->

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">


    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css"/>

    <link rel="stylesheet" href="style.css">

    <link rel="shortcut icon" href="jobvine_favicon.ico" type="image/x-icon" >

    <!--[if lt IE 9]>
    <script src="js/vendor/html5shiv.js"></script>
    <script src="js/vendor/respond.js"></script>
    <![endif]-->

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>


    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', '', 'auto');
        ga('send', 'pageview');
    </script>

</head>

<body>

<div id="root"></div>

<!--[if lt IE 9]>
<p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/" target="_blank">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<div id="page">

    <header class="fixed change in">

        <div class="top">

            <div class="container">

                <div class="left">

                    <div class="logo"><a href="">Jobvine</a></div>

                    <div class="pageName">Product Basket</div>

                    <div class="clear"></div>

                </div><!-- left -->

                <div class="right">

                    <a href="" class="basketHeader">

                        <img src="img/icon_cart.svg" class="basketIcon" alt="Basket Icon"/>

                        <div id="basketItemCount">2 <span class="label">Items</span></div>

                    </a><!-- basket -->

                </div><!-- right -->

                <div class="clear"></div>

            </div><!-- end container -->

        </div><!-- top -->

    </header><!-- end header -->

    <div id="navigation">

        <a href="" class="respMenu"><div class="bars"></div></a>

        <div class="container">

            <div class="inner">

                <span class="header login">Login or Sign Up</span>

                <ul>

                    <li><a href="">Jobseekers</a></li>

                    <li><a href="">Recruiters</a></li>

                </ul>

                <a href="" class="btn btnWhiteB">Upload Your CV</a>

                <div class="recruiters">

                    <span class="header">Are You Recruiting?</span>

                    <a href="" class="btn btnCyan">Post A Job</a>

                </div><!-- recruiters -->


            </div><!-- inner -->

        </div><!-- container -->

    </div><!-- end navigation -->


    <!--// main content body -->
    <main class="page top">

        <div id="basket" class="content table">

            <div class="container l0">

                <div class="title">

                    <h1>Your Basket</h1>

                </div><!-- title  -->

                <table id="shoppingBasketTable">

                    <thead>

                        <tr>

                            <th class="itemName">Item</th>

                            <th class="itemPriceEach">Price Each</th>

                            <th class="itemQuantity">Quantity</th>

                            <th class="itemTotalPrice">Total Price</th>

                            <th class="actions"></th>

                        </tr>

                    </thead>

                    <tbody>

                        <tr>

                            <td>Recruiter Job Adverts</td>

                            <td>$100</td>

                            <td><input type="text" class="quantity" value="10"/></td>

                            <td>$300</td>

                            <td>

                                <input type="button" class="btn btnDBlue update" value="Update"/>

                                <a href="" class="removeItem">Remove</a>

                                <div class="clear"></div>

                            </td>

                        </tr>

                        <tr>

                            <td>Recruiter Job Adverts</td>

                            <td>$100</td>

                            <td><input type="text" class="quantity" value="10"/></td>

                            <td>$300</td>

                            <td>

                                <input type="button" class="btn btnDBlue update" value="Update"/>

                                <a href="" class="removeItem">Remove</a>

                                <div class="clear"></div>

                            </td>

                        </tr>

                        <tr>

                            <td>Recruiter Job Adverts</td>

                            <td>$100</td>

                            <td><input type="text" class="quantity" value="10"/></td>

                            <td>$300</td>

                            <td>

                                <input type="button" class="btn btnDBlue update" value="Update"/>

                                <a href="" class="removeItem">Remove</a>

                                <div class="clear"></div>

                            </td>

                        </tr>

                        <tr>

                            <td>CV database - 1 week access</td>

                            <td>$100</td>

                            <td><input type="text" class="quantity error" value="0"/></td>

                            <td>$300</td>

                            <td>

                                <input type="button" class="btn btnDBlue update" value="Update"/>

                                <a href="" class="removeItem">Remove</a>

                                <div class="clear"></div>

                            </td>

                        </tr>

                        <tr>

                            <td>Recruiter Job Adverts</td>

                            <td>$100</td>

                            <td><input type="text" class="quantity" value="10"/></td>

                            <td>$300</td>

                            <td>

                                <input type="button" class="btn btnDBlue update" value="Update"/>

                                <a href="" class="removeItem">Remove</a>

                                <div class="clear"></div>

                            </td>

                        </tr>

                        <tr>

                            <td>Recruiter Job Adverts</td>

                            <td>$100</td>

                            <td><input type="text" class="quantity" value="10"/></td>

                            <td>$300</td>

                            <td>

                                <input type="button" class="btn btnDBlue update" value="Update"/>

                                <a href="" class="removeItem">Remove</a>

                                <div class="clear"></div>

                            </td>

                        </tr>

                    </tbody>

                </table><!-- shopping basket table -->

                <div class="collaterals">

                    <div class="totals">

                        <div class="promoWrapper ">

                            <!-- //Error message.
                            <div class="notification small error">

                                <div class="icon"></div>

                                <span>Invalid Promo Code</span>

                                <a href="" class="close"></a>

                            </div> message -->


                            <input type="text" placeholder="Promo Code"/>

                            <input type="button" class="btn btnBlue" value="Apply"/>

                        </div><!-- promo wrapper -->

                        <div class="clear"></div>

                        <div class="basketData">

                            <div class="lineItem">

                                <span class="label">Sub Total</span>

                                <span class="value">$600</span>

                            </div><!-- line item -->

                            <div class="lineItem">

                                <span class="label">Discount</span>

                                <span class="value">$100</span>

                            </div><!-- line item -->

                            <div class="lineItem">

                                <span class="label">Total (Exc. VAT)</span>

                                <span class="value">$500</span>

                            </div><!-- line item -->

                        </div><!-- basket data -->

                        <div class="buttons">

                            <input type="button" class="btn btnGrey" value="Continue Shopping"/>

                            <input type="submit" class="btn btnBlue" value="Secure Checkout"/>

                        </div><!-- buttons -->

                    </div><!-- totals -->

                    <p class="disclaimer">By completing your purchase you agree to Jobvine Global's Conditions of Use and Privacy Notice and authorize us to charge your designated credit card or another available credit card on file for any outstanding balances on your account as they become due.</p>

                </div><!-- cart collaterals -->

                <div class="bottom">

                    <p>100% Secure Checkout</p>

                    <img src="img/payment.png" class="paymentMethods" alt="Payment Methods"/>

                </div><!-- bottom -->

            </div><!-- container -->

        </div><!-- basket -->

    </main><!-- main -->




    <!--//footer -->
    <footer>

        <div class="tagline">

            <p>Be First <span></span> Be Fast <span></span> Be Smart</p>

        </div><!-- tag line -->

        <div class="container l1">

            <div class="top">

                <div class="threeColumn">

                    <div class="col one">

                        <h3>JobVine Global</h3>

                        <p>At Jobvine our goal is to help you make the most of the 80 or 90 years you have on this planet by connecting you to the real world opportunities that can help you achieve your goals and realize your dreams. Visit <a href="">Jobvine.com</a></p>

                    </div><!-- col -->

                    <div class="col two">

                        <h3>JobVine Blog</h3>

                        <p>News, views, career advice and interview tips. And more</p>

                    </div><!-- col -->

                    <div class="col three">

                        <h3>For Employers</h3>

                        <ul>

                            <li><a href="">Post a Job</a></li>

                            <li><a href="">Products & Services</a></li>

                            <li><a href="">Contact Us</a></li>

                        </ul>

                    </div><!-- col -->

                    <div class="clear"></div>

                </div><!-- three column -->

            </div><!-- top -->

            <div class="bottom">

                <div class="left">

                    <ul class="nav">

                        <li><a href="#">About Us</a></li>

                        <li><a href="#">Contact Us</a></li>

                        <li><a href="#">Terms and Conditions</a></li>

                        <li><a href="#">Testimonials</a></li>


                    </ul>

                    <div class="clear"></div>

                    <p>&#169; <?php echo date("Y");?>. JobVine.co.za All Right Reserved.  C/O Mauritius International Trust Company Limited, <br/>4th Floor, Ebene Skies, Rue de I'institut, Ebene, Mauritius</p>

                </div><!-- left -->


                <ul class="social">

                    <li><a href="#" class="twitter" target="_blank"></a></li>

                    <li><a href="#" class="fb" target="_blank"></a></li>

                    <li><a href="#" class="linkedin" target="_blank"></a></li>

                    <li><a href="#" class="gplus" target="_blank"></a></li>

                </ul><!-- end social -->


                <div class="clear"></div>

            </div><!-- bottom -->

            <div class="clear"></div>

        </div><!-- container -->

    </footer><!-- end footer -->



</div><!-- end page -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>

<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-color/2.1.2/jquery.color.min.js"></script>


<script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"></script>



<script src="js/main.js"></script>


</body>
</html>