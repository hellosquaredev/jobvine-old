<!DOCTYPE html>
<!--[if lt IE 7]>      <html lang="en" class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html lang="en" class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html lang="en" class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Jobvine</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

    <link rel="profile" href="http://gmpg.org/xfn/11">

    <!-- //Bootstrap
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">>
    -->

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">


    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css"/>

    <link rel="stylesheet" href="style.css">

    <link rel="shortcut icon" href="jobvine_favicon.ico" type="image/x-icon" >


    <!--[if lt IE 9]>
    <script src="js/vendor/html5shiv.js"></script>
    <script src="js/vendor/respond.js"></script>
    <![endif]-->

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>


    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', '', 'auto');
        ga('send', 'pageview');
    </script>

</head>

<body>

<div id="root"></div>

<!--[if lt IE 9]>
<p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/" target="_blank">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<div id="page">

    <header class="fixed change in">

        <div class="top">

            <div class="container">

                <div class="left">

                    <div class="logo"><a href="">Jobvine</a></div>

                    <ul>

                        <li class="dropdown">

                            <a href="">Jobseekers</a>

                            <div class="wrapper">

                                <div class="loginForm inner">

                                    <span class="header">Jobseekers Login</span>

                                    <form  method="post">

                                        <fieldset>

                                            <input type="email" placeholder="Email Address"/>

                                            <input type="password" placeholder="Password"/>

                                            <input type="submit" value="Login" class="btn btnBlue"/>

                                        </fieldset>

                                    </form>

                                    <a href="#" class="forgot">Forgot Password?</a>

                                    <div class="clear"></div>

                                </div><!-- inner -->

                                <div class="registerAction inner">

                                    <span class="header">Not a Member?</span>

                                    <a href="" class="btn btnDBlue">Register Here</a>

                                </div><!-- inner -->

                            </div><!-- wrapper -->

                        </li>

                        <li class="dropdown">

                            <a href="">Recruiters</a>

                            <div class="wrapper">

                                <div class="loginForm inner">

                                    <span class="header">Recruiters Login</span>

                                    <form  method="post">

                                        <fieldset>

                                            <input type="email" placeholder="Email Address"/>

                                            <input type="password" placeholder="Password"/>

                                            <input type="submit" value="Login" class="btn btnBlue"/>

                                        </fieldset>

                                    </form>

                                    <a href="#" class="forgot">Forgot Password?</a>

                                    <div class="clear"></div>

                                </div><!-- inner -->

                                <div class="registerAction inner">

                                    <span class="header">Not a Member?</span>

                                    <a href="" class="btn btnDBlue">Register Here</a>

                                </div><!-- inner -->

                            </div><!-- wrapper -->

                        </li>

                    </ul>

                    <div class="clear"></div>

                </div><!-- left -->


                <div class="right">

                    <a href="#" class="respMenu"><div class="bars"></div></a>

                    <a href="#" class="search mobile"><i class="fa fa-search" aria-hidden="true"></i></a>

                    <div class="notifications">

                        <div class="icon"></div>

                        <div class="count">1</div>

                        <div class="dropdown">

                            <span class="header">YAY! you have 1 new notification</span>

                            <div class="content">

                                <ul>

                                    <li><a href=""><strong>Sign up</strong> in seconds and find a job you’ll love!</a></li>

                                </ul>

                            </div><!-- content -->

                        </div><!-- dropdown -->

                    </div><!-- notifications -->

                    <a href="" class="btn btnWhiteB uploadCV">Upload Your CV</a>

                    <div class="userNav">

                        <div class="top">

                            <div class="sym">

                                <span>C</span>

                            </div><!-- sym -->

                            <span class="name">Chantel</span>

                            <span class="arrow"></span>

                        </div>

                        <div class="dropdown">

                            <ul>

                                <li><a href="">Edit Profile</a></li>

                                <li><a href="">Job Alerts</a></li>

                                <li><a href="">Job Applications</a></li>

                                <li><a href="">Freelance Profile</a></li>

                                <li class="logout"><a href="">Logout</a></li>

                            </ul>

                        </div>

                    </div><!-- user nav -->


                    <div class="clear"></div>

                </div><!-- right -->

                <div class="clear"></div>

            </div><!-- end container -->

        </div><!-- top -->

        <div class="bottom">

            <div class="search">

                <div class="inner">

                    <form  method="post">

                        <fieldset>

                            <span class="header">Search Jobs</span>

                            <input type="text" placeholder="Keywords (skills, job title etc)"/>

                            <input type="text" placeholder="Location (town, city etc)"/>

                            <input type="submit" value="Find Yours" class="btn btnBlue"/>

                        </fieldset>

                    </form>

                </div><!-- inner -->

            </div><!-- search -->

        </div><!-- bottom -->

    </header><!-- end header -->

    <div id="navigation">

        <a href="" class="respMenu"><div class="bars"></div></a>

        <div class="container">

            <div class="inner">

                <span class="header login">Login or Sign Up</span>

                <ul>

                    <li><a href="">Jobseekers</a></li>

                    <li><a href="">Recruiters</a></li>

                </ul>

                <a href="" class="btn btnWhiteB">Upload Your CV</a>

                <div class="recruiters">

                    <span class="header">Are You Recruiting?</span>

                    <a href="" class="btn btnCyan">Post A Job</a>

                </div><!-- recruiters -->


            </div><!-- inner -->

        </div><!-- container -->

    </div><!-- end navigation -->


    <!--// main content body -->
    <main class="page">

        <div id="default" class="content">

            <div class="title container l0">

                <h1>Terms & Conditions</h1>

            </div><!-- title -->

            <div class="container l1">

                <div class="copy">

                    <p>
        <span>Welcome to JobVine. If you continue to browse and use this website you are agreeing
            to comply with and be bound by the following Terms and Conditions of use, which
            together with our <a href="/privacy-policy/">Privacy Policy</a> govern JobVine’s relationship with you in relation to this
            website. </span>
                    </p>
                    <p>
        <span>The term ‘JobVine’ or ‘us’ or ‘we’ refers to the owner of this website whose registered
            office is Cape Town, South Africa. The term ‘you’ refers to the user or viewer of
            our website.</span>
                    </p>
                    <p>
                        <b><span>1. USE OF WEBSITE</span></b><span></span>
                    </p>
                    <p>
        <span>1.1 You are permitted to use our website for your own purposes and to print and
            download material from this website provided that you do not modify any content
            without our consent. No part of this website may be reproduced online or offline
            without our prior written permission.</span>
                    </p>
                    <p>
        <span>1.2 The copyright and other intellectual property rights in all material on this
            website are owned by us or our licensors and must not be reproduced without our
            prior consent. This material includes, but is not limited to, the design, layout,
            look, appearance and graphics. </span>
                    </p>
                    <p>
        <span>1.3 Unauthorised use of this website may give rise to a claim for damages and/or
            be a criminal offence. </span>
                    </p>
                    <p>
        <span>1.4 The content of the pages of this website is for your general information and
            use only. It is subject to change without notice.</span>
                    </p>
                    <p>
        <span>1.5 You acknowledge and agree that any personal details (including CVs) provided
            to this website will be available to view and access by approved JobVine recruiters.
            Approved JobVine recruiters are recruiters who have registered with and advertise
            jobs on the website.</span>
                    </p>
                    <p>
                        <b><span>2. VISITOR CONDUCT</span></b><span></span>
                    </p>
                    <p>
                        <span><span>2.1</span> </span><span>With the exception of personal information, the
            use of which is covered under our Privacy Policy, any material you send or post
            to this website shall be considered non-proprietary and not confidential. Unless
            you advise to the contrary we will be free to copy, disclose, distribute, incorporate
            and otherwise use such material for any and all purposes. </span>
                    </p>
                    <p>
        <span>shall not use the website in order to post, transmit, distribute, store or destroy
            material:</span>
                    </p>
                    <p>
        <span>(a) <span>&nbsp; </span>in violation of any applicable law or regulation;
        </span>
                    </p>
                    <p>
        <span>(b) <span>&nbsp; </span>in a manner that will infringe the copyright, trademark,
            trade secret or other intellectual property rights of others or violate the privacy,
            publicity or other personal rights of others; </span>
                    </p>
                    <p>
        <span>(c) <span>&nbsp; </span>that is defamatory, obscene, threatening, abusive or hateful.
        </span>
                    </p>
                    <p>
        <span>2.3 You are prohibited from violating or attempting to violate the security of
            this website, including, but without limitation:</span>
                    </p>
                    <p>
                        <span><span>(a)</span> </span><span>accessing data not intended for such user or logging
            into a server or account which you are not authorized to access; </span>
                    </p>
                    <p>
                        <span><span>(b)<span>&nbsp;&nbsp; </span></span></span><span>attempting to probe, scan
            or test the vulnerability of a system or network or to breach security or authentication
            measures without proper authorization; </span>
                    </p>
                    <p>
                        <span><span>(c)<span>&nbsp;&nbsp; </span></span></span><span>attempting to interfere
            with service to any user, host or network, including, without limitation, via means
            of submitting a virus to the website, overloading, &quot;flooding&quot;, &quot;spamming&quot;,
            &quot;mailbombing&quot; or &quot;crashing&quot;; </span>
                    </p>
                    <p>
                        <span><span>(d)<span>&nbsp;&nbsp; </span></span></span><span>sending unsolicited email,
            including promotions and/or advertising of products or services; </span>
                    </p>
                    <p>
                        <span><span>(e)<span>&nbsp;&nbsp; </span></span></span><span>forging any TCP/IP packet
            header or any part of the header information in any email or newsgroup posting;
        </span>
                    </p>
                    <p>
                        <span><span>(f)<span>&nbsp;&nbsp;&nbsp; </span></span></span><span>deleting or revising
            any material posted by any other person or entity; </span>
                    </p>
                    <p>
                        <span><span>(g)<span>&nbsp;&nbsp; </span></span></span><span>using any device, software
            or routine to interfere or attempt to interfere with the proper working of this
            website or any activity being conducted on this site. </span>
                    </p>
                    <p>
                        <span>2.4 Violations of paragraphs 2.2 or 2.3 may result in civil or criminal liability.</span>
                    </p>

                    <p>
                        <b><span>3. SITE UPTIME</span></b><span></span>
                    </p>
                    <p>
                        <span><span>3.1<span>&nbsp;&nbsp; </span></span></span><span>We take all reasonable
            steps to ensure that this website is available 24 hours every day, 365 days per
            year. However, websites do sometimes encounter downtime due to server and other
            technical issues. Therefore we will not be liable if this website is unavailable
            at any time.</span>
                    </p>
                    <p>
                        <b><span>4. LINKS TO AND FROM OTHER WEBSITES</span></b><span></span>
                    </p>
                    <p>
                        <span><span>4.1</span> </span><span>Any links to third party websites located on this
            website are provided for your convenience only.&nbsp; They do not signify that we
            endorse the website(s). We have no responsibility for the content of the linked
            website(s).</span>
                    </p>
                    <p>
                        <span><span>4.2<span>&nbsp;&nbsp; </span></span></span><span>If you would like to link
            to this website, you may only do so on the basis that you link to, but do not replicate,
            any page on this website and you do not in any way imply that we are endorsing any
            services or products unless this has been specifically agreed with us.</span>
                    </p>
                    <p>
                        <span><span>4.3</span> </span><span>If you link to our website in breach of the above,
            you shall fully indemnify us for any loss or damage suffered as a result of your
            actions.</span>
                    </p>
                    <p>
                    </p>
                    <p>
                        <b><span>5. EXCLUSION OF LIABILITY</span></b>
                    </p>
                    <p>
                        <span><span>5.1</span> </span><span>We take all reasonable steps to ensure that the
            information on this website is correct. However, neither we nor any third parties
            provide any warranty or guarantee as to the accuracy, timeliness, performance, completeness
            or suitability of the information and materials found or offered on this website
            for any particular purpose. You acknowledge that such information and materials
            may contain inaccuracies or errors and we expressly exclude liability for any such
            inaccuracies or errors to the fullest extent permitted by law. </span>
                    </p>
                    <p>
                        <span><span>5.2</span> </span><span>We are not to be considered to be a recruiter and/or
            employer with respect to the use of the website and shall not be responsible for
            you entering into any agreements or making decisions of whatever nature resulting
            from the posting of jobs, resumes and/or any other information obtained on the website.
            Your use of any information or materials on this website is entirely at your own
            risk, for which we shall not be liable. It shall be your own responsibility to ensure
            that any products, services or information available through this website meet your
            specific requirements.</span>
                    </p>
                    <p>
                        <span><span>5.3</span> </span><span>In no event shall we, our suppliers, or any third
            parties referred to on this website be liable for any damages of whatsoever nature,
            whether for bodily, moral or material injury (including, without limitation, indirect,
            punitive, incidental and consequential damages, lost profits, or damages resulting
            from lost data or business interruption) resulting from the use or inability to
            use the website and the material, whether based on warranty, contractual or extra-contractual
            liability, or any other legal matters, and whether or not we are advised of the
            possibility of such damages.</span>
                    </p>
                    <p>
                        <b><span>6. TERMINATION, SUSPENSION AND REMOVAL OF INFORMATION</span></b>
                    </p>
                    <p>
        <span>6.1 Without limitation, we reserve the right to refuse or deny access to our services,
            remove or alter, without prior notification, any material from the website, or to
            suspend, alter or terminate user registration and access to the website. </span>
                    </p>
                    <p>
                        <b><span>7. JURISDICTION</span></b>
                    </p>
                    <p>
        <span>7.1 Your use of this website and any dispute arising out of such use of the website
            is subject to the laws of South Africa. </span>
                    </p>
                    <p>
        <span>7.2 Should any part of these Terms and Conditions be held by a court of competent
            jurisdiction to be unenforceable, the validity of the remainder of the Terms and
            Conditions shall not be affected. </span>
                    </p>
                    <p>
                        <b><span>8.</span></b><span> <b>CONTACTING US</b></span>
                    </p>
                    <p>
        <span>8.1 We welcome any queries, comments or requests you may have regarding these
            Terms and Conditions.&nbsp; Please do not hesitate to <a href="/contact-us">contact us</a>.<span></span></span>
                    </p>

                    <h2 style="margin-top: 30px">
        <span>JobVine Premium Service Terms and Conditions
        </span>
                    </h2>
                    <p>
                        <span>Welcome to JobVine. Together with the Terms and Conditions and the Privacy Policy  the following governs the relationship between JobVine and yourself in respect of your use of the website as a JobVine Premium Client.</span>
                    </p>
                    <p>
        <span>Please Note: We reserve the right to update these terms and conditions and the onus is on the user to keep abreast of all changes and or updates that may occur from time to time.
        </span>
                    </p>

                    <p>
                        <b><span>1. DEFINITIONS AND INTERPRETATION</span></b><span></span>
                    </p>
                    <p>
                        <span>1.1 <strong>"JobVine"</strong>, <strong>"us"</strong> <strong>"our"</strong> or <strong>"we"</strong> refers to the owner of this website, JobVine Human Capital cc, whose registered office is Cape Town, South Africa.</span>
                    </p>
                    <p>
                        <span>1.2 <strong>"You"</strong>, <strong>"your"</strong> and <strong>"user"</strong> as used herein refer to all JobVine Premier Clients.</span>
                    </p>
                    <p>
                        <span>1.3 <strong>"Website"</strong> means the JobVine website, <a href="http://www.Jobvine.co.za">www.JobVine.co.za</a>, including any part or element thereof.</span>
                    </p>
                    <p>
                        <span>1.4 <strong>"Database"</strong> refers to the Candidate CV Database.</span>
                    </p>
                    <p>
                        <span>1.5 References herein to the singular includes the plural and vice versa.</span>
                    </p>
                    <p>
                        <span>1.6 As a registered JobVine recruiter, www.jobvine.co.za is free to use, but certain sections require you to purchase credits. Any free function of the website may be made payable (with credits) by JobVine, without prior notice to the recruiter.</span>
                    </p>
                    <p>
                        <b><span>2. GENERAL</span></b><span></span>
                    </p>
                    <p>
                        <span>2.1 All time displayed on the website is GMT. This includes the CV downloaded times.</span>
                    </p>
                    <p>
                        <span>2.2 IP addresses are tracked and recorded. Every CV you download from the Candidate CV Database will display the IP address used for the download.</span>
                    </p>
                    <p>
                    <p>
                        <span>2.3 Telephone support is only available to JobVine Premium Clients. All calls will be charged at the normal rates of your telephone or Cellular Service Provider.</span>
                    </p>
                    <p>
                        <span>2.4 JobVine complies with the Consumer Protection Act 68 0f 2008.</span>
                    </p>

                    <p>
                        <b><span>3. CANDIDATE CV DATABASE</span></b>
                    </p>
                    <p>
                        <span>3.1 Access to the Candidate CV Database is available only to JobVine Premier Clients who have been approved for CV search capabilities. However, the possibility exists that non-recruitment companies may be able to gain access to the database via a loop hole. We are doing our best to audit every user to prevent such unauthorized access. </span>
                    </p>
                    <p>
                        <span>3.2 You may access the database solely for the purpose of recruitment.</span>
                    </p>
                    <p>
                        <span>3.3 You may not use the database for any other purpose including, but not limited to, collecting and storing candidate contact information and other data.</span>
                    </p>
                    <p>
                        <span>3.4 You are required to be bound by the applicable South African laws, binding schemes or contracts that uphold privacy principles. Organisations, which are not bound by substantially similar principles, will be held to the rules described by the Information Technology Association of South Africa’s Code of Conduct.</span>
                    </p>
                    <p>
                        <span>3.5 The CV Search Engine operates through a server in the USA. You hereby accept and agree that we will not be liable to you or any other person whatsoever in respect of any loss or damages caused by or arising from the unavailability or interruption of this 3rd party server for any reason whatsoever. </span>
                    </p>
                    <p>
                        <span>3.6 The possibility exists that sometimes you may get a result returned that does not reflect your search criteria. However, the search algorithm is constantly being updated.</span>
                    </p>
                    <p>
                        <b><span>4. PREMIER CLIENT CREDITS</span></b><span></span>
                    </p>
                    <p>
                        <span>4.1 Payments for credits are made via PayFast. We will not be held liable for any loss or damage resulting from the use of the PayFast Website. Please make sure that you read the PayFast <a href="https://www.payfast.co.za/c/std/end-user-agreement" target="_blank">User Agreement</a>. </span>
                    </p>
                    <p>
                        <span>4.2 Credits purchased will be assigned to your company. These credits will be available for use to everyone in your company and all CV's downloaded, and the IP address used,  will be recorded and saved to your account.</span>
                    </p>
                    <p>
                        <span>4.3 The Website is Java Script enabled. Please note that if Java is not up to date on your system or your browser does not support Ajax, the amount of available credits displayed may be delayed and therefore incorrect. Pressing F5 (refresh) should update your credits.</span>
                    </p>
                    <p>
                        <span>4.4 You hereby accept and agree that the purchase of credits is not an explicit guarantee that you will find a suitable candidate in our Candidate CV Database.</span>
                    </p>
                    <p>
                        <span>4.5 Should the server be taken down or we decide not to continue with this service, your remaining credits will not be refunded.</span>
                    </p>
                    <p>
                        <span>4.6 Should you download a CV and the document is corrupted, that credit will not be refunded.</span>
                    </p>
                    <p>
                        <span>4.7 Credits not used within 12 months from the date of purchase will expire.</span>
                    </p>
                    <p>
                        <span>4.8 The prices listed on the website are valid from 1 Feb 2013. Prices may vary without notice.</span>
                    </p>
                    <p>
                        <span>4.9 The Premium recruiter will see minimal details related to each candidate, prior to the CV download. The minimal details includes - Title, Name (encrypted), Age, Highest Education, Years working experience, Ethnicity, Current location, and Job title.</span>
                    </p>
                    <p>
                        <span>4.10 All payments are non-refundable.</span>
                    </p>
                    <p>
                        <span>4.11 On renewal of a Premium job, the same amount of credits will be deducted to when the job was posted.</span>
                    </p>
                    <p>
                        <span>4.12 Some Premium jobs may not get posted on JobVine twitter due to high volume loads, but gets preference to Standard jobs.</span>
                    </p>
                    <p>
                        <span>4.13 The job approval process can be delayed if the company can't be easily verified.</span>
                    </p>
                    <p>
                        <span>4.14 Posting on the JobVine facebook group will be done within 1 hour of the job being activated, but JobVine doesn't take responsibility if any delays occur.</span>
                    </p>
                    <p>
                        <span>4.15 There might be times where Standard jobs are also posted to the JobVine Twitter account, due to a lack of Premium jobs.</span>
                    </p>
                    <p>
                        <b><span>5. CONTACTING US</span></b>
                    </p>
                    <p>
                        <span>5.1 We welcome any queries, comments or requests you may have regarding these Terms and Conditions. Please do not hesitate to <a href="http://www.jobvine.co.za/contact-us/">contact us</a>.</span>
                    </p>
                    <p>
                        The CPA may be downloaded from:
                        <br />
                        <a href="http://www.info.gov.za/view/DownloadFileAction?id=99961" target="_blank">http://www.info.gov.za/view/DownloadFileAction?id=99961</a>
                    </p>


                    <h2 style="margin-top: 30px">Third Parties and Other Users</h2>
                    <p>
                        <span>1. Services – Ownership and Restrictions.  You acknowledge that all the intellectual property rights in the Services (excluding any Content provided by Users) are owned by JobVine Insights, or JobVine Insights' licensors. You agree not to (a) reproduce, modify, publish, transmit, distribute, publicly perform or display, sell, or create derivative works based on the Services or the JobVine Insights Content and (b) rent, lease, loan, or sell access to the Services. “Content” means any work of authorship or information, including Salaries, Company Reviews, Interview Reviews, comments, opinions, postings, messages, text, files, images, photos, works of authorship, e-mail, or other materials.</span>
                    </p>
                    <p>
                        <span>2. JobVine Insights Content. Content from other Users, advertisers, and other third parties is made available to you through the Services. Because we do not control such Content, (a) you agree that we are not responsible for any such Content, including advertising and information about third party products or service, employer, interview and salary-related information provided by other Members through Salaries, Company Reviews, and Interview Reviews and (b) we make no guarantees about the accuracy, currency, suitability, or quality of the information in such Content, and we assume no responsibility for unintended, objectionable, inaccurate, misleading, or unlawful Content made available by other Users, advertisers, and third parties.</span>
                    </p>
                    <p>
                        <span>3. Aggregated content. Vacancies consumed via automatic integration into the JobVine database will be listed for 4 weeks, whereafter it will be automatically renewed by the system unless it has been expired by the account holder on the JobVine administration panel.</span>
                    </p>
                    <p>
                        <span>4. Savemoney <a href="">terms and conditions.</a></span>
                    </p>
                    <p>
                        <span>5. We collect your personal information that will be transferred to third parties who may distribute your information to their clients and customers for the purpose of quotes and related advertisements and marketing materials they may supply to you.</span>
                    </p>


                </div><!-- copy -->

            </div><!-- container -->

        </div><!-- profile -->

    </main><!-- main -->

    <!--//footer -->
    <footer>

        <div class="tagline">

            <p>Be First <span></span> Be Fast <span></span> Be Smart</p>

        </div><!-- tag line -->

        <div class="container l1">

            <div class="top">

                <div class="threeColumn">

                    <div class="col one">

                        <h3>JobVine Global</h3>

                        <p>At Jobvine our goal is to help you make the most of the 80 or 90 years you have on this planet by connecting you to the real world opportunities that can help you achieve your goals and realize your dreams. Visit <a href="">Jobvine.com</a></p>

                    </div><!-- col -->

                    <div class="col two">

                        <h3>JobVine Blog</h3>

                        <p>News, views, career advice and interview tips. And more</p>

                    </div><!-- col -->

                    <div class="col three">

                        <h3>For Employers</h3>

                        <ul>

                            <li><a href="">Post a Job</a></li>

                            <li><a href="">Products & Services</a></li>

                            <li><a href="">Contact Us</a></li>

                        </ul>

                    </div><!-- col -->

                    <div class="clear"></div>

                </div><!-- three column -->

            </div><!-- top -->

            <div class="bottom">

                <div class="left">

                    <ul class="nav">

                        <li><a href="#">About Us</a></li>

                        <li><a href="#">Contact Us</a></li>

                        <li><a href="#">Terms and Conditions</a></li>

                        <li><a href="#">Testimonials</a></li>


                    </ul>

                    <div class="clear"></div>

                    <p>&#169; <?php echo date("Y");?>. JobVine.co.za All Right Reserved.  C/O Mauritius International Trust Company Limited, <br/>4th Floor, Ebene Skies, Rue de I'institut, Ebene, Mauritius</p>

                </div><!-- left -->


                <ul class="social">

                    <li><a href="#" class="twitter" target="_blank"></a></li>

                    <li><a href="#" class="fb" target="_blank"></a></li>

                    <li><a href="#" class="linkedin" target="_blank"></a></li>

                    <li><a href="#" class="gplus" target="_blank"></a></li>

                </ul><!-- end social -->


                <div class="clear"></div>

            </div><!-- bottom -->

            <div class="clear"></div>

        </div><!-- container -->

    </footer><!-- end footer -->



</div><!-- end page -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>

<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-color/2.1.2/jquery.color.min.js"></script>


<script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"></script>



<script src="js/main.js"></script>


</body>
</html>
