<!DOCTYPE html>
<!--[if lt IE 7]>      <html lang="en" class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html lang="en" class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html lang="en" class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Jobvine</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

    <link rel="profile" href="http://gmpg.org/xfn/11">

    <!-- //Bootstrap
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">>
    -->

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">


    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css"/>

    <link rel="stylesheet" href="style.css">

    <link rel="shortcut icon" href="jobvine_favicon.ico" type="image/x-icon" >

    <!--[if lt IE 9]>
    <script src="js/vendor/html5shiv.js"></script>
    <script src="js/vendor/respond.js"></script>
    <![endif]-->

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>


    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', '', 'auto');
        ga('send', 'pageview');
    </script>

</head>

<body>

<div id="root"></div>

<!--[if lt IE 9]>
<p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/" target="_blank">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<div id="page">

    <header class="fixed change in">

        <div class="top">

            <div class="container">

                <div class="left">

                    <div class="logo"><a href="">Jobvine</a></div>

                    <ul>

                        <li class="dropdown">

                            <a href="">Jobseekers</a>

                            <div class="wrapper">

                                <div class="loginForm inner">

                                    <span class="header">Jobseekers Login</span>

                                    <form  method="post">

                                        <fieldset>

                                            <input type="email" placeholder="Email Address"/>

                                            <input type="password" placeholder="Password"/>

                                            <input type="submit" value="Login" class="btn btnBlue"/>

                                        </fieldset>

                                    </form>

                                    <a href="#" class="forgot">Forgot Password?</a>

                                    <div class="clear"></div>

                                </div><!-- inner -->

                                <div class="registerAction inner">

                                    <span class="header">Not a Member?</span>

                                    <a href="" class="btn btnDBlue">Register Here</a>

                                </div><!-- inner -->

                            </div><!-- wrapper -->

                        </li>

                        <li class="dropdown">

                            <a href="">Recruiters</a>

                            <div class="wrapper">

                                <div class="loginForm inner">

                                    <span class="header">Recruiters Login</span>

                                    <form  method="post">

                                        <fieldset>

                                            <input type="email" placeholder="Email Address"/>

                                            <input type="password" placeholder="Password"/>

                                            <input type="submit" value="Login" class="btn btnBlue"/>

                                        </fieldset>

                                    </form>

                                    <a href="#" class="forgot">Forgot Password?</a>

                                    <div class="clear"></div>

                                </div><!-- inner -->

                                <div class="registerAction inner">

                                    <span class="header">Not a Member?</span>

                                    <a href="" class="btn btnDBlue">Register Here</a>

                                </div><!-- inner -->

                            </div><!-- wrapper -->

                        </li>

                    </ul>

                    <div class="clear"></div>

                </div><!-- left -->


                <div class="right">

                    <a href="#" class="respMenu"><div class="bars"></div></a>

                    <a href="#" class="search mobile"><i class="fa fa-search" aria-hidden="true"></i></a>

                    <div class="notifications">

                        <div class="icon"></div>

                        <div class="count">1</div>

                        <div class="dropdown">

                            <span class="header">YAY! you have 1 new notification</span>

                            <div class="content">

                                <ul>

                                    <li><a href=""><strong>Sign up</strong> in seconds and find a job you’ll love!</a></li>

                                </ul>

                            </div><!-- content -->

                        </div><!-- dropdown -->

                    </div><!-- notifications -->

                    <a href="" class="btn btnWhiteB uploadCV">Upload Your CV</a>

                    <div class="userNav">

                        <div class="top">

                            <div class="sym">

                                <span>C</span>

                            </div><!-- sym -->

                            <span class="name">Chantel</span>

                            <span class="arrow"></span>

                        </div>

                        <div class="dropdown">

                            <ul>

                                <li><a href="">Edit Profile</a></li>

                                <li><a href="">Job Alerts</a></li>

                                <li><a href="">Job Applications</a></li>

                                <li><a href="">Freelance Profile</a></li>

                                <li class="logout"><a href="">Logout</a></li>

                            </ul>

                        </div>

                    </div><!-- user nav -->


                    <div class="clear"></div>

                </div><!-- right -->

                <div class="clear"></div>

            </div><!-- end container -->

        </div><!-- top -->

        <div class="bottom">

            <div class="search">

                <div class="inner">

                    <form  method="post">

                        <fieldset>

                            <span class="header">Search Jobs</span>

                            <input type="text" placeholder="Keywords (skills, job title etc)"/>

                            <input type="text" placeholder="Location (town, city etc)"/>

                            <input type="submit" value="Find Yours" class="btn btnBlue"/>

                        </fieldset>

                    </form>

                </div><!-- inner -->

            </div><!-- search -->

        </div><!-- bottom -->

    </header><!-- end header -->

    <div id="navigation">

        <a href="" class="respMenu"><div class="bars"></div></a>

        <div class="container">

            <div class="inner">

                <span class="header login">Login or Sign Up</span>

                <ul>

                    <li><a href="">Jobseekers</a></li>

                    <li><a href="">Recruiters</a></li>

                </ul>

                <a href="" class="btn btnWhiteB">Upload Your CV</a>

                <div class="recruiters">

                    <span class="header">Are You Recruiting?</span>

                    <a href="" class="btn btnCyan">Post A Job</a>

                </div><!-- recruiters -->


            </div><!-- inner -->

        </div><!-- container -->

    </div><!-- end navigation -->


    <!--// main content body -->
    <main class="page">

        <div id="testimonials" class="content">

            <section class="company container l0">

                <div class="title">

                    <div class="left">

                        <h1>10 000 + companies have used Jobvine</h1>

                        <p>This is what they have to say..</p>

                    </div><!-- left -->

                    <a href="" class="btn btnBlue">Write a Testimonial</a>

                    <div class="clear"></div>

                </div><!-- title -->

                <div class="reviews">

                    <div class="review col first">

                        <div class="inner">

                            <img src="img/testimonials/mikra.png" class="profileImage" alt="Mikra"/>

                            <p>As a recruiter I have decided to do a comparison over a period of 3 months of various recruitment sites. I have without a doubt gotten the best response as well as top quality CV's from Jobvine. I commend them on their site and look forward to using their services in 2010. Well done and thank you Jobvine!</p>

                            <div class="reviewerDetails">

                                <span class="name"><strong>Mikra Uys</strong> (Owner)</span>

                                <span class="companyName">Under African Skies Recruitment</span>

                            </div><!-- reviewer details -->

                        </div><!-- inner -->

                    </div><!-- col -->

                    <div class="review col second">

                        <div class="inner">

                            <img src="img/testimonials/terry.png" class="profileImage" alt="Terry"/>

                            <p>In an age when so many websites don't live up to their advertising claims, JobVine does everything it says it does. In addition, the personal care and service provided is truly amazing! I am one satisfied customer, and will continue to be for years to come.</p>

                            <div class="reviewerDetails">

                                <span class="name"><strong>Terry R Madavo</strong> (Co-Founder & Executive Recruiter) </span>

                                <span class="companyName">Sapientis </span>

                            </div><!-- reviewer details -->

                        </div><!-- inner -->

                    </div><!-- col -->

                    <div class="clear"></div>


                    <div class="review col first">

                        <div class="inner">

                            <img src="img/testimonials/cookie.png" class="profileImage" alt="Cookie"/>

                            <p>JobVine has provided a new platform for job advertising for Quantum Recruitment. As a result we have seen an excellent flow of job applications and CV's from job adverts done on JobVine. This has already yielded results for our team – we have done placements within our first month of advertising on JobVine. The website user interface is impressive and extremely user-friendly. Truth is we have received better results from JobVine, a free advertising website, as compared to websites which we currently pay a monthly subscription fee. Well done to JobVine and its team and I hope it grows from strength to strength.</p>

                            <div class="reviewerDetails">

                                <span class="name"><strong>Cookie Naidoo</strong> (Recruitment Manager)</span>

                                <span class="companyName">Quantum Recruitment</span>

                            </div><!-- reviewer details -->

                        </div><!-- inner -->

                    </div><!-- col -->

                    <div class="review col second">

                        <div class="inner">

                            <img src="img/testimonials/blank.png" class="profileImage" alt="Blank"/>

                            <p>Being a skeptic and not really anticipating much, Jet-Set Personnel decided to afford Jobvine an opportunity since we had nothing to lose. Little did we know, we gained more than we bargained for. The quality of CV’s are superior and not laced on every other website. How ironic that we placed a highly Technical IT SPEC within a month of registering by advertising and ONLY receiving one response which was the PLACEMENT. The responses are FANTASTIC and spot on. I highly recommend JOBVINE going forward. Thank you JOBVINE, Great job</p>

                            <div class="reviewerDetails">

                                <span class="name"><strong>Maude Morgan</strong> (Managing Director) </span>

                                <span class="companyName">Jet-Set Personnel Management</span>

                            </div><!-- reviewer details -->

                        </div><!-- inner -->

                    </div><!-- col -->

                    <div class="clear"></div>


                </div><!-- reviews -->

            </section><!-- container -->

            <section class="seeker">

                <div class="container l0">

                    <div class="title">

                        <div class="left">

                            <h1>Career Seeker Testimonials</h1>

                            <p>This is what they have to say..</p>

                        </div><!-- left -->

                        <a href="" class="btn btnBlue">Write a Testimonial</a>

                        <div class="clear"></div>

                    </div><!-- title -->

                    <div class="list">

                        <div class="container l1">

                            <div class="testimonial">

                                <div class="inner">

                                    <p>Thanks to Jobvine, I found a job as a Marketing Assistant in Gauteng at an Exhibition and Events Supplies company. Jobvine made it easy to apply for jobs, thank you so much :-)</p>

                                    <p class="meta">Vanessa, 02 July 2016</p>

                                </div><!-- inner -->

                            </div><!-- testimonial -->

                            <div class="testimonial">

                                <div class="inner">

                                    <p>Thanks to Jobvine I found a job as a KE marketing consultant in Gauteng at GGF Marketing Company. I am very grateful and will continue using your resources, on a light note - I would like to encourage everyone to use Jobvine as they are very helpful. Thank you :-)</p>

                                    <p class="meta">Vanessa, 02 July 2016</p>

                                </div><!-- inner -->

                            </div><!-- testimonial -->

                            <div class="testimonial">

                                <div class="inner">

                                    <p>Thanks to Jobvine I found a job as a KE marketing consultant in Gauteng at GGF Marketing Company. I am very grateful and will continue using your resources, on a light note - I would like to encourage everyone to use Jobvine as they are very helpful. Thank youThanks to Jobvine I found a job as a KE marketing consultant in Gauteng at GGF Marketing Company. I am very grateful and will continue using your resources, on a light note - I would like to encourage everyone to use Jobvine as they are very helpful. Thank you :-)</p>

                                    <p class="meta">Vanessa, 02 July 2016</p>

                                </div><!-- inner -->

                            </div><!-- testimonial -->

                            <div class="testimonial">

                                <div class="inner">

                                    <p>Thanks to Jobvine, I found a job as a Marketing Assistant in Gauteng at an Exhibitia job as a Marketing Assistant in Gauteng at an Exhibition and Events Supplies company. Ca job as a Marketing Assistant in at an Exhibitiareers made it easy to apply for jobs, thank you so much :-)</p>

                                    <p class="meta">Vanessa, 02 July 2016</p>

                                </div><!-- inner -->

                            </div><!-- testimonial -->



                        </div><!-- container -->

                        <div id="pagination">

                            <p class="pageResultCount">Showing Results 1 - 15 of 9609</p>

                            <div class="paging">

                                <ul>

                                    <li class="prev"><a href="">Previous</a></li>

                                    <li><a href="">1</a></li>

                                    <li class="current"><a href="">2</a></li>

                                    <li><a href="">3</a></li>

                                    <li><a href="">4</a></li>

                                    <li><a href="">5</a></li>

                                    <li>...</li>

                                    <li><a href="">10</a></li>

                                    <li class="next"><a href="">Next</a></li>

                                </ul>

                                <div class="clear"></div>

                            </div>

                            <div class="clear"></div>

                        </div><!-- pagination -->

                    </div><!-- list -->

                </div><!-- container -->

            </section><!-- career seekers -->

        </div><!-- profile -->

    </main><!-- main -->

    <!--//footer -->
    <footer>

        <div class="tagline">

            <p>Be First <span></span> Be Fast <span></span> Be Smart</p>

        </div><!-- tag line -->

        <div class="container l1">

            <div class="top">

                <div class="threeColumn">

                    <div class="col one">

                        <h3>JobVine Global</h3>

                        <p>At Jobvine our goal is to help you make the most of the 80 or 90 years you have on this planet by connecting you to the real world opportunities that can help you achieve your goals and realize your dreams. Visit <a href="">Jobvine.com</a></p>

                    </div><!-- col -->

                    <div class="col two">

                        <h3>JobVine Blog</h3>

                        <p>News, views, career advice and interview tips. And more</p>

                    </div><!-- col -->

                    <div class="col three">

                        <h3>For Employers</h3>

                        <ul>

                            <li><a href="">Post a Job</a></li>

                            <li><a href="">Products & Services</a></li>

                            <li><a href="">Contact Us</a></li>

                        </ul>

                    </div><!-- col -->

                    <div class="clear"></div>

                </div><!-- three column -->

            </div><!-- top -->

            <div class="bottom">

                <div class="left">

                    <ul class="nav">

                        <li><a href="#">About Us</a></li>

                        <li><a href="#">Contact Us</a></li>

                        <li><a href="#">Terms and Conditions</a></li>

                        <li><a href="#">Testimonials</a></li>


                    </ul>

                    <div class="clear"></div>

                    <p>&#169; <?php echo date("Y");?>. JobVine.co.za All Right Reserved.  C/O Mauritius International Trust Company Limited, <br/>4th Floor, Ebene Skies, Rue de I'institut, Ebene, Mauritius</p>

                </div><!-- left -->


                <ul class="social">

                    <li><a href="#" class="twitter" target="_blank"></a></li>

                    <li><a href="#" class="fb" target="_blank"></a></li>

                    <li><a href="#" class="linkedin" target="_blank"></a></li>

                    <li><a href="#" class="gplus" target="_blank"></a></li>

                </ul><!-- end social -->


                <div class="clear"></div>

            </div><!-- bottom -->

            <div class="clear"></div>

        </div><!-- container -->

    </footer><!-- end footer -->



</div><!-- end page -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>

<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-color/2.1.2/jquery.color.min.js"></script>


<script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"></script>



<script src="js/main.js"></script>


</body>
</html>
