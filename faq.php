<!DOCTYPE html>
<!--[if lt IE 7]>      <html lang="en" class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html lang="en" class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html lang="en" class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Jobvine</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

    <link rel="profile" href="http://gmpg.org/xfn/11">

    <!-- //Bootstrap
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">>
    -->

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">


    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css"/>

    <link rel="stylesheet" href="style.css">

    <link rel="shortcut icon" href="jobvine_favicon.ico" type="image/x-icon" >

    <!--[if lt IE 9]>
    <script src="js/vendor/html5shiv.js"></script>
    <script src="js/vendor/respond.js"></script>
    <![endif]-->

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>


    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', '', 'auto');
        ga('send', 'pageview');
    </script>

</head>

<body>

<div id="root"></div>

<!--[if lt IE 9]>
<p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/" target="_blank">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<div id="page">

    <header class="fixed change in">

        <div class="top">

            <div class="container">

                <div class="left">

                    <div class="logo"><a href="">Jobvine</a></div>

                    <div class="pageName">Help & Support</div>

                    <div class="clear"></div>

                </div><!-- left -->

                <div class="right">

                    <a href="" class="basketHeader">

                        <img src="img/icon_cart.svg" class="basketIcon" alt="Basket Icon"/>

                        <div id="basketItemCount">2 <span class="label">Items</span></div>

                    </a><!-- basket -->

                </div><!-- right -->

                <div class="clear"></div>

            </div><!-- end container -->

        </div><!-- top -->

    </header><!-- end header -->

    <div id="navigation">

        <a href="" class="respMenu"><div class="bars"></div></a>

        <div class="container">

            <div class="inner">

                <span class="header login">Login or Sign Up</span>

                <ul>

                    <li><a href="">Jobseekers</a></li>

                    <li><a href="">Recruiters</a></li>

                </ul>

                <a href="" class="btn btnWhiteB">Upload Your CV</a>

                <div class="recruiters">

                    <span class="header">Are You Recruiting?</span>

                    <a href="" class="btn btnCyan">Post A Job</a>

                </div><!-- recruiters -->


            </div><!-- inner -->

        </div><!-- container -->

    </div><!-- end navigation -->


    <!--// main content body -->
    <main class="page top">

        <section id="faq" class="content">

            <div class="container l0">

                <div class="title">

                    <h1>Help & Support</h1>

                    <p>Got a question to ask us? Use the list below to find answers to our most frequently asked questions.</p>

                </div><!-- title -->

                <div class="accordion">

                    <ul>

                        <li>

                            <div class="heading">How do I create an account</div>

                            <div class="panel">

                                <div class="inner">

                                    <p>To create an account, please follow <a href="">this link</a> and select the ‘Register new customer’ tab. Fill out all required fields and click submit. This will then take you to your recruiter dashboard where you can <a href="">buy job credits</a> and <a href="">CV Database access.</a></p>

                                </div><!-- inner -->

                            </div><!-- panel -->

                        </li>

                        <li>

                            <div class="heading">How do I login as a recruiter</div>

                            <div class="panel">

                                <div class="inner">

                                    <p>To create an account, please follow <a href="">this link</a> and select the ‘Register new customer’ tab. Fill out all required fields and click submit. This will then take you to your recruiter dashboard where you can <a href="">buy job credits</a> and <a href="">CV Database access.</a></p>

                                </div><!-- inner -->

                            </div><!-- panel -->

                        </li>

                        <li>

                            <div class="heading">I have forgotten my password or am having trouble logging in</div>

                            <div class="panel">

                                <div class="inner">

                                    <p>To create an account, please follow <a href="">this link</a> and select the ‘Register new customer’ tab. Fill out all required fields and click submit. This will then take you to your recruiter dashboard where you can <a href="">buy job credits</a> and <a href="">CV Database access.</a></p>

                                </div><!-- inner -->

                            </div><!-- panel -->

                        </li>

                        <li>

                            <div class="heading">How long will my job advertisement(s) appear on the site?</div>

                            <div class="panel">

                                <div class="inner">

                                    <p>To create an account, please follow <a href="">this link</a> and select the ‘Register new customer’ tab. Fill out all required fields and click submit. This will then take you to your recruiter dashboard where you can <a href="">buy job credits</a> and <a href="">CV Database access.</a></p>

                                </div><!-- inner -->

                            </div><!-- panel -->

                        </li>

                        <li>

                            <div class="heading">How long do my job credits last for?</div>

                            <div class="panel">

                                <div class="inner">

                                    <p>To create an account, please follow <a href="">this link</a> and select the ‘Register new customer’ tab. Fill out all required fields and click submit. This will then take you to your recruiter dashboard where you can <a href="">buy job credits</a> and <a href="">CV Database access.</a></p>

                                </div><!-- inner -->

                            </div><!-- panel -->

                        </li>

                        <li>

                            <div class="heading">How do I get a VAT receipt for online payments?</div>

                            <div class="panel">

                                <div class="inner">

                                    <p>To create an account, please follow <a href="">this link</a> and select the ‘Register new customer’ tab. Fill out all required fields and click submit. This will then take you to your recruiter dashboard where you can <a href="">buy job credits</a> and <a href="">CV Database access.</a></p>

                                </div><!-- inner -->

                            </div><!-- panel -->

                        </li>

                        <li>

                            <div class="heading">How do I pay my invoice online</div>

                            <div class="panel">

                                <div class="inner">

                                    <p>To create an account, please follow <a href="">this link</a> and select the ‘Register new customer’ tab. Fill out all required fields and click submit. This will then take you to your recruiter dashboard where you can <a href="">buy job credits</a> and <a href="">CV Database access.</a></p>

                                </div><!-- inner -->

                            </div><!-- panel -->

                        </li>


                    </ul>

                </div><!-- accordion -->

                <div class="bottom">

                    <div class="title">

                        <h1>Can't Find What You're Looking For?</h1>

                    </div><!-- title -->

                    <p>Please <a href="">complete this form</a> and we will respond shortly.</p>

                    <p>If you'd prefer to give us a call, please contact us on +254(0)21 713 8000. <br/>
                        Our Customer Services team is available Monday - Friday between 8am and 6pm (GMT).</p>

                </div><!-- bottom -->

            </div><!-- container -->

        </section><!-- faq -->

    </main><!-- main -->


    <!--//footer -->
    <footer>

        <div class="tagline">

            <p>Be First <span></span> Be Fast <span></span> Be Smart</p>

        </div><!-- tag line -->

        <div class="container l1">

            <div class="top">

                <div class="threeColumn">

                    <div class="col one">

                        <h3>JobVine Global</h3>

                        <p>At Jobvine our goal is to help you make the most of the 80 or 90 years you have on this planet by connecting you to the real world opportunities that can help you achieve your goals and realize your dreams. Visit <a href="">Jobvine.com</a></p>

                    </div><!-- col -->

                    <div class="col two">

                        <h3>JobVine Blog</h3>

                        <p>News, views, career advice and interview tips. And more</p>

                    </div><!-- col -->

                    <div class="col three">

                        <h3>For Employers</h3>

                        <ul>

                            <li><a href="">Post a Job</a></li>

                            <li><a href="">Products & Services</a></li>

                            <li><a href="">Contact Us</a></li>

                        </ul>

                    </div><!-- col -->

                    <div class="clear"></div>

                </div><!-- three column -->

            </div><!-- top -->

            <div class="bottom">

                <div class="left">

                    <ul class="nav">

                        <li><a href="#">About Us</a></li>

                        <li><a href="#">Contact Us</a></li>

                        <li><a href="#">Terms and Conditions</a></li>

                        <li><a href="#">Testimonials</a></li>


                    </ul>

                    <div class="clear"></div>

                    <p>&#169; <?php echo date("Y");?>. JobVine.co.za All Right Reserved.  C/O Mauritius International Trust Company Limited, <br/>4th Floor, Ebene Skies, Rue de I'institut, Ebene, Mauritius</p>

                </div><!-- left -->


                <ul class="social">

                    <li><a href="#" class="twitter" target="_blank"></a></li>

                    <li><a href="#" class="fb" target="_blank"></a></li>

                    <li><a href="#" class="linkedin" target="_blank"></a></li>

                    <li><a href="#" class="gplus" target="_blank"></a></li>

                </ul><!-- end social -->


                <div class="clear"></div>

            </div><!-- bottom -->

            <div class="clear"></div>

        </div><!-- container -->

    </footer><!-- end footer -->



</div><!-- end page -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>

<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-color/2.1.2/jquery.color.min.js"></script>


<script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"></script>



<script src="js/main.js"></script>


</body>
</html>