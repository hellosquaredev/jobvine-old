<!DOCTYPE html>
<!--[if lt IE 7]>      <html lang="en" class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html lang="en" class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html lang="en" class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
      
        <title>Jobvine</title>

       	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        
        <link rel="profile" href="http://gmpg.org/xfn/11">
        
        <!-- //Bootstrap
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">>
        -->

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
        

        <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css"/>

        <link rel="stylesheet" href="style.css">

        <link rel="shortcut icon" href="jobvine_favicon.ico" type="image/x-icon" >

        <!--[if lt IE 9]>
        <script src="js/vendor/html5shiv.js"></script>
        <script src="js/vendor/respond.js"></script>
        <![endif]-->

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

        
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', '', 'auto');
            ga('send', 'pageview');
        </script>
                
    </head>
		
    <body>
    	
        <div id="root"></div>

        <!--[if lt IE 9]>
        <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/" target="_blank">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

    	<div id="page">
						
            <header class="fixed">

                <div class="top">

                    <div class="container">

                      <div class="left">

                          <div class="logo"><a href="">Jobvine</a></div>

                          <ul>

                              <li class="dropdown">

                                  <a href="">Jobseekers</a>

                                  <div class="wrapper">

                                      <div class="loginForm inner">

                                          <span class="header">Jobseekers Login</span>

                                          <form  method="post">

                                              <fieldset>

                                                  <input type="email" placeholder="Email Address"/>

                                                  <input type="password" placeholder="Password"/>

                                                  <input type="submit" value="Login" class="btn btnBlue"/>

                                              </fieldset>

                                          </form>

                                          <a href="#" class="forgot">Forgot Password?</a>

                                          <div class="clear"></div>

                                      </div><!-- inner -->

                                      <div class="registerAction inner">

                                          <span class="header">Not a Member?</span>

                                          <a href="" class="btn btnDBlue">Register Here</a>

                                      </div><!-- inner -->

                                  </div><!-- wrapper -->

                              </li>

                              <li class="dropdown">

                                  <a href="">Recruiters</a>

                                  <div class="wrapper">

                                      <div class="loginForm inner">

                                          <span class="header">Recruiters Login</span>

                                          <form  method="post">

                                              <fieldset>

                                                  <input type="email" placeholder="Email Address"/>

                                                  <input type="password" placeholder="Password"/>

                                                  <input type="submit" value="Login" class="btn btnBlue"/>

                                              </fieldset>

                                          </form>

                                          <a href="#" class="forgot">Forgot Password?</a>

                                          <div class="clear"></div>

                                      </div><!-- inner -->

                                      <div class="registerAction inner">

                                          <span class="header">Not a Member?</span>

                                          <span class="btn btnDBlue">Register Here</span>

                                      </div><!-- inner -->

                                  </div><!-- wrapper -->

                              </li>

                          </ul>

                          <div class="clear"></div>

                      </div><!-- left -->


                      <div class="right">

                          <a href="#" class="respMenu"><div class="bars"></div></a>

                          <div class="notifications">

                              <div class="icon"></div>

                              <div class="count new">1</div>

                              <div class="dropdown">

                                  <span class="header">YAY! you have 1 new notification</span>

                                  <div class="content">

                                    <ul>

                                        <li><a href=""><strong>Sign up</strong> in seconds and find a job you’ll love!</a></li>

                                    </ul>

                                  </div><!-- content -->

                              </div><!-- dropdown -->

                          </div><!-- notifications -->


                          <a href="" class="btn btnWhiteB uploadCV">Upload Your CV</a>


                          <div class="clear"></div>

                      </div><!-- right -->

                      <div class="clear"></div>

                    </div><!-- end container -->

                </div><!-- top -->

                <div class="bottom">

                    <div class="search">

                        <div class="inner">

                            <form  method="post">

                                <fieldset>

                                    <input type="text" placeholder="Keywords (skills, job title etc)"/>

                                    <input type="text" placeholder="Location (town, city etc)"/>

                                    <input type="submit" value="Find Yours" class="btn btnBlue"/>

                                </fieldset>

                            </form>

                        </div><!-- inner -->

                    </div><!-- search -->

                </div><!-- bottom -->

            </header><!-- end header -->


            <div id="navigation">

                <a href="" class="respMenu"><div class="bars"></div></a>

                <div class="container">

                    <div class="inner">

                        <span class="header login">Login or Sign Up</span>

                        <ul>

                            <li><a href="">Jobseekers</a></li>

                            <li><a href="">Recruiters</a></li>

                        </ul>

                        <a href="" class="btn btnWhiteB">Upload Your CV</a>

                        <div class="recruiters">

                            <span class="header">Are You Recruiting?</span>

                            <a href="" class="btn btnCyan">Post A Job</a>

                        </div><!-- recruiters -->


                    </div><!-- inner -->

                </div><!-- container -->

            </div><!-- end navigation -->
          

            <!--//large hero image -->
            <section id="hero">

                <div class="container">

                      <div class="caption">

                        <div class="inner">

                            <h1>Jobs in South Africa</h1>

                            <p>29 034 jobs added in the last 28 days</p>


                            <div class="search">

                                <div class="inner">

                                    <form  method="post">

                                        <fieldset>

                                            <input type="text" placeholder="Keywords (skills, job title etc)"/>

                                            <input type="text" placeholder="Location (town, city etc)"/>

                                            <input type="submit" value="Find Yours" class="btn btnBlue"/>

                                        </fieldset>

                                    </form>

                                </div><!-- inner -->

                            </div><!-- search -->


                             <div class="recruiters">

                                 <span class="header">Are You Recruiting?</span>

                                 <a href="" class="btn btnCyan">Post A Job</a>

                             </div><!-- recruiters -->

                         </div><!-- innner -->

                      </div><!-- end caption -->

                </div><!-- container -->

            </section><!-- end hero -->






            <!--// main content body -->
            <main class="home">

                <div id="jobTypes">

                    <div class="tabButtons">

                        <div class="inner">

                            <div class="wrapper">

                                <a href="" data-tab="role"  class="active">Jobs By Role</a>

                                <a href="" data-tab="skill">Jobs By Skill</a>

                                <a href="" data-tab="sector">Jobs By Sector</a>

                                <a href="" data-tab="location">Jobs By Location</a>

                                <a href="" data-tab="company">Jobs By Company</a>

                                <a href="" data-tab="popular">Popular</a>

                            </div><!-- wrapper -->

                        </div><!-- inner -->

                    </div><!-- category buttons -->

                    <div class="tabs">

                        <div class="tab container l1 role active">

                            <a href="" class="mobile tabButton">Jobs By Role</a>

                            <div class="inner">

                                <div class="threeColumn">

                                    <div class="col">

                                        <ul>

                                            <li><a href="">Engineering Jobs <span class="value">(9579)</span></a></li>

                                            <li><a href="">IT & Telecoms Jobs <span class="value">(5499)</span></a></li>

                                            <li><a href="">Finance Jobs <span class="value">(3645)</span></a></li>

                                            <li><a href="">Hospitality & Catering Jobs <span class="value">(3569)</span></a></li>

                                            <li><a href="">Not Specified <span class="value">(3459)</span></a></li>

                                        </ul>

                                    </div><!-- col -->

                                    <div class="col">

                                        <ul>

                                            <li><a href="">Engineering Jobs <span class="value">(9579)</span></a></li>

                                            <li><a href="">IT & Telecoms Jobs <span class="value">(5499)</span></a></li>

                                            <li><a href="">Finance Jobs <span class="value">(3645)</span></a></li>

                                            <li><a href="">Hospitality & Catering Jobs <span class="value">(3569)</span></a></li>

                                            <li><a href="">Not Specified <span class="value">(3459)</span></a></li>

                                        </ul>

                                    </div><!-- col -->


                                    <div class="col">

                                        <ul>

                                            <li><a href="">Engineering Jobs <span class="value">(9579)</span></a></li>

                                            <li><a href="">IT & Telecoms Jobs <span class="value">(5499)</span></a></li>

                                            <li><a href="">Finance Jobs <span class="value">(3645)</span></a></li>

                                            <li><a href="">Hospitality & Catering Jobs <span class="value">(3569)</span></a></li>

                                            <li><a href="">Not Specified <span class="value">(3459)</span></a></li>

                                        </ul>

                                    </div><!-- col -->

                                    <div class="clear"></div>

                                </div><!-- coloumn wrapper -->

                                <a href="listing.php" class="btn showAll">Show All</a>

                            </div><!-- inner -->

                        </div><!-- tab -->


                        <div class="tab container l1 skill">

                            <a href="" class="mobile tabButton">Jobs By Skill</a>

                            <div class="inner">

                                <div class="threeColumn">

                                    <div class="col">

                                        <ul>

                                            <li><a href="">Engineering Jobs <span class="value">(9579)</span></a></li>

                                            <li><a href="">IT & Telecoms Jobs <span class="value">(5499)</span></a></li>

                                            <li><a href="">Finance Jobs <span class="value">(3645)</span></a></li>

                                            <li><a href="">Hospitality & Catering Jobs <span class="value">(3569)</span></a></li>

                                            <li><a href="">Not Specified <span class="value">(3459)</span></a></li>

                                        </ul>

                                    </div><!-- col -->

                                    <div class="col">

                                        <ul>

                                            <li><a href="">Engineering Jobs <span class="value">(9579)</span></a></li>

                                            <li><a href="">IT & Telecoms Jobs <span class="value">(5499)</span></a></li>

                                            <li><a href="">Finance Jobs <span class="value">(3645)</span></a></li>

                                            <li><a href="">Hospitality & Catering Jobs <span class="value">(3569)</span></a></li>

                                            <li><a href="">Not Specified <span class="value">(3459)</span></a></li>

                                        </ul>

                                    </div><!-- col -->


                                    <div class="col">

                                        <ul>

                                            <li><a href="">Engineering Jobs <span class="value">(9579)</span></a></li>

                                            <li><a href="">IT & Telecoms Jobs <span class="value">(5499)</span></a></li>

                                            <li><a href="">Finance Jobs <span class="value">(3645)</span></a></li>

                                            <li><a href="">Hospitality & Catering Jobs <span class="value">(3569)</span></a></li>

                                            <li><a href="">Not Specified <span class="value">(3459)</span></a></li>

                                        </ul>

                                    </div><!-- col -->

                                    <div class="clear"></div>

                                </div><!-- coloumn wrapper -->

                                <a href="listing.php" class="btn showAll">Show All</a>

                            </div><!-- inner -->

                        </div><!-- tab -->

                        <div class="tab container l1 sector">

                            <a href="" class="mobile tabButton">Jobs By Sector</a>

                            <div class="inner">

                                <div class="threeColumn">

                                    <div class="col">

                                        <ul>

                                            <li><a href="">Engineering Jobs <span class="value">(9579)</span></a></li>

                                            <li><a href="">IT & Telecoms Jobs <span class="value">(5499)</span></a></li>

                                            <li><a href="">Finance Jobs <span class="value">(3645)</span></a></li>

                                            <li><a href="">Hospitality & Catering Jobs <span class="value">(3569)</span></a></li>

                                            <li><a href="">Not Specified <span class="value">(3459)</span></a></li>

                                        </ul>

                                    </div><!-- col -->

                                    <div class="col">

                                        <ul>

                                            <li><a href="">Engineering Jobs <span class="value">(9579)</span></a></li>

                                            <li><a href="">IT & Telecoms Jobs <span class="value">(5499)</span></a></li>

                                            <li><a href="">Finance Jobs <span class="value">(3645)</span></a></li>

                                            <li><a href="">Hospitality & Catering Jobs <span class="value">(3569)</span></a></li>

                                            <li><a href="">Not Specified <span class="value">(3459)</span></a></li>

                                        </ul>

                                    </div><!-- col -->


                                    <div class="col">

                                        <ul>

                                            <li><a href="">Engineering Jobs <span class="value">(9579)</span></a></li>

                                            <li><a href="">IT & Telecoms Jobs <span class="value">(5499)</span></a></li>

                                            <li><a href="">Finance Jobs <span class="value">(3645)</span></a></li>

                                            <li><a href="">Hospitality & Catering Jobs <span class="value">(3569)</span></a></li>

                                            <li><a href="">Not Specified <span class="value">(3459)</span></a></li>

                                        </ul>

                                    </div><!-- col -->

                                    <div class="clear"></div>

                                </div><!-- coloumn wrapper -->

                                <a href="listing.php" class="btn showAll">Show All</a>

                            </div><!-- inner -->

                        </div><!-- tab -->

                        <div class="tab container l1 location">

                            <a href="" class="mobile tabButton">Jobs By Location</a>

                            <div class="inner">

                                <div class="threeColumn">

                                    <div class="col">

                                        <ul>

                                            <li><a href="">Engineering Jobs <span class="value">(9579)</span></a></li>

                                            <li><a href="">IT & Telecoms Jobs <span class="value">(5499)</span></a></li>

                                            <li><a href="">Finance Jobs <span class="value">(3645)</span></a></li>

                                            <li><a href="">Hospitality & Catering Jobs <span class="value">(3569)</span></a></li>

                                            <li><a href="">Not Specified <span class="value">(3459)</span></a></li>

                                        </ul>

                                    </div><!-- col -->

                                    <div class="col">

                                        <ul>

                                            <li><a href="">Engineering Jobs <span class="value">(9579)</span></a></li>

                                            <li><a href="">IT & Telecoms Jobs <span class="value">(5499)</span></a></li>

                                            <li><a href="">Finance Jobs <span class="value">(3645)</span></a></li>

                                            <li><a href="">Hospitality & Catering Jobs <span class="value">(3569)</span></a></li>

                                            <li><a href="">Not Specified <span class="value">(3459)</span></a></li>

                                        </ul>

                                    </div><!-- col -->


                                    <div class="col">

                                        <ul>

                                            <li><a href="">Engineering Jobs <span class="value">(9579)</span></a></li>

                                            <li><a href="">IT & Telecoms Jobs <span class="value">(5499)</span></a></li>

                                            <li><a href="">Finance Jobs <span class="value">(3645)</span></a></li>

                                            <li><a href="">Hospitality & Catering Jobs <span class="value">(3569)</span></a></li>

                                            <li><a href="">Not Specified <span class="value">(3459)</span></a></li>

                                        </ul>

                                    </div><!-- col -->

                                    <div class="clear"></div>

                                </div><!-- coloumn wrapper -->

                                <a href="listing.php" class="btn showAll">Show All</a>

                            </div><!-- inner -->

                        </div><!-- tab -->

                        <div class="tab container l1 company">

                            <a href="" class="mobile tabButton">Jobs By Company</a>

                            <div class="inner">

                                <div class="threeColumn">

                                    <div class="col">

                                        <ul>

                                            <li><a href="">Engineering Jobs <span class="value">(9579)</span></a></li>

                                            <li><a href="">IT & Telecoms Jobs <span class="value">(5499)</span></a></li>

                                            <li><a href="">Finance Jobs <span class="value">(3645)</span></a></li>

                                            <li><a href="">Hospitality & Catering Jobs <span class="value">(3569)</span></a></li>

                                            <li><a href="">Not Specified <span class="value">(3459)</span></a></li>

                                        </ul>

                                    </div><!-- col -->

                                    <div class="col">

                                        <ul>

                                            <li><a href="">Engineering Jobs <span class="value">(9579)</span></a></li>

                                            <li><a href="">IT & Telecoms Jobs <span class="value">(5499)</span></a></li>

                                            <li><a href="">Finance Jobs <span class="value">(3645)</span></a></li>

                                            <li><a href="">Hospitality & Catering Jobs <span class="value">(3569)</span></a></li>

                                            <li><a href="">Not Specified <span class="value">(3459)</span></a></li>

                                        </ul>

                                    </div><!-- col -->


                                    <div class="col">

                                        <ul>

                                            <li><a href="">Engineering Jobs <span class="value">(9579)</span></a></li>

                                            <li><a href="">IT & Telecoms Jobs <span class="value">(5499)</span></a></li>

                                            <li><a href="">Finance Jobs <span class="value">(3645)</span></a></li>

                                            <li><a href="">Hospitality & Catering Jobs <span class="value">(3569)</span></a></li>

                                            <li><a href="">Not Specified <span class="value">(3459)</span></a></li>

                                        </ul>

                                    </div><!-- col -->

                                    <div class="clear"></div>

                                </div><!-- coloumn wrapper -->

                                <a href="listing.php" class="btn showAll">Show All</a>

                            </div><!-- inner -->

                        </div><!-- tab -->

                        <div class="tab container l1 popular">

                            <a href="" class="mobile tabButton">Popular</a>

                            <div class="inner">

                                <div class="threeColumn">

                                    <div class="col">

                                        <ul>

                                            <li><a href="">Engineering Jobs <span class="value">(9579)</span></a></li>

                                            <li><a href="">IT & Telecoms Jobs <span class="value">(5499)</span></a></li>

                                            <li><a href="">Finance Jobs <span class="value">(3645)</span></a></li>

                                            <li><a href="">Hospitality & Catering Jobs <span class="value">(3569)</span></a></li>

                                            <li><a href="">Not Specified <span class="value">(3459)</span></a></li>

                                        </ul>

                                    </div><!-- col -->

                                    <div class="col">

                                        <ul>

                                            <li><a href="">Engineering Jobs <span class="value">(9579)</span></a></li>

                                            <li><a href="">IT & Telecoms Jobs <span class="value">(5499)</span></a></li>

                                            <li><a href="">Finance Jobs <span class="value">(3645)</span></a></li>

                                            <li><a href="">Hospitality & Catering Jobs <span class="value">(3569)</span></a></li>

                                            <li><a href="">Not Specified <span class="value">(3459)</span></a></li>

                                        </ul>

                                    </div><!-- col -->


                                    <div class="col">

                                        <ul>

                                            <li><a href="">Engineering Jobs <span class="value">(9579)</span></a></li>

                                            <li><a href="">IT & Telecoms Jobs <span class="value">(5499)</span></a></li>

                                            <li><a href="">Finance Jobs <span class="value">(3645)</span></a></li>

                                            <li><a href="">Hospitality & Catering Jobs <span class="value">(3569)</span></a></li>

                                            <li><a href="">Not Specified <span class="value">(3459)</span></a></li>

                                        </ul>

                                    </div><!-- col -->

                                    <div class="clear"></div>

                                </div><!-- coloumn wrapper -->

                                <a href="listing.php" class="btn showAll">Show All</a>

                            </div><!-- inner -->

                        </div><!-- tab -->



                    </div><!-- container-->


                    <div id="openSubscribe"><a href="" class="btn btnBlue">Jobs By Email</a></div><!-- subscribe -->

                    <div id="subscribe">

                        <div class="inner">

                            <h3>Get That Job Sooner!</h3>

                            <p>Receive job alerts instantly in your inbox</p>

                            <form  method="post">

                                <fieldset>

                                    <input type="email" placeholder="Your Email"/>

                                    <input type="text" placeholder="Job Title"/>

                                    <input type="text" placeholder="Location"/>

                                    <input type="submit" value="Sign Up" class="btn btnBlue"/>

                                </fieldset>

                            </form>

                            <a href="" class="close"></a>

                        </div><!-- inner -->

                        <div class="notification">

                            <div class="icon"></div>

                            <p>Job alert created</p>

                        </div><!-- notification -->

                    </div><!-- subscrbie -->

                </div><!-- job types tabs -->

                <section id="companies" class="content">


                    <div class="container l1">

                        <div class="title">

                            <h2>6 743 Companies Hiring</h2>



                        </div><!-- title -->

                        <div class="wrapper">

                            <a href="" class="control prev"></a>
                            <a href="" class="control next"></a>

                            <div class="slider">

                                <div class="slide"><img src="img/home/og_logo.jpg" alt="ogilvy"/></div>

                                <div class="slide"><img src="img/home/google_logo.jpg" alt="Google Logo"/></div>

                                <div class="slide"><img src="img/home/mrp_logo.png" alt="Mr Price Logo"/></div>

                                <div class="slide"><img src="img/home/edcon_logo.png" alt="Edcon Logo"/></div>

                                <div class="slide"><img src="img/home/hrt_logo.png" alt="HRT & Carter Logo"/></div>

                                <div class="slide"><img src="img/home/tmg_logo.png" alt="Times Media Group"/></div>

                                <div class="slide"><img src="img/home/og_logo.jpg" alt="ogilvy"/></div>

                                <div class="slide"><img src="img/home/google_logo.jpg" alt="Google Logo"/></div>

                                <div class="slide"><img src="img/home/mrp_logo.png" alt="Mr Price Logo"/></div>

                                <div class="slide"><img src="img/home/edcon_logo.png" alt="Edcon Logo"/></div>

                                <div class="slide"><img src="img/home/hrt_logo.png" alt="HRT & Carter Logo"/></div>

                                <div class="slide"><img src="img/home/tmg_logo.png" alt="Times Media Group"/></div>

                            </div><!-- slider -->

                        </div><!-- wrapper -->

                        <a href="" class="btn btnBlueB showAll">Show All</a>

                    </div><!-- container -->

                </section><!-- companies -->


                <section id="recent" class="content">

                    <div class="title">

                        <div class="container l1">

                            <h2>Recent Jobs Added</h2>

                            <a href="" class="btn btnBlueB showAll desktop">Show All</a>

                        </div><!-- container -->

                    </div><!-- title -->

                    <div class="container l0">

                        <div class="inner">

                            <div class="col">

                                <h3><a href="">Electronics Engineer </a></h3>

                                <span class="salary">R 20k per month</span>

                                <span class="location">Roodepoort, Gauteng</span>

                                <p>An ambitious and entrepreneurial company is looking for a young Mechanical Engineer with 5 years mechanical design experience. Our company designs...</p>

                                <a href="">More Info</a>

                            </div><!-- col -->

                            <div class="col">

                                <h3><a href="">Java Programmers</a></h3>

                                <span class="salary">R 20k per month</span>

                                <span class="location">Roodepoort, Gauteng</span>

                                <p>An ambitious and entrepreneurial company is looking for a young Mechanical Engineer with 5 years mechanical design experience. Our company designs...</p>

                                <a href="">More Info</a>

                            </div><!-- col -->

                            <div class="col">

                                <h3><a href="">Financial Controller</a></h3>

                                <span class="salary">R 30k per month</span>

                                <span class="location">City Of Tshwane Metro, Gauteng</span>

                                <p>An ambitious and entrepreneurial company is looking for a young Mechanical Engineer with 5 years mechanical design experience. Our company designs...</p>

                                <a href="">More Info</a>

                            </div><!-- col -->

                            <div class="col">

                                <h3><a href="">Java Senior Programmers</a></h3>

                                <span class="salary">R 40k per month</span>

                                <span class="location">Cape Town, Western Cape</span>

                                <p>An ambitious and entrepreneurial company is looking for a young Mechanical Engineer with 5 years mechanical design experience. Our company designs...</p>

                                <a href="">More Info</a>

                            </div><!-- col -->

                            <div class="clear"></div>

                            <a href="" class="btn btnBlueB showAll mobile">Show All</a>

                        </div><!-- inner -->

                    </div><!-- container -->

                </section><!-- recent jobs -->

                <section class="content">

                    <div class="container l1">

                        <div id="featured">

                            <div class="title">

                                <h2>Featured On</h2>

                            </div><!-- title -->

                            <div class="wrapper">

                                <img src="img/home/business_insider_logo.png" alt="Business Insider"/>

                                <img src="img/home/mashable_logo.png" alt="Mashable"/>

                                <img src="img/home/harvard_logo.png" alt="Harvard"/>

                                <img src="img/home/alltop_logo.png" alt="Alltop"/>

                                <img src="img/home/fast_company_logo.png" alt="Fast Company"/>

                                <div class="clear"></div>

                            </div><!-- wrapper -->



                        </div><!-- featured -->

                        <div id="advice">

                            <div class="title">

                                <h2>Career Advice</h2>

                            </div><!-- title -->

                            <div class="threeColumn">

                                <article class="col">

                                    <h3><a href="">25 Questions to Anticipate from an Interviewer</a></h3>

                                    <p>You’ve seen your dream job listed in a newspaper, magazine or on the internet, you’ve sent off your CV and now you received a call to come in for an interview.</p>

                                    <a href="" class="readMore">Read Full Post</a>

                                </article><!-- col -->

                                <article class="col">

                                    <h3><a href="">A Few Things to do when you get fired</a></h3>

                                    <p>Getting fired can be an unsettling experience and many of us hope to never see the day. But, it is in fact a major facet of everyday business life,</p>

                                    <a href="" class="readMore">Read Full Post</a>

                                </article><!-- col -->

                                <article class="col">

                                    <h3><a href="">A Few Ways to know when it's time for a job change</a></h3>

                                    <p>When you’ve been working at the same desk, in the same office, with the same workload weighing down on you day after day, it might be time for a...</p>

                                    <a href="" class="readMore">Read Full Post</a>

                                </article><!-- col -->

                                <div class="clear"></div>

                            </div><!-- three column -->

                        </div><!-- career -->

                    </div><!-- container -->

                </section><!-- content -->




            </main><!-- end main -->


            <!--//footer -->
            <footer>

                <div class="tagline">

                    <p>Be First <span></span> Be Fast <span></span> Be Smart</p>

                </div><!-- tag line -->

                <div class="container l1">

                    <div class="top">

                        <div class="threeColumn">

                            <div class="col one">

                                <h3>JobVine Global</h3>

                                <p>At Jobvine our goal is to help you make the most of the 80 or 90 years you have on this planet by connecting you to the real world opportunities that can help you achieve your goals and realize your dreams. Visit <a href="">Jobvine.com</a></p>

                            </div><!-- col -->

                            <div class="col two">

                                <h3>JobVine Blog</h3>

                                <p>News, views, career advice and interview tips. And more</p>

                            </div><!-- col -->

                            <div class="col three">

                                <h3>For Employers</h3>

                                <ul>

                                    <li><a href="">Post a Job</a></li>

                                    <li><a href="">Products & Services</a></li>

                                    <li><a href="">Contact Us</a></li>

                                </ul>

                            </div><!-- col -->

                            <div class="clear"></div>

                        </div><!-- three column -->

                    </div><!-- top -->

                    <div class="bottom">

                        <div class="left">

                            <ul class="nav">

                                <li><a href="#">About Us</a></li>

                                <li><a href="#">Contact Us</a></li>

                                <li><a href="#">Terms and Conditions</a></li>

                                <li><a href="#">Testimonials</a></li>


                            </ul>

                            <div class="clear"></div>

                            <p>&#169; <?php echo date("Y");?>. JobVine.co.za All Right Reserved.  C/O Mauritius International Trust Company Limited, <br/>4th Floor, Ebene Skies, Rue de I'institut, Ebene, Mauritius</p>

                        </div><!-- left -->


                        <ul class="social">

                            <li><a href="#" class="twitter" target="_blank"></a></li>

                            <li><a href="#" class="fb" target="_blank"></a></li>

                            <li><a href="#" class="linkedin" target="_blank"></a></li>

                            <li><a href="#" class="gplus" target="_blank"></a></li>

                        </ul><!-- end social -->


                        <div class="clear"></div>

                    </div><!-- bottom -->

                    <div class="clear"></div>

                </div><!-- container -->

            </footer><!-- end footer -->



        </div><!-- end page -->
        
        <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>

        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
				
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-color/2.1.2/jquery.color.min.js"></script>


        <script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"></script>


        <script src="js/main.js"></script>

    </body>
</html>

            