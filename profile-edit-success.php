<!DOCTYPE html>
<!--[if lt IE 7]>      <html lang="en" class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html lang="en" class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html lang="en" class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Jobvine</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

    <link rel="profile" href="http://gmpg.org/xfn/11">

    <!-- //Bootstrap
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">>
    -->

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">


    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css"/>

    <link rel="stylesheet" href="style.css">

    <link rel="shortcut icon" href="jobvine_favicon.ico" type="image/x-icon" >

    <!--[if lt IE 9]>
    <script src="js/vendor/html5shiv.js"></script>
    <script src="js/vendor/respond.js"></script>
    <![endif]-->

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>


    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', '', 'auto');
        ga('send', 'pageview');
    </script>

</head>

<body>

<div id="root"></div>

<!--[if lt IE 9]>
<p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/" target="_blank">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<div id="page">

    <header class="fixed change in">

        <div class="top">

            <div class="container">

                <div class="left">

                    <div class="logo"><a href="">Jobvine</a></div>

                    <ul>

                        <li class="dropdown">

                            <a href="">Jobseekers</a>

                            <div class="wrapper">

                                <div class="loginForm inner">

                                    <span class="header">Jobseekers Login</span>

                                    <form  method="post">

                                        <fieldset>

                                            <input type="email" placeholder="Email Address"/>

                                            <input type="password" placeholder="Password"/>

                                            <input type="submit" value="Login" class="btn btnBlue"/>

                                        </fieldset>

                                    </form>

                                    <a href="#" class="forgot">Forgot Password?</a>

                                    <div class="clear"></div>

                                </div><!-- inner -->

                                <div class="registerAction inner">

                                    <span class="header">Not a Member?</span>

                                    <a href="" class="btn btnDBlue">Register Here</a>

                                </div><!-- inner -->

                            </div><!-- wrapper -->

                        </li>

                        <li class="dropdown">

                            <a href="">Recruiters</a>

                            <div class="wrapper">

                                <div class="loginForm inner">

                                    <span class="header">Recruiters Login</span>

                                    <form  method="post">

                                        <fieldset>

                                            <input type="email" placeholder="Email Address"/>

                                            <input type="password" placeholder="Password"/>

                                            <input type="submit" value="Login" class="btn btnBlue"/>

                                        </fieldset>

                                    </form>

                                    <a href="#" class="forgot">Forgot Password?</a>

                                    <div class="clear"></div>

                                </div><!-- inner -->

                                <div class="registerAction inner">

                                    <span class="header">Not a Member?</span>

                                    <a href="" class="btn btnDBlue">Register Here</a>

                                </div><!-- inner -->

                            </div><!-- wrapper -->

                        </li>

                    </ul>

                    <div class="clear"></div>

                </div><!-- left -->


                <div class="right">

                    <a href="#" class="respMenu"><div class="bars"></div></a>

                    <a href="#" class="search mobile"><i class="fa fa-search" aria-hidden="true"></i></a>

                    <div class="notifications">

                        <div class="icon"></div>

                        <div class="count">1</div>

                        <div class="dropdown">

                            <span class="header">YAY! you have 1 new notification</span>

                            <div class="content">

                                <ul>

                                    <li><a href=""><strong>Sign up</strong> in seconds and find a job you’ll love!</a></li>

                                </ul>

                            </div><!-- content -->

                        </div><!-- dropdown -->

                    </div><!-- notifications -->

                    <a href="" class="btn btnWhiteB uploadCV">Upload Your CV</a>

                    <div class="userNav">

                        <div class="top">

                            <div class="sym">

                                <span>C</span>

                            </div><!-- sym -->

                            <span class="name">Chantel</span>

                            <span class="arrow"></span>

                        </div>

                        <div class="dropdown">

                            <ul>

                                <li><a href="">Edit Profile</a></li>

                                <li><a href="">Job Alerts</a></li>

                                <li><a href="">Job Applications</a></li>

                                <li><a href="">Freelance Profile</a></li>

                                <li class="logout"><a href="">Logout</a></li>

                            </ul>

                        </div>

                    </div><!-- user nav -->


                    <div class="clear"></div>

                </div><!-- right -->

                <div class="clear"></div>

            </div><!-- end container -->

        </div><!-- top -->

        <div class="bottom">

            <div class="search">

                <div class="inner">

                    <form  method="post">

                        <fieldset>

                            <span class="header">Search Jobs</span>

                            <input type="text" placeholder="Keywords (skills, job title etc)"/>

                            <input type="text" placeholder="Location (town, city etc)"/>

                            <input type="submit" value="Find Yours" class="btn btnBlue"/>

                        </fieldset>

                    </form>

                </div><!-- inner -->

            </div><!-- search -->

        </div><!-- bottom -->

    </header><!-- end header -->

    <div id="navigation">

        <a href="" class="respMenu"><div class="bars"></div></a>

        <div class="container">

            <div class="inner">

                <span class="header login">Login or Sign Up</span>

                <ul>

                    <li><a href="">Jobseekers</a></li>

                    <li><a href="">Recruiters</a></li>

                </ul>

                <a href="" class="btn btnWhiteB">Upload Your CV</a>

                <div class="recruiters">

                    <span class="header">Are You Recruiting?</span>

                    <a href="" class="btn btnCyan">Post A Job</a>

                </div><!-- recruiters -->


            </div><!-- inner -->

        </div><!-- container -->

    </div><!-- end navigation -->


    <!--// main content body -->
    <main class="page">

        <div id="profile" class="content edit">

            <div class="title container l0">

                <h1 class="profileName">Chantel Pretorius</h1>

                <span class="note">All mandatory fields are marked with *</span>

            </div><!-- container -->

            <div class="form container l1">

                <div class="notification success">

                    <div class="icon"></div>

                    <p>Profile successfully updated</p>

                </div><!-- notification -->

                <form method="post" enctype="multipart/form-data">

                    <div class="block">

                        <h2>Sign In Details</h2>

                        <ul>

                            <li>

                                <label>Email Address</label>

                                <div class="field">chantel@hellosquare.co.za</div>

                            </li>



                        </ul>


                    </div><!-- block -->

                    <div class="block">

                        <h2>Your CV</h2>

                        <ul>

                            <li class="">

                                <label>Add CV*</label>

                                <div class="field">

                                    <!-- Upload Box

                                    <div class="upload">

                                        <input type="file" />

                                        <div class="box">Upload your CV</div>

                                    </div><

                                    <label>

                                        <input type="checkbox"/>

                                        Remind me to upload my CV later

                                    </label>
                                    -->

                                    <!--
                                    <div class="upload">

                                        <select class="custom_select cv">

                                            <option>Upload your CV</option>

                                            <option value="1">from this device</option>

                                            <option value="2">from Google Drive</option>

                                            <option value="3">from OneDrive</option>

                                            <option value="4">from Dropbox</option>

                                            <option value="5">from Box</option>

                                        </select>

                                    </div>

                                    <label>

                                        <input type="checkbox"/>

                                        Remind me to upload my CV later

                                    </label>
                                    -->

                                    <a href="" class="cvLink" target="_blank">CV_Chantel_Pretorius.pdf</a>

                                    <a href="" class="cvUpdate">Update</a>

                                </div><!-- field -->

                            </li>

                        </ul>

                    </div><!-- block -->

                    <div class="block">

                        <h2>Personal Details</h2>

                        <ul>

                            <li>

                                <label>Title*</label>

                                <div class="field small">

                                    <select class="custom_select">

                                        <option value="">Select</option>

                                        <option value="4">Dr</option>
                                        <option value="2">Miss</option>
                                        <option value="3">Mr</option>
                                        <option value="5" selected="selected">Mrs</option>
                                        <option value="1">Ms</option>
                                        <option value="6">Prof</option>


                                    </select>

                                </div><!-- field -->

                            </li>

                            <li>

                                <label>First Name*</label>

                                <div class="field">

                                    <input type="text" value="Chantel"/>

                                </div><!-- field -->

                            </li>

                            <li>

                                <label>Surname*</label>

                                <div class="field">

                                    <input type="text" value="Pretorius"/>

                                </div><!-- field -->

                            </li>

                            <li>

                                <label>Current Job Title*</label>

                                <div class="field">

                                    <input type="text" value="IT Manager"/>

                                </div><!-- field -->

                            </li>

                            <li>

                                <label>Current City*</label>

                                <div class="field">

                                    <input type="text" value="Durban"/>

                                </div>

                            </li>

                            <li>

                                <label>Employment Status*</label>

                                <div class="field">

                                    <select class="custom_select">

                                        <option value="">Select</option>

                                        <option value="1" selected="selected">Employed Fulltime</option>

                                    </select>

                                </div><!-- field -->

                            </li>

                            <li>

                                <label>Highest level of education*</label>

                                <div class="field">

                                    <select class="custom_select">

                                        <option value="">Select</option>

                                        <option value="Various Short Courses">Various Short Courses</option>
                                        <option value="Junior Certificate - Grade 10">Junior Certificate - Grade 10</option>
                                        <option value="Junior Certificate - Grade 11">Junior Certificate - Grade 11</option>
                                        <option value="Senior Certificate - Grade 12">Senior Certificate - Grade 12</option>
                                        <option value="Trade Certification">Trade Certification</option>
                                        <option value="National Certificate">National Certificate</option>
                                        <option value="National Diploma">National Diploma</option>
                                        <option value="Higher Diploma">Higher Diploma</option>
                                        <option value="Recent Graduate - No Experience">Recent Graduate - No Experience</option>
                                        <option value="Graduate - Degree">Graduate - Degree</option>
                                        <option value="Professional Qualification" selected="selected">Professional Qualification</option>
                                        <option value="Post Graduate - Honours">Post Graduate - Honours</option>
                                        <option value="Post Graduate - Masters">Post Graduate - Masters</option>
                                        <option value="Post Graduate - Doctorate">Post Graduate - Doctorate</option>

                                    </select>

                                </div><!-- field -->

                            </li>

                            <li>

                                <label>When did you start work?*</label>

                                <div class="field">

                                    <div class="col first">

                                        <select class="custom_select">

                                            <option value="">Year</option>

                                            <?php foreach(range(2016, 1966) as $year):?>

                                                <option value="<?php echo $year;?>"><?php echo $year;?></option>

                                            <?php endforeach;?>

                                        </select>

                                    </div><!-- col -->

                                    <div class="col second">

                                        <select class="custom_select">


                                            <option value="">Month</option>

                                            <option value="1" selected="selected">Jan</option>

                                            <option value="2">Feb</option>

                                            <option value="3">Mar</option>

                                            <option value="4">Apr</option>

                                            <option value="5">May</option>

                                            <option value="6">Jun</option>

                                            <option value="7">Jul</option>

                                            <option value="8">Aug</option>

                                            <option value="9">Sep</option>

                                            <option value="10">Oct</option>

                                            <option value="11">Nov</option>

                                            <option value="12">Dec</option>


                                        </select>

                                    </div><!-- col -->

                                    <div class="clear"></div>

                                </div><!-- field -->

                            </li>

                            <li>

                                <label>Select you ethnicity*</label>

                                <div class="field">

                                    <select class="custom_select">

                                        <option value="">Select</option>

                                        <option value="African">African</option>
                                        <option value="Asian">Asian</option>
                                        <option value="Coloured">Coloured</option>
                                        <option value="Indian">Indian</option>
                                        <option value="White" selected="selected">White</option>

                                    </select>

                                </div><!-- field -->

                            </li>

                            <li>

                                <label>Date of Birth*</label>

                                <div class="field">

                                    <input type="text" value="24/11/1977"/>

                                </div><!-- field -->

                            </li>

                            <li>

                                <label>Nationality*</label>

                                <div class="field">

                                    <select class="custom_select">

                                        <option value="">Select</option>

                                        <option value="74">Afghanistan</option>
                                        <option value="50">Albania</option>
                                        <option value="199">Algeria</option>
                                        <option value="139">American Samoa</option>
                                        <option value="171">Andorra</option>
                                        <option value="114">Angola</option>
                                        <option value="101">Anguilla</option>
                                        <option value="188">Antarctica</option>
                                        <option value="34">Antigua and Barbuda</option>
                                        <option value="219">Argentina</option>
                                        <option value="70">Armenia</option>
                                        <option value="88">Aruba</option>
                                        <option value="28">Australia</option>
                                        <option value="119">Austria</option>
                                        <option value="217">Azerbaidjan</option>
                                        <option value="227">Bahamas</option>
                                        <option value="97">Bahrain</option>
                                        <option value="89">Bangladesh</option>
                                        <option value="6">Barbados</option>
                                        <option value="5">Belarus</option>
                                        <option value="230">Belgium</option>
                                        <option value="46">Belize</option>
                                        <option value="32">Benin</option>
                                        <option value="158">Bermuda</option>
                                        <option value="124">Bhutan</option>
                                        <option value="84">Bolivia</option>
                                        <option value="30">Bosnia-Herzegovina</option>
                                        <option value="86">Botswana</option>
                                        <option value="96">Bouvet Island</option>
                                        <option value="174">Brazil</option>
                                        <option value="134">British Indian Ocean Territory</option>
                                        <option value="65">Brunei Darussalam</option>
                                        <option value="168">Bulgaria</option>
                                        <option value="54">Burkina Faso</option>
                                        <option value="144">Burundi</option>
                                        <option value="19">Cambodia</option>
                                        <option value="81">Cameroon</option>
                                        <option value="40">Canada</option>
                                        <option value="229">Cape Verde</option>
                                        <option value="148">Cayman Islands</option>
                                        <option value="141">Central African Republic</option>
                                        <option value="193">Chad</option>
                                        <option value="76">Chile</option>
                                        <option value="220">China</option>
                                        <option value="17">Christmas Island</option>
                                        <option value="11">Cocos (Keeling) Islands</option>
                                        <option value="128">Colombia</option>
                                        <option value="42">Comoros</option>
                                        <option value="98">Congo</option>
                                        <option value="189">Cook Islands</option>
                                        <option value="117">Costa Rica</option>
                                        <option value="94">Croatia</option>
                                        <option value="162">Cyprus</option>
                                        <option value="31">Czech Republic</option>
                                        <option value="25">Democratic Republic of Congo</option>
                                        <option value="214">Denmark</option>
                                        <option value="178">Djibouti</option>
                                        <option value="4">Dominica</option>
                                        <option value="67">Dominican Republic</option>
                                        <option value="64">East Timor</option>
                                        <option value="182">Ecuador</option>
                                        <option value="159">Egypt</option>
                                        <option value="85">El Salvador</option>
                                        <option value="208">Equatorial Guinea</option>
                                        <option value="160">Eritrea</option>
                                        <option value="100">Estonia</option>
                                        <option value="95">Ethiopia</option>
                                        <option value="149">Falkland Islands</option>
                                        <option value="209">Faroe Islands</option>
                                        <option value="12">Fiji</option>
                                        <option value="143">Former USSR</option>
                                        <option value="177">France</option>
                                        <option value="125">France (European Territory)</option>
                                        <option value="91">French Guyana</option>
                                        <option value="48">French Southern Territories</option>
                                        <option value="170">Gabon</option>
                                        <option value="169">Gambia</option>
                                        <option value="142">Georgia</option>
                                        <option value="176">Germany</option>
                                        <option value="231">Ghana</option>
                                        <option value="90">Gibraltar</option>
                                        <option value="102">Greece</option>
                                        <option value="69">Greenland</option>
                                        <option value="175">Grenada</option>
                                        <option value="191">Guadeloupe (French)</option>
                                        <option value="121">Guam</option>
                                        <option value="49">Guatemala</option>
                                        <option value="202">Guinea</option>
                                        <option value="133">Guinea Bissau</option>
                                        <option value="116">Guyana</option>
                                        <option value="36">Haiti</option>
                                        <option value="29">Heard and McDonald Islands</option>
                                        <option value="16">Honduras</option>
                                        <option value="33">Hong Kong</option>
                                        <option value="21">Hungary</option>
                                        <option value="78">Iceland</option>
                                        <option value="203">India</option>
                                        <option value="80">Indonesia</option>
                                        <option value="122">Iraq</option>
                                        <option value="165">Ireland</option>
                                        <option value="131">Israel</option>
                                        <option value="179">Italy</option>
                                        <option value="153">Ivory Coast</option>
                                        <option value="187">Jamaica</option>
                                        <option value="7">Japan</option>
                                        <option value="211">Jordan</option>
                                        <option value="201">Kazakhstan</option>
                                        <option value="1">Kenya</option>
                                        <option value="37">Kiribati</option>
                                        <option value="164">Kuwait</option>
                                        <option value="45">Kyrgyzstan</option>
                                        <option value="71">Laos</option>
                                        <option value="151">Latvia</option>
                                        <option value="183">Lebanon</option>
                                        <option value="3">Lesotho</option>
                                        <option value="157">Liberia</option>
                                        <option value="58">Liechtenstein</option>
                                        <option value="108">Lithuania</option>
                                        <option value="99">Luxembourg</option>
                                        <option value="135">Macau</option>
                                        <option value="9">Macedonia</option>
                                        <option value="26">Madagascar</option>
                                        <option value="39">Malawi</option>
                                        <option value="111">Malaysia</option>
                                        <option value="180">Maldives</option>
                                        <option value="156">Mali</option>
                                        <option value="41">Malta</option>
                                        <option value="92">Marshall Islands</option>
                                        <option value="216">Martinique (French)</option>
                                        <option value="181">Mauritania</option>
                                        <option value="190">Mauritius</option>
                                        <option value="184">Mayotte</option>
                                        <option value="27">Mexico</option>
                                        <option value="186">Micronesia</option>
                                        <option value="18">Moldavia</option>
                                        <option value="225">Monaco</option>
                                        <option value="82">Mongolia</option>
                                        <option value="137">Montserrat</option>
                                        <option value="38">Morocco</option>
                                        <option value="43">Mozambique</option>
                                        <option value="196">Namibia</option>
                                        <option value="83">Nauru</option>
                                        <option value="63">Nepal</option>
                                        <option value="207">Netherlands</option>
                                        <option value="150">Netherlands Antilles</option>
                                        <option value="173">Neutral Zone</option>
                                        <option value="228">New Caledonia (French)</option>
                                        <option value="13">New Zealand</option>
                                        <option value="200">Nicaragua</option>
                                        <option value="44">Niger</option>
                                        <option value="2">Nigeria</option>
                                        <option value="204">Niue</option>
                                        <option value="198">Norfolk Island</option>
                                        <option value="147">Northern Mariana Islands</option>
                                        <option value="47">Norway</option>
                                        <option value="222">Oman</option>
                                        <option value="163">Pakistan</option>
                                        <option value="194">Palau</option>
                                        <option value="172">Panama</option>
                                        <option value="154">Papua New Guinea</option>
                                        <option value="126">Paraguay</option>
                                        <option value="103">Peru</option>
                                        <option value="10">Philippines</option>
                                        <option value="14">Pitcairn Island</option>
                                        <option value="52">Poland</option>
                                        <option value="93">Polynesia (French)</option>
                                        <option value="161">Portugal</option>
                                        <option value="138">Qatar</option>
                                        <option value="23">Reunion (French)</option>
                                        <option value="66">Romania</option>
                                        <option value="130">Russian Federation</option>
                                        <option value="15">Rwanda</option>
                                        <option value="53">S. Georgia and S. Sandwich Islands</option>
                                        <option value="22">Saint Helena</option>
                                        <option value="79">Saint Kitts and Nevis Anguilla</option>
                                        <option value="105">Saint Lucia</option>
                                        <option value="185">Saint Pierre and Miquelon</option>
                                        <option value="132">Saint Tome and Principe</option>
                                        <option value="107">Saint Vincent and Grenadines</option>
                                        <option value="129">Samoa</option>
                                        <option value="223">San Marino</option>
                                        <option value="104">Saudi Arabia</option>
                                        <option value="213">Senegal</option>
                                        <option value="197">Serbia-Montenegro</option>
                                        <option value="68">Seychelles</option>
                                        <option value="195">Sierra Leone</option>
                                        <option value="113">Singapore</option>
                                        <option value="61">Slovenia</option>
                                        <option value="112">Solomon Islands</option>
                                        <option value="192">Somalia</option>
                                        <option value="140" selected="selected">South Africa</option>
                                        <option value="59">South Korea</option>
                                        <option value="155">Spain</option>
                                        <option value="87">Sri Lanka</option>
                                        <option value="152">Suriname</option>
                                        <option value="120">Svalbard and Jan Mayen Islands</option>
                                        <option value="72">Swaziland</option>
                                        <option value="166">Sweden</option>
                                        <option value="118">Switzerland</option>
                                        <option value="226">Tadjikistan</option>
                                        <option value="221">Taiwan</option>
                                        <option value="205">Tanzania</option>
                                        <option value="110">Thailand</option>
                                        <option value="56">Togo</option>
                                        <option value="123">Tokelau</option>
                                        <option value="55">Tonga</option>
                                        <option value="210">Trinidad and Tobago</option>
                                        <option value="145">Tunisia</option>
                                        <option value="106">Turkey</option>
                                        <option value="77">Turkmenistan</option>
                                        <option value="8">Turks and Caicos Islands</option>
                                        <option value="24">Tuvalu</option>
                                        <option value="167">Uganda</option>
                                        <option value="109">UK</option>
                                        <option value="115">Ukraine</option>
                                        <option value="212">United Arab Emirates</option>
                                        <option value="35">Uruguay</option>
                                        <option value="75">US</option>
                                        <option value="73">USA Minor Outlying Islands</option>
                                        <option value="206">Uzbekistan</option>
                                        <option value="57">Vanuatu</option>
                                        <option value="218">Vatican City</option>
                                        <option value="136">Venezuela</option>
                                        <option value="62">Vietnam</option>
                                        <option value="146">Virgin Islands (British)</option>
                                        <option value="60">Virgin Islands (USA)</option>
                                        <option value="127">Wallis and Futuna Islands</option>
                                        <option value="224">Western Sahara</option>
                                        <option value="215">Yemen</option>
                                        <option value="20">Zambia</option>
                                        <option value="51">Zimbabwe</option>

                                    </select>

                                </div><!-- field -->

                            </li>

                            <li>

                                <label>Telephone*</label>

                                <div class="field">

                                    <div class="field">

                                        <input type="text" value="082 454 8945"/>

                                    </div><!-- field -->

                                </div>

                            </li>

                            <li>

                                <label>Mobile Phone*</label>

                                <div class="field">

                                    <input type="text" value="082 454 8945"/>

                                </div><!-- field -->

                            </li>

                            <li class="top">

                                <label>Languages*<br/><span class="note">Hold Ctrl to select multiple</span></label>

                                <div class="field">

                                    <select multiple="multiple">

                                        <option value="">Afrikaans</option>

                                        <option value="" selected="selected">English</option>

                                        <option value="">isiNdebele</option>

                                        <option value="">isiXhosa</option>

                                    </select>

                                </div><!-- field -->

                            </li>

                        </ul>


                    </div><!-- block -->

                    <div class="submit">

                        <input type="submit" class="btn btnBlue" value="Update Profile"/>

                        <div class="clear"></div>

                    </div>

                </form>

            </div><!-- container -->


        </div><!-- profile -->

    </main><!-- main -->

    <!--//footer -->
    <footer>

        <div class="tagline">

            <p>Be First <span></span> Be Fast <span></span> Be Smart</p>

        </div><!-- tag line -->

        <div class="container l1">

            <div class="top">

                <div class="threeColumn">

                    <div class="col one">

                        <h3>JobVine Global</h3>

                        <p>At Jobvine our goal is to help you make the most of the 80 or 90 years you have on this planet by connecting you to the real world opportunities that can help you achieve your goals and realize your dreams. Visit <a href="">Jobvine.com</a></p>

                    </div><!-- col -->

                    <div class="col two">

                        <h3>JobVine Blog</h3>

                        <p>News, views, career advice and interview tips. And more</p>

                    </div><!-- col -->

                    <div class="col three">

                        <h3>For Employers</h3>

                        <ul>

                            <li><a href="">Post a Job</a></li>

                            <li><a href="">Products & Services</a></li>

                            <li><a href="">Contact Us</a></li>

                        </ul>

                    </div><!-- col -->

                    <div class="clear"></div>

                </div><!-- three column -->

            </div><!-- top -->

            <div class="bottom">

                <div class="left">

                    <ul class="nav">

                        <li><a href="#">About Us</a></li>

                        <li><a href="#">Contact Us</a></li>

                        <li><a href="#">Terms and Conditions</a></li>

                        <li><a href="#">Testimonials</a></li>


                    </ul>

                    <div class="clear"></div>

                    <p>&#169; <?php echo date("Y");?>. JobVine.co.za All Right Reserved.  C/O Mauritius International Trust Company Limited, <br/>4th Floor, Ebene Skies, Rue de I'institut, Ebene, Mauritius</p>

                </div><!-- left -->


                <ul class="social">

                    <li><a href="#" class="twitter" target="_blank"></a></li>

                    <li><a href="#" class="fb" target="_blank"></a></li>

                    <li><a href="#" class="linkedin" target="_blank"></a></li>

                    <li><a href="#" class="gplus" target="_blank"></a></li>

                </ul><!-- end social -->


                <div class="clear"></div>

            </div><!-- bottom -->

            <div class="clear"></div>

        </div><!-- container -->

    </footer><!-- end footer -->



</div><!-- end page -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>

<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-color/2.1.2/jquery.color.min.js"></script>


<script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"></script>



<script src="js/main.js"></script>


</body>
</html>
